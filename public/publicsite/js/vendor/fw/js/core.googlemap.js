function chezaschool_googlemap_init(dom_obj, coords) {
	"use strict";
	if (typeof chezaschool_STORAGE['googlemap_init_obj'] == 'undefined') chezaschool_googlemap_init_styles();
	chezaschool_STORAGE['googlemap_init_obj'].geocoder = '';
	try {
		var id = dom_obj.id;
		chezaschool_STORAGE['googlemap_init_obj'][id] = {
			dom: dom_obj,
			markers: coords.markers,
			geocoder_request: false,
			opt: {
				zoom: coords.zoom,
				center: null,
				scrollwheel: false,
				scaleControl: false,
				disableDefaultUI: false,
				panControl: true,
				zoomControl: true, //zoom
				mapTypeControl: false,
				streetViewControl: false,
				overviewMapControl: false,
				styles: chezaschool_STORAGE['googlemap_styles'][coords.style ? coords.style : 'default'],
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
		};
		
		chezaschool_googlemap_create(id);

	} catch (e) {
		
		dcl(chezaschool_STORAGE['strings']['googlemap_not_avail']);

	};
}

function chezaschool_googlemap_create(id) {
	"use strict";

	// Create map
	chezaschool_STORAGE['googlemap_init_obj'][id].map = new google.maps.Map(chezaschool_STORAGE['googlemap_init_obj'][id].dom, chezaschool_STORAGE['googlemap_init_obj'][id].opt);

	// Add markers
	for (var i in chezaschool_STORAGE['googlemap_init_obj'][id].markers)
		chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].inited = false;
	chezaschool_googlemap_add_markers(id);
	
	// Add resize listener
	jQuery(window).resize(function() {
		if (chezaschool_STORAGE['googlemap_init_obj'][id].map)
			chezaschool_STORAGE['googlemap_init_obj'][id].map.setCenter(chezaschool_STORAGE['googlemap_init_obj'][id].opt.center);
	});
}

function chezaschool_googlemap_add_markers(id) {
	"use strict";
	for (var i in chezaschool_STORAGE['googlemap_init_obj'][id].markers) {
		
		if (chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].inited) continue;
		
		if (chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].latlng == '') {
			
			if (chezaschool_STORAGE['googlemap_init_obj'][id].geocoder_request!==false) continue;
			
			if (chezaschool_STORAGE['googlemap_init_obj'].geocoder == '') chezaschool_STORAGE['googlemap_init_obj'].geocoder = new google.maps.Geocoder();
			chezaschool_STORAGE['googlemap_init_obj'][id].geocoder_request = i;
			chezaschool_STORAGE['googlemap_init_obj'].geocoder.geocode({address: chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].address}, function(results, status) {
				"use strict";
				if (status == google.maps.GeocoderStatus.OK) {
					var idx = chezaschool_STORAGE['googlemap_init_obj'][id].geocoder_request;
					if (results[0].geometry.location.lat && results[0].geometry.location.lng) {
						chezaschool_STORAGE['googlemap_init_obj'][id].markers[idx].latlng = '' + results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng();
					} else {
						chezaschool_STORAGE['googlemap_init_obj'][id].markers[idx].latlng = results[0].geometry.location.toString().replace(/\(\)/g, '');
					}
					chezaschool_STORAGE['googlemap_init_obj'][id].geocoder_request = false;
					setTimeout(function() { 
						chezaschool_googlemap_add_markers(id); 
						}, 200);
				} else
					dcl(chezaschool_STORAGE['strings']['geocode_error'] + ' ' + status);
			});
		
		} else {
			
			// Prepare marker object
			var latlngStr = chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].latlng.split(',');
			var markerInit = {
				map: chezaschool_STORAGE['googlemap_init_obj'][id].map,
				position: new google.maps.LatLng(latlngStr[0], latlngStr[1]),
				clickable: chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].description!=''
			};
			if (chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].point) markerInit.icon = chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].point;
			if (chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].title) markerInit.title = chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].title;
			chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].marker = new google.maps.Marker(markerInit);
			
			// Set Map center
			if (chezaschool_STORAGE['googlemap_init_obj'][id].opt.center == null) {
				chezaschool_STORAGE['googlemap_init_obj'][id].opt.center = markerInit.position;
				chezaschool_STORAGE['googlemap_init_obj'][id].map.setCenter(chezaschool_STORAGE['googlemap_init_obj'][id].opt.center);				
			}
			
			// Add description window
			if (chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].description!='') {
				chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].infowindow = new google.maps.InfoWindow({
					content: chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].description
				});
				google.maps.event.addListener(chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].marker, "click", function(e) {
					var latlng = e.latLng.toString().replace("(", '').replace(")", "").replace(" ", "");
					for (var i in chezaschool_STORAGE['googlemap_init_obj'][id].markers) {
						if (latlng == chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].latlng) {
							chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].infowindow.open(
								chezaschool_STORAGE['googlemap_init_obj'][id].map,
								chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].marker
							);
							break;
						}
					}
				});
			}
			
			chezaschool_STORAGE['googlemap_init_obj'][id].markers[i].inited = true;
		}
	}
}

function chezaschool_googlemap_refresh() {
	"use strict";
	for (id in chezaschool_STORAGE['googlemap_init_obj']) {
		chezaschool_googlemap_create(id);
	}
}

function chezaschool_googlemap_init_styles() {
	// Init Google map
	chezaschool_STORAGE['googlemap_init_obj'] = {};
	chezaschool_STORAGE['googlemap_styles'] = {
		'default': []
	};
	if (window.chezaschool_theme_googlemap_styles!==undefined)
		chezaschool_STORAGE['googlemap_styles'] = chezaschool_theme_googlemap_styles(chezaschool_STORAGE['googlemap_styles']);
}