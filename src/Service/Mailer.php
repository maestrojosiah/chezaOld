<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use App\Entity\User;
use App\Repository\UserSettingsRepository;
use Knp\Snappy\Pdf;
use Twig\Environment;

class Mailer 
{
    private $mailer;
    private $pdf;
    private $twig;
    private $usr;

    public function __construct(MailerInterface $mailer, Pdf $pdf, Environment $twig, UserSettingsRepository $usr)
    {
        $this->mailer = $mailer;
        $this->pdf = $pdf;
        $this->twig = $twig;
        $this->usr = $usr;

    }

    public function sendEmailMessage($data, $sendto, $subject, $template, $category, $emailtype)
    {
        $settings = [];
        $emailtypes = [];
        $send = false;
        $usersettings = $this->usr->findAll();
        foreach ($usersettings as $key => $setting) {
            if($setting->getUser()->getEmail() == $sendto){
                $settings[$setting->getSetting()->getDescription()] = $setting->getSetting()->getDescription()."-".$setting->getValue();
                $emailtypes[$setting->getSetting()->getId()] = $setting->getSetting()->getDescription();
            }
        }

        // if no settings have been done OR the category for this email is 'necessary', send email
        if(empty($settings) || $category == "necessary" ){
            $send = true; //change this to true when the assigning of lessons is over
        } 
        // if the setting for this emailtype is done and it is on, send email
        if(in_array($emailtype."-on", $settings)){
            $send = true;
        }
        // if the setting for this emailtype is not done, assume that it is on
        if(!in_array($emailtype, $emailtypes)){
            $send = true;
        }
        // if all emails is off, don't send email
        if(in_array("all_emails-off", $settings)){
            $send = false;
        }

        if($send == true){
            $email = (new TemplatedEmail())
            ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
            ->to(new Address($sendto))
            ->subject($subject)
        
            // path of the Twig template to render
            ->htmlTemplate('emails/'.$template)
        
            // pass variables (name => value) to the template
            ->context($data);
    
            $this->mailer->send($email);
            return true;
    
        }

    }

    public function sendEmailWithLinkedAttachment($data, $sendto, $subject, $template, $category, $emailtype, $attachmentPath, $attachmentName)
    {
        $settings = [];
        $emailtypes = [];
        $send = false;
        $usersettings = $this->usr->findAll();
        foreach ($usersettings as $key => $setting) {
            if($setting->getUser()->getEmail() == $sendto){
                $settings[$setting->getSetting()->getDescription()] = $setting->getSetting()->getDescription()."-".$setting->getValue();
                $emailtypes[$setting->getSetting()->getId()] = $setting->getSetting()->getDescription();
            }
        }

        // if no settings have been done OR the category for this email is 'necessary', send email
        if(empty($settings) || $category == "necessary" ){
            $send = true; //change this to true when the assigning of lessons is over
        } 
        // if the setting for this emailtype is done and it is on, send email
        if(in_array($emailtype."-on", $settings)){
            $send = true;
        }
        // if the setting for this emailtype is not done, assume that it is on
        if(!in_array($emailtype, $emailtypes)){
            $send = true;
        }
        // if all emails is off, don't send email
        if(in_array("all_emails-off", $settings)){
            $send = false;
        }

        if($send == true){
            $email = (new TemplatedEmail())
            ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
            ->to(new Address($sendto))
            ->subject($subject)
            ->attachFromPath($attachmentPath, $attachmentName)
        
            // path of the Twig template to render
            ->htmlTemplate('emails/'.$template)
        
            // pass variables (name => value) to the template
            ->context($data);
    
            $this->mailer->send($email);
            return true;
    
        }

    }

    // public function sendWelcomeMessage(Article $article)
    // {
    //     $email = (new TemplatedEmail())
    //     ->from(new Address('info@chezamusicschool.co.ke', 'MuSKe Ke'))
    //     ->to(new Address($article->getAuthor()->getEmail()))
    //     ->subject('Your article has been published!')
    
    //     // path of the Twig template to render
    //     ->htmlTemplate('emails/sample.html.twig')
    
    //     // pass variables (name => value) to the template
    //     ->context([
    //         // 'expiration_date' => new \DateTime('+7 days'),
    //         'article' => $article,
    //     ]);

    //     $this->mailer->send($email);

    // }

    public function sendEmailWithAttachment($data, $sendto, $subject, $html, $template, $docname)
    {
        // $html = $this->twig->render('emails/author-weekly-report-pdf.html.twig', [
        //     'articles' => $articles,
        // ]);
        $pdf = $this->pdf->getOutputFromHtml($html);

        $email = (new TemplatedEmail())
        ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
        ->to(new Address($sendto))
        ->subject($subject)
        ->htmlTemplate('emails/'.$template)
        ->context($data)
        ->attach($pdf, sprintf($docname.'-%s.pdf', date('Y-m-d')));

        $this->mailer->send($email);
        return true;

    }

}