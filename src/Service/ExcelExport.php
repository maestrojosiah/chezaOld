<?php

namespace App\Service;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Entity\User;
use Knp\Snappy\Pdf;
use Twig\Environment;

class ExcelExport 
{
    private $pdf;
    private $twig;

    public function __construct( Pdf $pdf, Environment $twig)
    {
        $this->pdf = $pdf;
        $this->twig = $twig;

    }

    public function exportExcelFile($data, $user, $ths, $title, $description, $keywords, $category, $filename)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $atoz = range('A', 'Z');
        
        // Set document properties // magegania
        $spreadsheet->getProperties()->setCreator($user->getFullname())
            ->setLastModifiedBy($user->getFullname())
            ->setTitle($title)
            ->setSubject($title)
            ->setDescription($description)
            ->setKeywords($keywords)
            ->setCategory($category);
        
        // th -> ['A1' => 'name', 'A2' => 'email']
        
        foreach ($ths as $key => $value ) {
            $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue($key, $value);
        }
        $spreadsheet->getActiveSheet()->getStyle("A1:Z1")->getFont()->setBold( true );

        $counter = 2;
        // trs (array of arrays) -> [['name' => 'Someone Omori', 'email' => 'someone@mail.com'], ['name' => 'Another Onyango', 'email' => 'another@mail.com']]
        
        foreach ($data as $entry) {
            $col = 0;
            foreach ($entry as $key => $value) {
                $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue($atoz[$col].$counter, $value)->getColumnDimension($atoz[$col])->setAutoSize(true);
                $col++;
            }
            $counter++;
            $col = 0;
        }
                
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;    

    }

    public function exportPdfFile($data, $user, $ths, $title, $description, $keywords, $category, $filename)
    {
        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $atoz = range('A', 'Z');
        
        // Set document properties // magegania
        $spreadsheet->getProperties()->setCreator($user->getFullname())
            ->setLastModifiedBy($user->getFullname())
            ->setTitle($title)
            ->setSubject($title)
            ->setDescription($description)
            ->setKeywords($keywords)
            ->setCategory($category);
        
        // th -> ['A1' => 'name', 'A2' => 'email']
        
        foreach ($ths as $key => $value ) {
            $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue($key, $value);
        }
        $spreadsheet->getActiveSheet()->getStyle("A1:Z1")->getFont()->setBold( true )->setSize(32);

        $styleArray = [
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        
        $counter = 2;
        // trs (array of arrays) -> [['name' => 'Someone Omori', 'email' => 'someone@mail.com'], ['name' => 'Another Onyango', 'email' => 'another@mail.com']]
        
        foreach ($data as $entry) {
            $col = 0;
            foreach ($entry as $key => $value) {
                $spreadsheet->setActiveSheetIndex(0)
                ->setCellValue($atoz[$col].$counter, $value)->getColumnDimension($atoz[$col])->setAutoSize(true);
                $spreadsheet->getActiveSheet()->getStyle($atoz[$col].$counter)->applyFromArray($styleArray)->getFont()->setSize(32);  
                $spreadsheet->getActiveSheet()->getStyle($atoz[$col].$counter)->getAlignment()->setIndent(5);     
                $col++;
            }
            $counter++;
            $col = 0;
        }
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);
        
        IOFactory::registerWriter('Pdf', \PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf::class);

        // Redirect output to a client’s web browser (PDF)
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment;filename="'.$filename.'.pdf"');
        header('Cache-Control: max-age=0');

        $writer = IOFactory::createWriter($spreadsheet, 'Pdf');
        $writer->save('php://output');
        exit;

    }


}