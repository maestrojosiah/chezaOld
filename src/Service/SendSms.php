<?php 

namespace App\Service;

class SendSms
{

    function CallAPI($method, $url, $data = false)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "$method",
            CURLOPT_POSTFIELDS =>"$data",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json",
                "Content-Type: application/json",
                "Authorization: Basic Y29uc2VydmtjbTpFaXJ3bXItMzMtaHA="
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    function getReports($method, $url, $data = false){
        // Delivery reports can only be retrieved one time. Once you retrieve a delivery report, you 
        // will not be able to get the same report again by using this endpoint
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "$method",
          CURLOPT_POSTFIELDS =>"$data",
          CURLOPT_HTTPHEADER => array(
            'Authorization: Basic Y29uc2VydmtjbTpFaXJ3bXItMzMtaHA=',
            'Content-Type: application/json'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;
        
    }

    function getLogs(){
        // Unlike delivery reports, these logs can be requested as many times as you want
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => 'http://54.247.191.102/restapi/sms/1/logs',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_POSTFIELDS =>'{ 
         "from":"CHEZAMUSIC",
         "to":"254702825682",
         "text":"test SMS"
        }',
          CURLOPT_HTTPHEADER => array(
            'Authorization: Basic Y29uc2VydmtjbTpFaXJ3bXItMzMtaHA=',
            'Content-Type: application/json'
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;
        
    }

}
