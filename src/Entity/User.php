<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fullname;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\OneToMany(targetEntity=Session::class, mappedBy="teacher")
     */
    private $teachersessions;

    /**
     * @ORM\OneToMany(targetEntity=Session::class, mappedBy="student")
     */
    private $studentsessions;

    /**
     * @ORM\OneToOne(targetEntity=TeacherUserData::class, inversedBy="teacher", cascade={"persist", "remove"})
     */
    private $teacherdata;

    /**
     * @ORM\OneToOne(targetEntity=StudentUserData::class, inversedBy="student", cascade={"persist", "remove"})
     */
    private $studentdata;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $usertype;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(mimeTypes={ "image/png", "image/jpg", "image/jpeg" })
     */
    private $photo;

    /**
     * @ORM\OneToMany(targetEntity=SchoolTime::class, mappedBy="teacher")
     */
    private $schoolTimes;

    /**
     * @ORM\OneToMany(targetEntity=Payment::class, mappedBy="user")
     */
    private $payments;

    /**
     * @ORM\OneToMany(targetEntity=UserSettings::class, mappedBy="user")
     */
    private $userSettings;

    /**
     * @ORM\OneToMany(targetEntity=Communication::class, mappedBy="admin")
     */
    private $communications;

    /**
     * @ORM\OneToMany(targetEntity=DeductionPayment::class, mappedBy="teacher")
     */
    private $deductionPayments;

    /**
     * @ORM\OneToMany(targetEntity=Post::class, mappedBy="user")
     */
    private $posts;

    public function  __toString(){
        return $this->getFullName();
    }

    public function __construct()
    {
        $this->teachersessions = new ArrayCollection();
        $this->studentsessions = new ArrayCollection();
        $this->schoolTimes = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->settings = new ArrayCollection();
        $this->userSettings = new ArrayCollection();
        $this->communications = new ArrayCollection();
        $this->deductionPayments = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function hasRole(array $roles, $role)
    {
        return in_array($role, $roles);
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    public function setFullname(string $fullname): self
    {
        $this->fullname = $fullname;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getTeacherSessions(): Collection
    {
        return $this->teachersessions;
    }

    public function addTeacherSession(Session $teachersession): self
    {
        if (!$this->teachersessions->contains($teachersession)) {
            $this->teachersessions[] = $teachersession;
            $teachersession->setTeacher($this);
        }

        return $this;
    }

    public function removeTeacherSession(Session $teachersession): self
    {
        if ($this->teachersessions->removeElement($teachersession)) {
            // set the owning side to null (unless already changed)`
            if ($teachersession->getTeacher() === $this) {
                $teachersession->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Session[]
     */
    public function getStudentsessions(): Collection
    {
        return $this->studentsessions;
    }

    public function addStudentsession(Session $studentsession): self
    {
        if (!$this->studentsessions->contains($studentsession)) {
            $this->studentsessions[] = $studentsession;
            $studentsession->setStudent($this);
        }

        return $this;
    }

    public function removeStudentsession(Session $studentsession): self
    {
        if ($this->studentsessions->removeElement($studentsession)) {
            // set the owning side to null (unless already changed)
            if ($studentsession->getStudent() === $this) {
                $studentsession->setStudent(null);
            }
        }

        return $this;
    }

    public function getTeacherdata(): ?TeacherUserData
    {
        return $this->teacherdata;
    }

    public function setTeacherdata(?TeacherUserData $teacherdata): self
    {
        $this->teacherdata = $teacherdata;

        return $this;
    }

    public function getStudentdata(): ?StudentUserData
    {
        return $this->studentdata;
    }

    public function setStudentdata(?StudentUserData $studentdata): self
    {
        $this->studentdata = $studentdata;

        return $this;
    }

    public function getUsertype(): ?string
    {
        return $this->usertype;
    }

    public function setUsertype(string $usertype): self
    {
        $this->usertype = $usertype;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * @return Collection|SchoolTime[]
     */
    public function getSchoolTimes(): Collection
    {
        return $this->schoolTimes;
    }

    public function addSchoolTime(SchoolTime $schoolTime): self
    {
        if (!$this->schoolTimes->contains($schoolTime)) {
            $this->schoolTimes[] = $schoolTime;
            $schoolTime->setTeacher($this);
        }

        return $this;
    }

    public function removeSchoolTime(SchoolTime $schoolTime): self
    {
        if ($this->schoolTimes->removeElement($schoolTime)) {
            // set the owning side to null (unless already changed)
            if ($schoolTime->getTeacher() === $this) {
                $schoolTime->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setUser($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->removeElement($payment)) {
            // set the owning side to null (unless already changed)
            if ($payment->getUser() === $this) {
                $payment->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Settings[]
     */
    public function getSettings(): Collection
    {
        return $this->settings;
    }

    public function addSetting(Settings $setting): self
    {
        if (!$this->settings->contains($setting)) {
            $this->settings[] = $setting;
            $setting->setUser($this);
        }

        return $this;
    }

    public function removeSetting(Settings $setting): self
    {
        if ($this->settings->removeElement($setting)) {
            // set the owning side to null (unless already changed)
            if ($setting->getUser() === $this) {
                $setting->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserSettings[]
     */
    public function getUserSettings(): Collection
    {
        return $this->userSettings;
    }

    public function addUserSetting(UserSettings $userSetting): self
    {
        if (!$this->userSettings->contains($userSetting)) {
            $this->userSettings[] = $userSetting;
            $userSetting->setUser($this);
        }

        return $this;
    }

    public function removeUserSetting(UserSettings $userSetting): self
    {
        if ($this->userSettings->removeElement($userSetting)) {
            // set the owning side to null (unless already changed)
            if ($userSetting->getUser() === $this) {
                $userSetting->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Communication[]
     */
    public function getCommunications(): Collection
    {
        return $this->communications;
    }

    public function addCommunication(Communication $communication): self
    {
        if (!$this->communications->contains($communication)) {
            $this->communications[] = $communication;
            $communication->setAdmin($this);
        }

        return $this;
    }

    public function removeCommunication(Communication $communication): self
    {
        if ($this->communications->removeElement($communication)) {
            // set the owning side to null (unless already changed)
            if ($communication->getAdmin() === $this) {
                $communication->setAdmin(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DeductionPayment[]
     */
    public function getDeductionPayments(): Collection
    {
        return $this->deductionPayments;
    }

    public function addDeductionPayment(DeductionPayment $deductionPayment): self
    {
        if (!$this->deductionPayments->contains($deductionPayment)) {
            $this->deductionPayments[] = $deductionPayment;
            $deductionPayment->setTeacher($this);
        }

        return $this;
    }

    public function removeDeductionPayment(DeductionPayment $deductionPayment): self
    {
        if ($this->deductionPayments->removeElement($deductionPayment)) {
            // set the owning side to null (unless already changed)
            if ($deductionPayment->getTeacher() === $this) {
                $deductionPayment->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setUser($this);
        }

        return $this;
    }

    public function removePost(Post $post): self
    {
        if ($this->posts->removeElement($post)) {
            // set the owning side to null (unless already changed)
            if ($post->getUser() === $this) {
                $post->setUser(null);
            }
        }

        return $this;
    }
}
