<?php

namespace App\Entity;

use App\Repository\TermRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TermRepository::class)
 */
class Term
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $termnumber;

    /**
     * @ORM\Column(type="datetime")
     */
    private $startingOn;

    /**
     * @ORM\Column(type="datetime")
     */
    private $endingOn;

    /**
     * @ORM\OneToMany(targetEntity=Report::class, mappedBy="term")
     */
    private $reports;

    /**
     * @ORM\Column(type="datetime")
     */
    private $halftermstart;

    /**
     * @ORM\Column(type="datetime")
     */
    private $halftermend;

    /**
     * @ORM\Column(type="datetime")
     */
    private $holidaystart;

    /**
     * @ORM\Column(type="datetime")
     */
    private $holidayend;

    public function __construct()
    {
        $this->reports = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->termnumber;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTermnumber(): ?string
    {
        return $this->termnumber;
    }

    public function setTermnumber(string $termnumber): self
    {
        $this->termnumber = $termnumber;

        return $this;
    }

    public function getStartingOn(): ?\DateTimeInterface
    {
        return $this->startingOn;
    }

    public function setStartingOn(\DateTimeInterface $startingOn): self
    {
        $this->startingOn = $startingOn;

        return $this;
    }

    public function getEndingOn(): ?\DateTimeInterface
    {
        return $this->endingOn;
    }

    public function setEndingOn(\DateTimeInterface $endingOn): self
    {
        $this->endingOn = $endingOn;

        return $this;
    }

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setTerm($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getTerm() === $this) {
                $report->setTerm(null);
            }
        }

        return $this;
    }

    public function getHalftermstart(): ?\DateTimeInterface
    {
        return $this->halftermstart;
    }

    public function setHalftermstart(\DateTimeInterface $halftermstart): self
    {
        $this->halftermstart = $halftermstart;

        return $this;
    }

    public function getHalftermend(): ?\DateTimeInterface
    {
        return $this->halftermend;
    }

    public function setHalftermend(\DateTimeInterface $halftermend): self
    {
        $this->halftermend = $halftermend;

        return $this;
    }

    public function getHolidaystart(): ?\DateTimeInterface
    {
        return $this->holidaystart;
    }

    public function setHolidaystart(\DateTimeInterface $holidaystart): self
    {
        $this->holidaystart = $holidaystart;

        return $this;
    }

    public function getHolidayend(): ?\DateTimeInterface
    {
        return $this->holidayend;
    }

    public function setHolidayend(\DateTimeInterface $holidayend): self
    {
        $this->holidayend = $holidayend;

        return $this;
    }
}
