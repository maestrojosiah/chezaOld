<?php

namespace App\Entity;

use App\Repository\DeductionPaymentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DeductionPaymentRepository::class)
 */
class DeductionPayment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $paidAmt;

    /**
     * @ORM\Column(type="date")
     */
    private $paidOn;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="deductionPayments")
     */
    private $teacher;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $balance;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cleared;

    /**
     * @ORM\ManyToOne(targetEntity=Deductions::class, inversedBy="deductionPayments")
     */
    private $deduction;

    public function __toString(){
        return $this->teacher->getFullname();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPaidAmt(): ?string
    {
        return $this->paidAmt;
    }

    public function setPaidAmt(string $paidAmt): self
    {
        $this->paidAmt = $paidAmt;

        return $this;
    }

    public function getPaidOn(): ?\DateTimeInterface
    {
        return $this->paidOn;
    }

    public function setPaidOn(\DateTimeInterface $paidOn): self
    {
        $this->paidOn = $paidOn;

        return $this;
    }

    public function getTeacher(): ?User
    {
        return $this->teacher;
    }

    public function setTeacher(?User $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getBalance(): ?string
    {
        return $this->balance;
    }

    public function setBalance(string $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getCleared(): ?bool
    {
        return $this->cleared;
    }

    public function setCleared(bool $cleared): self
    {
        $this->cleared = $cleared;

        return $this;
    }

    public function getDeduction(): ?Deductions
    {
        return $this->deduction;
    }

    public function setDeduction(?Deductions $deduction): self
    {
        $this->deduction = $deduction;

        return $this;
    }
}
