<?php

namespace App\Entity;

use App\Repository\TeacherUserDataRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TeacherUserDataRepository::class)
 */
class TeacherUserData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resumelnk;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="teacherdata", cascade={"persist", "remove"})
     */
    private $teacher;

    /**
     * @ORM\OneToMany(targetEntity=UserInstrumentGrade::class, mappedBy="teacherUserData")
     */
    private $userinstruments;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lessonmode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $branch;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sex;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nextofkinname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nextofkinphonenum;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nextofkinemail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nextofkinrship;

    /**
     * @ORM\OneToMany(targetEntity=Deductions::class, mappedBy="teacher")
     */
    private $deductions;

    /**
     * @ORM\OneToMany(targetEntity=Report::class, mappedBy="teacher")
     */
    private $reports;

    public function __construct()
    {
        $this->userinstruments = new ArrayCollection();
        $this->deductions = new ArrayCollection();
        $this->reports = new ArrayCollection();
    }

    public function __toString()
    {
        if(null !== $this->teacher){
            return $this->teacher->getFullname();
        } else {
            return (string)$this->id;
        }
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getResumelnk(): ?string
    {
        return $this->resumelnk;
    }

    public function setResumelnk(string $resumelnk): self
    {
        $this->resumelnk = $resumelnk;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->teacher;
    }

    public function setUser(?User $teacher): self
    {
        // unset the owning side of the relation if necessary
        if ($teacher === null && $this->teacher !== null) {
            $this->teacher->setTeacherdata(null);
        }

        // set the owning side of the relation if necessary
        if ($teacher !== null && $teacher->getTeacherdata() !== $this) {
            $teacher->setTeacherdata($this);
        }

        $this->teacher = $teacher;

        return $this;
    }

    /**
     * @return Collection|UserInstrumentGrade[]
     */
    public function getUserinstruments(): Collection
    {
        return $this->userinstruments;
    }

    public function addUserinstruments(UserInstrumentGrade $userinstruments): self
    {
        if (!$this->userinstruments->contains($userinstruments)) {
            $this->userinstruments[] = $userinstruments;
            $userinstruments->setTeacherUserData($this);
        }

        return $this;
    }

    public function removeUserinstruments(UserInstrumentGrade $userinstruments): self
    {
        if ($this->userinstruments->removeElement($userinstruments)) {
            // set the owning side to null (unless already changed)
            if ($userinstruments->getTeacherUserData() === $this) {
                $userinstruments->setTeacherUserData(null);
            }
        }

        return $this;
    }

    public function getLessonmode(): ?string
    {
        return $this->lessonmode;
    }

    public function setLessonmode(?string $lessonmode): self
    {
        $this->lessonmode = $lessonmode;

        return $this;
    }

    public function getBranch(): ?string
    {
        return $this->branch;
    }

    public function setBranch(?string $branch): self
    {
        $this->branch = $branch;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(?string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getNextofkinname(): ?string
    {
        return $this->nextofkinname;
    }

    public function setNextofkinname(?string $nextofkinname): self
    {
        $this->nextofkinname = $nextofkinname;

        return $this;
    }

    public function getNextofkinphonenum(): ?string
    {
        return $this->nextofkinphonenum;
    }

    public function setNextofkinphonenum(?string $nextofkinphonenum): self
    {
        $this->nextofkinphonenum = $nextofkinphonenum;

        return $this;
    }

    public function getNextofkinemail(): ?string
    {
        return $this->nextofkinemail;
    }

    public function setNextofkinemail(?string $nextofkinemail): self
    {
        $this->nextofkinemail = $nextofkinemail;

        return $this;
    }

    public function getNextofkinrship(): ?string
    {
        return $this->nextofkinrship;
    }

    public function setNextofkinrship(?string $nextofkinrship): self
    {
        $this->nextofkinrship = $nextofkinrship;

        return $this;
    }

    /**
     * @return Collection|Deductions[]
     */
    public function getDeductions(): Collection
    {
        return $this->deductions;
    }

    public function addDeduction(Deductions $deduction): self
    {
        if (!$this->deductions->contains($deduction)) {
            $this->deductions[] = $deduction;
            $deduction->setTeacher($this);
        }

        return $this;
    }

    public function removeDeduction(Deductions $deduction): self
    {
        if ($this->deductions->removeElement($deduction)) {
            // set the owning side to null (unless already changed)
            if ($deduction->getTeacher() === $this) {
                $deduction->setTeacher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setTeacher($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getTeacher() === $this) {
                $report->setTeacher(null);
            }
        }

        return $this;
    }
}
