<?php

namespace App\Entity;

use App\Repository\ReportRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 */
class Report
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=StudentUserData::class, inversedBy="reports")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity=TeacherUserData::class, inversedBy="reports")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $teacher;

    /**
     * @ORM\ManyToOne(targetEntity=Term::class, inversedBy="reports")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $term;

    /**
     * @ORM\ManyToOne(targetEntity=UserInstrumentGrade::class, inversedBy="reports")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $uig;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $practice;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $rhythm;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $dexterity;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $tone;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $intonation;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $literacy;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $exercises;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $musicianship;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comments;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $timevalues;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $barlinesntimesig;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $notesonstave;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $resttiesndots;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $scalesnkeysig;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $degreesnintervals;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $tonictriads;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $perfdirections;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $cadences;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $figuredbass;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $partwriting;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $harmonizing;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $composition;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $scoreanalysis;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $instruments;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $ornaments;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $sent;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $addedOn;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStudent(): ?StudentUserData
    {
        return $this->student;
    }

    public function setStudent(?StudentUserData $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getTeacher(): ?TeacherUserData
    {
        return $this->teacher;
    }

    public function setTeacher(?TeacherUserData $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getTerm(): ?Term
    {
        return $this->term;
    }

    public function setTerm(?Term $term): self
    {
        $this->term = $term;

        return $this;
    }

    public function getUig(): ?UserInstrumentGrade
    {
        return $this->uig;
    }

    public function setUig(?UserInstrumentGrade $uig): self
    {
        $this->uig = $uig;

        return $this;
    }

    public function getPractice(): ?string
    {
        return $this->practice;
    }

    public function setPractice(?string $practice): self
    {
        $this->practice = $practice;

        return $this;
    }

    public function getRhythm(): ?string
    {
        return $this->rhythm;
    }

    public function setRhythm(?string $rhythm): self
    {
        $this->rhythm = $rhythm;

        return $this;
    }

    public function getDexterity(): ?string
    {
        return $this->dexterity;
    }

    public function setDexterity(string $dexterity): self
    {
        $this->dexterity = $dexterity;

        return $this;
    }

    public function getIntonation(): ?string
    {
        return $this->intonation;
    }

    public function setIntonation(?string $intonation): self
    {
        $this->intonation = $intonation;

        return $this;
    }

    public function getLiteracy(): ?string
    {
        return $this->literacy;
    }

    public function setLiteracy(?string $literacy): self
    {
        $this->literacy = $literacy;

        return $this;
    }

    public function getExercises(): ?string
    {
        return $this->exercises;
    }

    public function setExercises(?string $exercises): self
    {
        $this->exercises = $exercises;

        return $this;
    }

    public function getMusicianship(): ?string
    {
        return $this->musicianship;
    }

    public function setMusicianship(?string $musicianship): self
    {
        $this->musicianship = $musicianship;

        return $this;
    }

    public function getComments(): ?string
    {
        return $this->comments;
    }

    public function setComments(?string $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    public function getTimevalues(): ?string
    {
        return $this->timevalues;
    }

    public function setTimevalues(?string $timevalues): self
    {
        $this->timevalues = $timevalues;

        return $this;
    }

    public function getBarlinesntimesig(): ?string
    {
        return $this->barlinesntimesig;
    }

    public function setBarlinesntimesig(?string $barlinesntimesig): self
    {
        $this->barlinesntimesig = $barlinesntimesig;

        return $this;
    }

    public function getNotesonstave(): ?string
    {
        return $this->notesonstave;
    }

    public function setNotesonstave(?string $notesonstave): self
    {
        $this->notesonstave = $notesonstave;

        return $this;
    }

    public function getResttiesndots(): ?string
    {
        return $this->resttiesndots;
    }

    public function setResttiesndots(?string $resttiesndots): self
    {
        $this->resttiesndots = $resttiesndots;

        return $this;
    }

    public function getScalesnkeysig(): ?string
    {
        return $this->scalesnkeysig;
    }

    public function setScalesnkeysig(?string $scalesnkeysig): self
    {
        $this->scalesnkeysig = $scalesnkeysig;

        return $this;
    }

    public function getDegreesnintervals(): ?string
    {
        return $this->degreesnintervals;
    }

    public function setDegreesnintervals(?string $degreesnintervals): self
    {
        $this->degreesnintervals = $degreesnintervals;

        return $this;
    }

    public function getTonictriads(): ?string
    {
        return $this->tonictriads;
    }

    public function setTonictriads(?string $tonictriads): self
    {
        $this->tonictriads = $tonictriads;

        return $this;
    }

    public function getPerfdirections(): ?string
    {
        return $this->perfdirections;
    }

    public function setPerfdirections(?string $perfdirections): self
    {
        $this->perfdirections = $perfdirections;

        return $this;
    }

    public function getCadences(): ?string
    {
        return $this->cadences;
    }

    public function setCadences(?string $cadences): self
    {
        $this->cadences = $cadences;

        return $this;
    }

    public function getFiguredbass(): ?string
    {
        return $this->figuredbass;
    }

    public function setFiguredbass(?string $figuredbass): self
    {
        $this->figuredbass = $figuredbass;

        return $this;
    }

    public function getPartwriting(): ?string
    {
        return $this->partwriting;
    }

    public function setPartwriting(?string $partwriting): self
    {
        $this->partwriting = $partwriting;

        return $this;
    }

    public function getHarmonizing(): ?string
    {
        return $this->harmonizing;
    }

    public function setHarmonizing(?string $harmonizing): self
    {
        $this->harmonizing = $harmonizing;

        return $this;
    }

    public function getComposition(): ?string
    {
        return $this->composition;
    }

    public function setComposition(?string $composition): self
    {
        $this->composition = $composition;

        return $this;
    }

    public function getScoreanalysis(): ?string
    {
        return $this->scoreanalysis;
    }

    public function setScoreanalysis(?string $scoreanalysis): self
    {
        $this->scoreanalysis = $scoreanalysis;

        return $this;
    }

    public function getInstruments(): ?string
    {
        return $this->instruments;
    }

    public function setInstruments(?string $instruments): self
    {
        $this->instruments = $instruments;

        return $this;
    }

    public function getOrnaments(): ?string
    {
        return $this->ornaments;
    }

    public function setOrnaments(?string $ornaments): self
    {
        $this->ornaments = $ornaments;

        return $this;
    }

    public function getTone(): ?string
    {
        return $this->tone;
    }

    public function setTone(?string $tone): self
    {
        $this->tone = $tone;

        return $this;
    }

    public function getSent(): ?bool
    {
        return $this->sent;
    }

    public function setSent(?bool $sent): self
    {
        $this->sent = $sent;

        return $this;
    }

    public function getAddedOn(): ?\DateTimeInterface
    {
        return $this->addedOn;
    }

    public function setAddedOn(\DateTimeInterface $addedOn): self
    {
        $this->addedOn = $addedOn;

        return $this;
    }
}
