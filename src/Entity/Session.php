<?php

namespace App\Entity;

use App\Repository\SessionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SessionRepository::class)
 */
class Session
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $beginAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endAt;

    /**
     * @ORM\Column(type="date")
     */
    private $startingOn;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $numberofsessions;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $paid;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cleared;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="teachersessions")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $teacher;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="studentsessions")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity=Package::class, inversedBy="sessions")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $package;

    /**
     * @ORM\ManyToOne(targetEntity=UserInstrumentGrade::class, inversedBy="sessions")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $userInstrumentGrade;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $done;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $invoiced;

    public function __toString()
    {
        return $this->package->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBeginAt(): ?\DateTimeInterface
    {
        return $this->beginAt;
    }

    public function setBeginAt(\DateTimeInterface $beginAt): self
    {
        $this->beginAt = $beginAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(?\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getStartingOn(): ?\DateTimeInterface
    {
        return $this->startingOn;
    }

    public function setStartingOn(\DateTimeInterface $startingOn): self
    {
        $this->startingOn = $startingOn;

        return $this;
    }

    public function getNumberofsessions(): ?string
    {
        return $this->numberofsessions;
    }

    public function setNumberofsessions(string $numberofsessions): self
    {
        $this->numberofsessions = $numberofsessions;

        return $this;
    }

    public function getPaid(): ?string
    {
        return $this->paid;
    }

    public function setPaid(string $paid): self
    {
        $this->paid = $paid;

        return $this;
    }

    public function getCleared(): ?bool
    {
        return $this->cleared;
    }

    public function setCleared(bool $cleared): self
    {
        $this->cleared = $cleared;

        return $this;
    }

    public function getTeacher(): ?User
    {
        return $this->teacher;
    }

    public function setTeacher(?User $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getStudent(): ?User
    {
        return $this->student;
    }

    public function setStudent(?User $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getPackage(): ?Package
    {
        return $this->package;
    }

    public function setPackage(?Package $package): self
    {
        $this->package = $package;

        return $this;
    }

    public function getUserInstrumentGrade(): ?UserInstrumentGrade
    {
        return $this->userInstrumentGrade;
    }

    public function setUserInstrumentGrade(?UserInstrumentGrade $userInstrumentGrade): self
    {
        $this->userInstrumentGrade = $userInstrumentGrade;

        return $this;
    }

    public function getDone(): ?bool
    {
        return $this->done;
    }

    public function setDone(?bool $done): self
    {
        $this->done = $done;

        return $this;
    }

    public function getInvoiced(): ?bool
    {
        return $this->invoiced;
    }

    public function setInvoiced(?bool $invoiced): self
    {
        $this->invoiced = $invoiced;

        return $this;
    }
}
