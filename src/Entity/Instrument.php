<?php

namespace App\Entity;

use App\Repository\InstrumentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InstrumentRepository::class)
 */
class Instrument
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $availability;

    /**
     * @ORM\Column(type="text", length=255)
     */
    private $category;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Please, upload the product image as a png or jpg image.")
     * @Assert\File(mimeTypes={ "image/png", "image/jpg", "image/jpeg" })
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=UserInstrumentGrade::class, mappedBy="instrument")
     */
    private $userInstrumentGrades;

    public function __construct()
    {
        $this->instrumentGrades = new ArrayCollection();
        $this->userInstrumentGrades = new ArrayCollection();
    }

    public function  __toString(){
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAvailability(): ?string
    {
        return $this->availability;
    }

    public function setAvailability(string $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|UserInstrumentGrade[]
     */
    public function getUserInstrumentGrades(): Collection
    {
        return $this->userInstrumentGrades;
    }

    public function addUserInstrumentGrade(UserInstrumentGrade $userInstrumentGrade): self
    {
        if (!$this->userInstrumentGrades->contains($userInstrumentGrade)) {
            $this->userInstrumentGrades[] = $userInstrumentGrade;
            $userInstrumentGrade->setInstrument($this);
        }

        return $this;
    }

    public function removeUserInstrumentGrade(UserInstrumentGrade $userInstrumentGrade): self
    {
        if ($this->userInstrumentGrades->removeElement($userInstrumentGrade)) {
            // set the owning side to null (unless already changed)
            if ($userInstrumentGrade->getInstrument() === $this) {
                $userInstrumentGrade->setInstrument(null);
            }
        }

        return $this;
    }

}
