<?php

namespace App\Entity;

use App\Repository\DeductionsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DeductionsRepository::class)
 */
class Deductions
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity=TeacherUserData::class, inversedBy="deductions")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $teacher;

    /**
     * @ORM\Column(type="date")
     */
    private $addedOn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $balance;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $toDeduct;

    /**
     * @ORM\OneToMany(targetEntity=DeductionPayment::class, mappedBy="deduction")
     */
    private $deductionPayments;

    public function __construct()
    {
        $this->deductionPayments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->description;
    }
    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getTeacher(): ?TeacherUserData
    {
        return $this->teacher;
    }

    public function setTeacher(?TeacherUserData $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    public function getAddedOn(): ?\DateTimeInterface
    {
        return $this->addedOn;
    }

    public function setAddedOn(\DateTimeInterface $addedOn): self
    {
        $this->addedOn = $addedOn;

        return $this;
    }

    public function getBalance(): ?string
    {
        return $this->balance;
    }

    public function setBalance(string $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getToDeduct(): ?string
    {
        return $this->toDeduct;
    }

    public function setToDeduct(string $toDeduct): self
    {
        $this->toDeduct = $toDeduct;

        return $this;
    }

    /**
     * @return Collection|DeductionPayment[]
     */
    public function getDeductionPayments(): Collection
    {
        return $this->deductionPayments;
    }

    public function addDeductionPayment(DeductionPayment $deductionPayment): self
    {
        if (!$this->deductionPayments->contains($deductionPayment)) {
            $this->deductionPayments[] = $deductionPayment;
            $deductionPayment->setDeduction($this);
        }

        return $this;
    }

    public function removeDeductionPayment(DeductionPayment $deductionPayment): self
    {
        if ($this->deductionPayments->removeElement($deductionPayment)) {
            // set the owning side to null (unless already changed)
            if ($deductionPayment->getDeduction() === $this) {
                $deductionPayment->setDeduction(null);
            }
        }

        return $this;
    }
}
