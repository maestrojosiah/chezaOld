<?php

namespace App\Entity;

use App\Repository\StudentUserDataRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StudentUserDataRepository::class)
 */
class StudentUserData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lessonmode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="studentdata", cascade={"persist", "remove"})
     */
    private $student;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $branch;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sex;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nextofkinname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nextofkinphonenum;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nextofkinemail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nextofkinrship;

    /**
     * @ORM\OneToMany(targetEntity=UserInstrumentGrade::class, mappedBy="studentUserData")
     */
    private $userInstruments;

    /**
     * @ORM\OneToMany(targetEntity=Report::class, mappedBy="student")
     */
    private $reports;

    public function __construct()
    {
        $this->userInstruments = new ArrayCollection();
        $this->reports = new ArrayCollection();
    }

    public function __toString()
    {
        if(is_null($this->student)) {
            return $this->phone;
        }  
        return $this->student->getFullname();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLessonmode(): ?string
    {
        return $this->lessonmode;
    }

    public function setLessonmode(string $lessonmode): self
    {
        $this->lessonmode = $lessonmode;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getStudent(): ?User
    {
        return $this->student;
    }

    public function setStudent(?User $student): self
    {
        // unset the owning side of the relation if necessary
        if ($student === null && $this->student !== null) {
            $this->student->setStudentdata(null);
        }

        // set the owning side of the relation if necessary
        if ($student !== null && $student->getStudentdata() !== $this) {
            $student->setStudentdata($this);
        }

        $this->student = $student;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->age;
    }

    public function setAge(?string $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getBranch(): ?string
    {
        return $this->branch;
    }

    public function setBranch(string $branch): self
    {
        $this->branch = $branch;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->sex;
    }

    public function setSex(string $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    public function getNextofkinname(): ?string
    {
        return $this->nextofkinname;
    }

    public function setNextofkinname(string $nextofkinname): self
    {
        $this->nextofkinname = $nextofkinname;

        return $this;
    }

    public function getNextofkinphonenum(): ?string
    {
        return $this->nextofkinphonenum;
    }

    public function setNextofkinphonenum(string $nextofkinphonenum): self
    {
        $this->nextofkinphonenum = $nextofkinphonenum;

        return $this;
    }

    public function getNextofkinemail(): ?string
    {
        return $this->nextofkinemail;
    }

    public function setNextofkinemail(string $nextofkinemail): self
    {
        $this->nextofkinemail = $nextofkinemail;

        return $this;
    }

    public function getNextofkinrship(): ?string
    {
        return $this->nextofkinrship;
    }

    public function setNextofkinrship(string $nextofkinrship): self
    {
        $this->nextofkinrship = $nextofkinrship;

        return $this;
    }

    /**
     * @return Collection|UserInstrumentGrade[]
     */
    public function getUserInstruments(): Collection
    {
        return $this->userInstruments;
    }

    public function addUserInstruments(UserInstrumentGrade $userInstruments): self
    {
        if (!$this->userInstruments->contains($userInstruments)) {
            $this->userInstruments[] = $userInstruments;
            $userInstruments->setStudentUserData($this);
        }

        return $this;
    }

    public function removeUserInstruments(UserInstrumentGrade $userInstruments): self
    {
        if ($this->userInstruments->removeElement($userInstruments)) {
            // set the owning side to null (unless already changed)
            if ($userInstruments->getStudentUserData() === $this) {
                $userInstruments->setStudentUserData(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Report[]
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setStudent($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getStudent() === $this) {
                $report->setStudent(null);
            }
        }

        return $this;
    }

}
