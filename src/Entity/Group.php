<?php

namespace App\Entity;

use App\Repository\GroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GroupRepository::class)
 * @ORM\Table(name="`group`")
 */
class Group
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=Contact::class, mappedBy="contactgroup")
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity=Upload::class, mappedBy="uploadgroup")
     */
    private $file;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->file = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Contact[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setContactgroup($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getContactgroup() === $this) {
                $contact->setContactgroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Upload[]
     */
    public function getFile(): Collection
    {
        return $this->file;
    }

    public function addFile(Upload $file): self
    {
        if (!$this->file->contains($file)) {
            $this->file[] = $file;
            $file->setUploadgroup($this);
        }

        return $this;
    }

    public function removeFile(Upload $file): self
    {
        if ($this->file->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getUploadgroup() === $this) {
                $file->setUploadgroup(null);
            }
        }

        return $this;
    }

}
