<?php 

// src/Security/AccessDeniedHandler.php
namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
        $content = "If you're a newly registered student, your account has not been activated. Someone from the office will call you
        as soon as possible during working hours and assign you a teacher and a slot at the timetable. That's when you'll be able to 
        access the portal. <br /><br />You do not have permission to access the information that you have requested. 
        Kindly ask the admin to assign you the priviledges.<br /><br />
        For help, call 0720 962 288 / 0737 227 903.<br /><br /> <a href='/'>Homepage</a> <a href='/logout'>Logout</a>";

        return new Response($content, 403);
    }
}