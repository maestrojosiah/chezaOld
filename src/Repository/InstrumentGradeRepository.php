<?php

namespace App\Repository;

use App\Entity\InstrumentGrade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method InstrumentGrade|null find($id, $lockMode = null, $lockVersion = null)
 * @method InstrumentGrade|null findOneBy(array $criteria, array $orderBy = null)
 * @method InstrumentGrade[]    findAll()
 * @method InstrumentGrade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InstrumentGradeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InstrumentGrade::class);
    }

    // /**
    //  * @return InstrumentGrade[] Returns an array of InstrumentGrade objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InstrumentGrade
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
