<?php

namespace App\Repository;

use App\Entity\UserInstrumentGrade;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserInstrumentGrade|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserInstrumentGrade|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserInstrumentGrade[]    findAll()
 * @method UserInstrumentGrade[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserInstrumentGradeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserInstrumentGrade::class);
    }

    /**
     * @return UserInstrumentGrade[] Returns an array of UserInstrumentGrade objects
     */
    public function findWithNoSessions()
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.studentUserData != ?1')
            ->andWhere('SIZE(s.sessions) = 0')
            ->setParameter(1, 'null')
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult();
        ;
    }

    /**
     * @return UserInstrumentGrade[] Returns an array of UserInstrumentGrade objects
     */
    public function findUIGForStudents()
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.studentUserData != ?1')
            ->andWhere('SIZE(s.sessions) > 0')
            ->setParameter(1, 'null')
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult();
        ;
    }

    /**
     * @return UserInstrumentGrade[] Returns an array of UserInstrumentGrade objects
     */
    public function findUigInstruments()
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.instrument != ?1')
            ->andWhere('s.active != ?1')
            ->andWhere('SIZE(s.sessions) > 0')
            ->setParameter(1, 'null')
            ->setParameter(1, false)
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult();
        ;
    }

    /**
     * @return UserInstrumentGrade[] Returns an array of UserInstrumentGrade objects
     */
    public function findUIGForAllStudents()
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.studentUserData != ?1')
            ->setParameter(1, 'null')
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult();
        ;
    }

    /**
     * @return UserInstrumentGrade[] Returns an array of UserInstrumentGrade objects
     */
    public function findUIGForActiveStudents()
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.active = ?1')
            ->setParameter(1, true)
            ->orderBy('s.id', 'ASC')
            ->getQuery()
            ->getResult();
        ;
    }
    
    public function countForThisInstrument($instrument)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.instrument = :instrument')
            ->andWhere('u.active != ?1')
            ->setParameter(1, false)
            ->setParameter('instrument', $instrument)
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }


    // /**
    //  * @return UserInstrumentGrade[] Returns an array of UserInstrumentGrade objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserInstrumentGrade
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
