<?php

namespace App\Repository;

use App\Entity\Session;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Session|null find($id, $lockMode = null, $lockVersion = null)
 * @method Session|null findOneBy(array $criteria, array $orderBy = null)
 * @method Session[]    findAll()
 * @method Session[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Session::class);
    }
    
    // /**
    //  * @return Session[] Returns an array of Session objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @return Session[] Returns an array of Session objects
     */
    
    public function findForThisTerm($startingOn, $endingOn, $teacher)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.startingOn BETWEEN :from AND :to')
            ->andWhere('s.teacher = :teacher')
            ->setParameter('from', $startingOn)
            ->setParameter('to', $endingOn)
            ->setParameter('teacher', $teacher)
            ->orderBy('s.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function countForThisPackage($package)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.package = :package')
            ->setParameter('package', $package)
            ->select('count(s.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findTodaysLessons($teacher)
    {   
        $today = new \Datetime(date('Y-m-d'));
        return $this->createQueryBuilder('s')
            ->andWhere('s.startingOn = :today')
            ->andWhere('s.teacher = :teacher')
            ->andWhere('s.invoiced is null')
            ->setParameter('today', $today)
            ->setParameter('teacher', $teacher)
            ->orderBy('s.beginAt', 'ASC')
            // ->select('count(s.id)')
            ->getQuery()
            // ->getSingleScalarResult();
            ->getResult();
    }

    public function findFutureLessons($date, $teacher)
    {   
        $today = new \Datetime(date('Y-m-d'));
        return $this->createQueryBuilder('s')
            ->andWhere('s.startingOn >= :date')
            ->andWhere('s.teacher = :teacher')
            ->andWhere('s.invoiced is null')
            ->setParameter('date', $date)
            ->setParameter('teacher', $teacher)
            ->orderBy('s.beginAt', 'ASC')
            ->getQuery()
            ->getResult();
    }


    /*
    public function findOneBySomeField($value): ?Session
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
