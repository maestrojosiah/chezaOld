<?php

namespace App\Repository;

use App\Entity\Payment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Payment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payment[]    findAll()
 * @method Payment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    // /**
    //  * @return Payment[] Returns an array of Payment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
    public function countEntriesWithType($typestring)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.type = :type')
            ->setParameter('type', $typestring)
            ->select('count(p.type)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findSumOfTypeForUser($typestring, $user)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.type = :type')
            ->andWhere('p.user = :user')
            ->setParameter('type', $typestring)
            ->setParameter('user', $user)
            ->select('SUM(p.amount) as totalAmount')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findSumOfTypeForUserBeforeDate($typestring, $beforeDate, $user)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.type = :type')
            ->andWhere('p.user = :user')
            ->andWhere('p.doneOn < :beforeDate')
            ->setParameter('type', $typestring)
            ->setParameter('user', $user)
            ->setParameter('beforeDate', $beforeDate)
            ->select('SUM(p.amount) as totalAmount')
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function findAllWithinRange($dateTimeFrom, $dateTimeTo, $student){
        return $this->createQueryBuilder('p')
        ->andWhere('p.doneOn BETWEEN :from AND :to')
        ->andWhere('p.user = :student')
        ->setParameter('from', $dateTimeFrom)
        ->setParameter('to', $dateTimeTo)
        ->setParameter('student', $student)
        ->orderBy('p.id', 'ASC')
        ->getQuery()
        ->getResult()
    ;

    }

    public function findSumOfAllWithinRange($typestring, $dateTimeFrom, $dateTimeTo, $student){

        $frmdate = new \Datetime();
        $frmdate->modify("-$dateTimeFrom days");
        $todate = new \Datetime();
        $todate->modify("-$dateTimeTo days");

        return $this->createQueryBuilder('p')
        ->andWhere('p.type = :type')
        ->andWhere('p.doneOn BETWEEN :from AND :to')
        ->andWhere('p.user = :student')
        ->setParameter('from', $frmdate)
        ->setParameter('to', $todate)
        ->setParameter('type', $typestring)
        ->setParameter('student', $student)
        ->select('SUM(p.amount) as totalAmount')
        ->orderBy('p.id', 'ASC')
        ->getQuery()
        ->getOneOrNullResult()
    ;

    }
    /*
    public function findOneBySomeField($value): ?Payment
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
