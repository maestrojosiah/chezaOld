<?php

namespace App\EventSubscriber;

use App\Repository\SchoolTimeRepository;
use CalendarBundle\CalendarEvents;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class TrCalendarSubscriber implements EventSubscriberInterface
{
    private $schoolTimeRepository;
    private $router;

    public function __construct(
        SchoolTimeRepository $schoolTimeRepository,
        UrlGeneratorInterface $router
    ) {
        $this->schoolTimeRepository = $schoolTimeRepository;
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return [
            CalendarEvents::SET_DATA => 'onCalendarSetData',
        ];
    }

    public function onCalendarSetData(CalendarEvent $calendar)
    {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();
        $filters = $calendar->getFilters();

       switch($filters['calendar-id']) {
            case 'schoolTime-calendar':
                $this->fillCalendarWithSchoolTimes($calendar, $start, $end, $filters);
                break;
            // case 'other-calendar':
            //     $this->fillCalendarWithOthers($calendar, $start, $end, $filters);
            //     break;
       }
    }

    public function fillCalendarWithSchoolTimes(CalendarEvent $calendar, \DateTimeInterface $start, \DateTimeInterface $end, array $filters)
    {
        // Modify the query to fit to your entity and needs
        // Change schoolTime.beginAt by your start date property
        $schoolTimes = $this->schoolTimeRepository
            ->createQueryBuilder('schoolTime')
            ->where('schoolTime.beginAt BETWEEN :start and :end OR schoolTime.endAt BETWEEN :start and :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getResult()
        ;

        foreach ($schoolTimes as $schoolTime) {
            // this create the events with your data (here schoolTime data) to fill calendar
            $schoolTimeEvent = new Event(
                $schoolTime->getTeacher()->getFullname(),
                $schoolTime->getBeginAt(),
                $schoolTime->getEndAt() // If the end date is null or not defined, a all day event is created.
            );

            /*
             * Add custom options to events
             *
             * For more information see: https://fullcalendar.io/docs/event-object
             * and: https://github.com/fullcalendar/fullcalendar/blob/master/src/core/options.ts
             */

            $schoolTimeEvent->setOptions([
                'backgroundColor' => 'red',
                'borderColor' => 'red',
            ]);
            $schoolTimeEvent->addOption(
                'url',
                $this->router->generate('schoolTime_show', [
                    'id' => $schoolTime->getId(),
                ])
            );

            // finally, add the event to the CalendarEvent to fill the calendar
            $calendar->addEvent($schoolTimeEvent);
        }
    }
}