<?php

namespace App\EventSubscriber;


use App\Repository\SessionRepository;
use CalendarBundle\CalendarEvents;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CalendarSubscriber implements EventSubscriberInterface
{
    private $sessionRepository;
    private $router;

    public function __construct(SessionRepository $sessionRepository, UrlGeneratorInterface $router, TokenStorageInterface $tokenStorage ) {
        $this->tokenStorage = $tokenStorage;
        $this->sessionRepository = $sessionRepository;
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return [
            CalendarEvents::SET_DATA => 'onCalendarSetData',
        ];
    }

    public function onCalendarSetData(CalendarEvent $calendar)
    {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();
        $filters = $calendar->getFilters();
        

        switch (true){
            case stristr($filters['calendar-id'], 'admin-calendar'):
                $this->fillCalendarWithAllSessions($calendar, $start, $end, $filters);
                break;
            case stristr($filters['calendar-id'], 'student-calendar'):
                $this->fillCalendarWithStudentSessions($calendar, $start, $end, $filters);
                break;
            case stristr($filters['calendar-id'], 'teacher-calendar'):
                $this->fillCalendarWithTeacherSessions($calendar, $start, $end, $filters);
                break;
            case stristr($filters['calendar-id'], 'admin-vtr-calendar'):
                $teacherId = explode("-", $filters['calendar-id'])[3];
                $this->fillAdminCalendarWithTeacherSessions($calendar, $start, $end, $filters, $teacherId);
                break;
        }

    //     switch($filters['calendar-id']) {
    //         case 'admin-calendar':
    //             $this->fillCalendarWithAllSessions($calendar, $start, $end, $filters);
    //             break;
    //         case 'student-calendar':
    //             $this->fillCalendarWithStudentSessions($calendar, $start, $end, $filters);
    //             break;
    //         case 'teacher-calendar':
    //             $this->fillCalendarWithTeacherSessions($calendar, $start, $end, $filters);
    //             break;
    //    }
    }        
    
    public function fillCalendarWithAllSessions(CalendarEvent $calendar, \DateTimeInterface $start, \DateTimeInterface $end, array $filters) {
        // Modify the query to fit to your entity and needs
        // Change session.beginAt by your start date property
        $sessions = $this->sessionRepository
            ->createQueryBuilder('session')
            ->where('session.beginAt BETWEEN :start and :end OR session.endAt BETWEEN :start and :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getResult()
        ;

        foreach ($sessions as $session) {
            // this create the events with your data (here session data) to fill calendar
            $sessionEvent = new Event(
                $session->getUserInstrumentGrade(),
                $session->getBeginAt(),
                $session->getEndAt() // If the end date is null or not defined, a all day event is created.
            );

            /*
             * Add custom options to events
             *
             * For more information see: https://fullcalendar.io/docs/event-object
             * and: https://github.com/fullcalendar/fullcalendar/blob/master/src/core/options.ts
             */

            $sessionEvent->setOptions([
                'backgroundColor' => 'blue',
                'borderColor' => 'blue',
            ]);
            $sessionEvent->addOption(
                'url',
                $this->router->generate('session_show', [
                    'id' => $session->getId(),
                ])
            );

            // finally, add the event to the CalendarEvent to fill the calendar
            $calendar->addEvent($sessionEvent);
        }
    }

    public function fillCalendarWithStudentSessions(CalendarEvent $calendar, \DateTimeInterface $start, \DateTimeInterface $end, array $filters) {
        // Modify the query to fit to your entity and needs
        // Change session.beginAt by your start date property
        $token = $this->tokenStorage->getToken();
        $sessions = $this->sessionRepository
            ->createQueryBuilder('session')
            ->andWhere('session.beginAt BETWEEN :start and :end OR session.endAt BETWEEN :start and :end')
            ->andWhere('session.student = :student')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->setParameter('student', $token->getUser())
            ->getQuery()
            ->getResult()
        ;

        foreach ($sessions as $session) {
            // this create the events with your data (here session data) to fill calendar
            $sessionEvent = new Event(
                $session->getUserInstrumentGrade(),
                $session->getBeginAt(),
                $session->getEndAt() // If the end date is null or not defined, a all day event is created.
            );

            /*
             * Add custom options to events
             *
             * For more information see: https://fullcalendar.io/docs/event-object
             * and: https://github.com/fullcalendar/fullcalendar/blob/master/src/core/options.ts
             */

            $sessionEvent->setOptions([
                'backgroundColor' => 'blue',
                'borderColor' => 'blue',
            ]);
            $sessionEvent->addOption(
                'url',
                $this->router->generate('session_show', [
                    'id' => $session->getId(),
                ])
            );

            // finally, add the event to the CalendarEvent to fill the calendar
            $calendar->addEvent($sessionEvent);
        }
    }

    public function fillCalendarWithTeacherSessions(CalendarEvent $calendar, \DateTimeInterface $start, \DateTimeInterface $end, array $filters) {
        // Modify the query to fit to your entity and needs
        // Change session.beginAt by your start date property
        $token = $this->tokenStorage->getToken();
        $sessions = $this->sessionRepository
            ->createQueryBuilder('session')
            ->andWhere('session.beginAt BETWEEN :start and :end OR session.endAt BETWEEN :start and :end')
            ->andWhere('session.teacher = :teacher')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->setParameter('teacher', $token->getUser())
            ->getQuery()
            ->getResult()
        ;

        foreach ($sessions as $session) {
            // this create the events with your data (here session data) to fill calendar
            $sessionEvent = new Event(
                $session->getUserInstrumentGrade(),
                $session->getBeginAt(),
                $session->getEndAt() // If the end date is null or not defined, a all day event is created.
            );

            /*
             * Add custom options to events
             *
             * For more information see: https://fullcalendar.io/docs/event-object
             * and: https://github.com/fullcalendar/fullcalendar/blob/master/src/core/options.ts
             */

            $sessionEvent->setOptions([
                'backgroundColor' => 'blue',
                'borderColor' => 'blue',
            ]);
            $sessionEvent->addOption(
                'url',
                $this->router->generate('session_show', [
                    'id' => $session->getId(),
                ])
            );

            // finally, add the event to the CalendarEvent to fill the calendar
            $calendar->addEvent($sessionEvent);
        }
    }

    public function fillAdminCalendarWithTeacherSessions(CalendarEvent $calendar, \DateTimeInterface $start, \DateTimeInterface $end, array $filters, $teacherId) {
        // Modify the query to fit to your entity and needs
        // Change session.beginAt by your start date property

        $sessions = $this->sessionRepository
            ->createQueryBuilder('session')
            ->andWhere('session.beginAt BETWEEN :start and :end OR session.endAt BETWEEN :start and :end')
            ->andWhere('session.teacher = :teacher')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->setParameter('teacher', $teacherId)
            ->getQuery()
            ->getResult()
        ;

        foreach ($sessions as $session) {
            // this create the events with your data (here session data) to fill calendar
            $sessionEvent = new Event(
                $session->getUserInstrumentGrade(),
                $session->getBeginAt(),
                $session->getEndAt() // If the end date is null or not defined, a all day event is created.
            );

            /*
             * Add custom options to events
             *
             * For more information see: https://fullcalendar.io/docs/event-object
             * and: https://github.com/fullcalendar/fullcalendar/blob/master/src/core/options.ts
             */

            $sessionEvent->setOptions([
                'backgroundColor' => 'blue',
                'borderColor' => 'blue',
            ]);
            $sessionEvent->addOption(
                'url',
                $this->router->generate('session_show', [
                    'id' => $session->getId(),
                ])
            );

            // finally, add the event to the CalendarEvent to fill the calendar
            $calendar->addEvent($sessionEvent);
        }
    }

}