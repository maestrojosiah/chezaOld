<?php

namespace App\Form;

use App\Entity\Session;
use App\Entity\User;
use App\Entity\Package;
use App\Entity\UserInstrumentGrade;
use App\Repository\UserRepository;
use App\Repository\UserInstrumentGradeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class SessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startingOn', DateType::class, [
                'html5' => false,
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'format' => 'yyyy-MM-dd'
            ])
            ->add('beginAt', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-timepicker'],
                'input_format' => 'H:i:s'
            ])
            ->add('endAt', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-timepicker'],
                'input_format' => 'H:i:s'
            ])
            ->add('numberofsessions', ChoiceType::class, [
                'choices' => [
                    'Twelve (Default)' => '12',
                    'One' => '1',
                    'Two' => '2',
                    'Three' => '3',
                    'Four' => '4',
                    'Five' => '5',
                    'Six' => '6',
                    'Seven' => '7',
                    'Eight' => '8',
                    'Nine' => '9',
                    'Ten' => '10',
                    'Eighteen' => '18',
                    'Twenty Four' => '24',
                    'Thirty Six' => '36',
                    'Fourty Eight' => '48',
                    'Sixty' => '60',
                    'Seventy Two' => '72',
                ],
                'data' => 12,
            ])
            ->add('teacher', EntityType::class, [
                'class' => User::class,
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.usertype = ?1')
                        ->orderBy('u.id', 'ASC')
                        ->setParameter(1, 'teacher');
                },
            ])
            ->add('userinstrumentgrade', EntityType::class, [
                'class' => UserInstrumentGrade::class,
                'query_builder' => function (UserInstrumentGradeRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->andWhere('u.studentUserData != ?1')
                        ->andWhere('u.active != ?2')
                        ->andWhere('SIZE(u.sessions) = 0')
                        ->orderBy('u.id', 'DESC')
                        ->setParameter(1, 'null')
                        ->setParameter(2, false);
                },
            ])
            ->add('package', EntityType::class, [
                'class' => Package::class,
                'placeholder' => 'Choose a Package',
            ])
            ->add('paid')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Session::class,
        ]);
    }
}
