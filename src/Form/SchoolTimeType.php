<?php

namespace App\Form;

use App\Entity\SchoolTime;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SchoolTimeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('day', ChoiceType::class, [
                'choices' => [
                    'Monday' => 'Monday-0',
                    'Tuesday' => 'Tuesday-1',
                    'Wednesday' => 'Wednesday-2',
                    'Thursday' => 'Thursday-3',
                    'Friday' => 'Friday-4',
                    'Saturday' => 'Saturday-5',
                    'Sunday' => 'Sunday-6',
                ],
            ])
            ->add('beginAt', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-timepicker'],
                'input_format' => 'H:i:s'
            ])
            ->add('endAt', TimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-timepicker'],
                'input_format' => 'H:i:s'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SchoolTime::class,
        ]);
    }
}
