<?php

namespace App\Form;

use App\Entity\Report;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('practice')
            ->add('rhythm')
            ->add('dexterity')
            ->add('tone')
            ->add('intonation')
            ->add('literacy')
            ->add('exercises')
            ->add('musicianship')
            ->add('comments')
            ->add('timevalues')
            ->add('barlinesntimesig')
            ->add('notesonstave')
            ->add('resttiesndots')
            ->add('scalesnkeysig')
            ->add('degreesnintervals')
            ->add('tonictriads')
            ->add('perfdirections')
            ->add('cadences')
            ->add('figuredbass')
            ->add('partwriting')
            ->add('harmonizing')
            ->add('composition')
            ->add('scoreanalysis')
            ->add('instruments')
            ->add('ornaments')
            ->add('student')
            ->add('teacher')
            ->add('term')
            ->add('uig')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Report::class,
        ]);
    }
}
