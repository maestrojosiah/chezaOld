<?php

namespace App\Form;

use App\Entity\UserInstrumentGrade;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserInstrumentGradeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('studentUserData')
            ->add('teacherUserData')
            ->add('grade')
            ->add('instrument')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserInstrumentGrade::class,
        ]);
    }
}
