<?php

namespace App\Form;

use App\Entity\Term;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class TermType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('termnumber')
            ->add('startingOn', DateTimeType::class, [
                'html5' => false,
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'format' => 'yyyy-MM-dd'
            ])
            ->add('endingOn', DateTimeType::class, [
                'html5' => false,
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'format' => 'yyyy-MM-dd'
            ])
            ->add('halftermstart', DateTimeType::class, [
                'html5' => false,
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'format' => 'yyyy-MM-dd'
            ])
            ->add('halftermend', DateTimeType::class, [
                'html5' => false,
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'format' => 'yyyy-MM-dd'
            ])
            ->add('holidaystart', DateTimeType::class, [
                'html5' => false,
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'format' => 'yyyy-MM-dd'
            ])
            ->add('holidayend', DateTimeType::class, [
                'html5' => false,
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'format' => 'yyyy-MM-dd'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Term::class,
        ]);
    }
}
