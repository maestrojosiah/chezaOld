<?php

namespace App\Form;

use App\Entity\Instrument;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;

class InstrumentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            // ->add('description', TextareaType::class, [
            //     'attr' => ['class' => 'tinymce'],
            // ])
            ->add('description', CKEditorType::class, array(
                'config' => array(
                    'uiColor' => '#ffffff',
                    //...
                ),
            ))
            ->add('image', FileType::class, array('label' => 'Image (PNG/JPG file)', 'data_class' => null))
            ->add('category', ChoiceType::class, [
                'choices' => [
                    'Instrument' => 'instrument',
                    'Course' => 'course',
                    'Group classes' => 'traditional',
                ]
            ])
            ->add('availability', ChoiceType::class, [
                'choices' => [
                    'Available' => 'available',
                    'Online mode only' => 'online-only',
                    'Physical lessons only' => 'offline-only',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Instrument::class,
        ]);
    }
}
