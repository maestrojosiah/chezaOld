<?php

namespace App\Form;

use App\Entity\DeductionPayment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DeductionPaymentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('paidAmt')
            ->add('paidOn')
            ->add('balance')
            ->add('cleared')
            ->add('teacher')
            ->add('deduction')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DeductionPayment::class,
        ]);
    }
}
