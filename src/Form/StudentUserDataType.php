<?php

namespace App\Form;

use App\Entity\StudentUserData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentUserDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('regnum')
            ->add('lessonmode')
            ->add('phone')
            ->add('age')
            ->add('branch')
            ->add('sex')
            ->add('nextofkinname')
            ->add('nextofkinphonenum')
            ->add('nextofkinemail')
            ->add('nextofkinrship')
            ->add('student')
            ->add('instruments')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => StudentUserData::class,
        ]);
    }
}
