<?php

namespace App\Form;

use App\Entity\Deductions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class DeductionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description')
            ->add('amount')
            ->add('addedOn', DateTimeType::class, [
                'html5' => false,
                'input' => 'datetime',
                'widget' => 'single_text',
                'attr' => ['class' => 'js-datepicker'],
                'format' => 'yyyy-MM-dd'
            ])
            ->add('teacher')
            ->add('toDeduct')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Deductions::class,
        ]);
    }
}
