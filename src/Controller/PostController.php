<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;
use Sonata\SeoBundle\Seo\SeoPageInterface;

/**
 * @Route("/post")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="post_index", methods={"GET"})
     */
    public function index(PostRepository $postRepository, EntityManagerInterface $em, PaginatorInterface $paginator, Request $request): Response
    {

        $dql   = "SELECT p FROM App:Post p ORDER BY p.id DESC";
        $query = $em->createQuery($dql);
    
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );

        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->findAll(),
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/pb/articles", name="public_post_index", methods={"GET"})
     */
    public function public_index(PostRepository $postRepository, EntityManagerInterface $em, PaginatorInterface $paginator, Request $request, SeoPageInterface $seoPage): Response
    {

        $dql   = "SELECT p FROM App:Post p ORDER BY p.id DESC";
        $query = $em->createQuery($dql);
    
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            9 /*limit per page*/
        );

        $seoPage
            ->setTitle("Cheza Music School | Blog")
            ->addMeta('name', 'keywords', 'Cheza Music School Blog Articles')
            ->addMeta('name', 'description', 'Cheza Music School blog. Musical articles.')
            ->addMeta('property', 'og:title', "Cheza Music School | Blog")
            ->addMeta('property', 'og:site_name', "Cheza Music School | Blog")
            ->addMeta('property', 'og:type', 'Blog')
            ->addMeta('property', 'og:url',  $this->generateUrl('post_index'))
            ->addMeta('property', 'og:description', 'Cheza Music School blog. Musical articles.')
        ;
            
        return $this->render('post/public_index.html.twig', [
            'posts' => $postRepository->findAll(),
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/new", name="post_new", methods={"GET","POST"})
     */
    public function new(Request $request, SluggerInterface $slugger): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var uploadedFile $uploadedFile */
            $uploadedFile = $form->get('image')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($uploadedFile) {
                $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile->move(
                        $this->getParameter('image_files'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'uploadedFilename' property to store the PDF file name
                // instead of its contents
                $post->setImage($newFilename);
            }
            $post->setCreatedAt(new \Datetime());
            $post->setUpdatedAt(new \Datetime());
            $post->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('post_index');
        }

        return $this->render('post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{title}", name="post_show", methods={"GET"})
     */
    public function show(Post $post, PostRepository $postRepo, SeoPageInterface $seoPage): Response
    {
        $imageLink = $this->toImageLink($post->getImage());

        $seoPage
            ->setTitle($post->getTitle())
            ->addMeta('name', 'keywords', $post->getTitle())
            ->addMeta('name', 'title', $post->getTitle())
            ->addMeta('name', 'description', substr($post->getContent(), 0, 150))
            ->addMeta('name', 'author', $post->getUser()->getFullname())
            ->addMeta('property', 'og:title', $post->getTitle().' | Cheza Blog')
            ->addMeta('property', 'og:type', 'article')
            ->addMeta('property', 'og:image', $imageLink)
            ->addMeta('property', 'og:url',  "https://chezamusicschool.co.ke/post/".$post->getId()."/".$post->getTitle())
            ->addMeta('property', 'og:description', substr($post->getContent(), 0 ,150))
        ;
        
        return $this->render('post/show.html.twig', [
            'post' => $post,
            'posts' => $postRepo->findThreeLatestPosts(),
        ]);
    }

    public function toImageLink($content){

        if (preg_match('/(\.jpg|\.png|\.bmp)$/', $content)) {
            return "https://chezamusicschool.co.ke/dist/assets/images/" . $content;
        } elseif (strpos($content, "youtube.com") !== false) {
            return "https://img.youtube.com/vi/" . substr($content, -11)."/0.jpg";
        } elseif (strpos($content, "youtu.be") !== false) {
            return "https://img.youtube.com/vi/" . substr($content, -11)."/0.jpg";
        } else {
            return "https://chezamusicschool.co.ke/dist/assets/images/" . $content;
        }

    }

    /**
     * @Route("/{id}/article/edit", name="post_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Post $post, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var uploadedFile $uploadedFile */
            $uploadedFile = $form->get('image')->getData();
            $postOldImage = $_POST['old_file'];

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($uploadedFile) {
                $this->rm($post, $postOldImage, 'prefix_to_delete_img', false);
                $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();
    
                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile->move(
                        $this->getParameter('image_files'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'uploadedFilename' property to store the PDF file name
                // instead of its contents
                $post->setImage($newFilename);
            }
            $post->setCreatedAt(new \Datetime());
            $post->setUpdatedAt(new \Datetime());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('post_index');
        }

        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="post_delete", methods={"POST"})
     */
    public function delete(Request $request, Post $post): Response
    {
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $this->rm($post, $post->getImage(), 'prefix_to_delete_img');
        }

        return $this->redirectToRoute('post_index', [], Response::HTTP_SEE_OTHER);
    }

    public function rm($entity, $filename, $delete_path, $delEntity = true){
        $prefix_to_delete = $this->getParameter($delete_path);
        $existing_file = $prefix_to_delete . $filename;
        if(is_file($existing_file) && file_exists($existing_file)){ unlink($existing_file); }   
        if($delEntity == true){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();    
        } 
    }

}
