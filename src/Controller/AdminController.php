<?php

namespace App\Controller;

use App\Entity\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\UserRepository;
use App\Repository\GroupRepository;
use App\Repository\SchoolTimeRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\InstrumentRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\PackageRepository;
use App\Repository\PaymentRepository;
use App\Repository\SessionRepository;
use App\Repository\TemplateRepository;
use App\Repository\ContactRepository;
use App\Repository\TermRepository;
use App\Repository\CommunicationRepository;
use App\Repository\AttachmentRepository;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin")
     */
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        
        return $this->render('admin/profile.html.twig', [
            'admin' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/communications/sms/email/{template_id}", defaults={"template_id"=null}, name="admin-communications")
     */
    public function communications(TemplateRepository $templateRepo, AttachmentRepository $attachRepo, CommunicationRepository $commRepository, GroupRepository $groupRepo, $template_id = null): Response
    {
        if(null !== $template_id){
            $template = $templateRepo->findOneById($template_id);
        } else {
            $template = null;
        }
        $groups = $groupRepo->findAll();
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $communications = $commRepository->findHistory();
        $attachments = $attachRepo->findAll();
        $templates = $templateRepo->findAll();

        return $this->render('admin/communication.html.twig', [
            'admin' => $this->getUser(),
            'templates' => $templates,
            'template' => $template,
            'groups' => $groups,
            'communications' => $communications,
            'attachments' => $attachments,
        ]);
    }

    /**
     * @Route("/communications/notify/sms/{send_to}/{session_id}/{template_id}", name="admin-communications-notify")
     */
    public function communicationsFromLink(
        UserRepository $userRepo, 
        SessionRepository $sessionRepo, 
        TemplateRepository $templateRepo, 
        AttachmentRepository $attachRepo, 
        CommunicationRepository $commRepository, 
        GroupRepository $groupRepo, 
        $send_to = null, 
        $template_id = null,
        $session_id): Response
    {
        if(null !== $template_id){
            $template = $templateRepo->findOneById($template_id);
        } else {
            $template = null;
        }
        $user = $userRepo->findOneById($send_to);
        $groups = $groupRepo->findAll();
        $session = $sessionRepo->findOneById($session_id);
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $communications = $commRepository->findHistory();
        $attachments = $attachRepo->findAll();
        $templates = $templateRepo->findAll();
        $uig = $session->getUserInstrumentGrade();
        $uigsessions = $uig->getSessions();
        $days = [];
        foreach ($uigsessions as $key => $sess) {
            $beginAt = $sess->getBeginAt();
            $endAt = $sess->getEndAt();
            $days[$beginAt->format('H:i').'-'.$endAt->format('H:i').'-'.$beginAt->format('l')] = $beginAt->format('l');
        }

      	$who = "";
        if(strlen($session->getUserInstrumentGrade()->getSiblingname()) > 0){
        	$who = "Parent\'s";
        } else {
        	$who = $session->getStudent()->getStudentdata()->getSex() == 'male' ? 'His' : 'Her';
        }
      	$daysString = "";
      	foreach($days as $key => $day){ 
          	$daysString .= ucfirst($day)."s";
          	$daysString .= " from ";
        	if($key){
              $daysString .= date('H:i', strtotime(explode("-", $key)[0])); 
            }
          	$daysString .= " to ";
        	if($key){
              $daysString .= date('H:i', strtotime(explode("-", $key)[1])); 
            }
          	$daysString .= ", ";
          
          	
        }
      
      	if($user->getUsertype() == 'teacher'){
          $msg = "Dear ".$session->getTeacher()->getFullname() . ".Please note you have a new " . $session->getUserInstrumentGrade()->getInstrument()->getName()." student on ".
          $daysString . "First lesson: " . $session->getUserInstrumentGrade()->getSessions()[0]->getStartingOn()->format('l, jS F Y').". ". $who . " name is " . 
          $session->getStudent()->getFullname() . " ,tel: ". $session->getStudent()->getStudentData()->getPhone().".Regards.";
        } else {
          $msg = "Dear ". $session->getStudent()->getFullname() . ", Thank you for choosing the Conservatoire for your music lessons, your " . $session->getUserInstrumentGrade()->getInstrument()->getName() . " lessons will be on " .$daysString . "First lesson: " . $session->getUserInstrumentGrade()->getSessions()[0]->getStartingOn()->format('l, jS F Y') . ". We have assigned you Teacher ".
          $session->getTeacher()->getFullname() . ". phone no is " . $session->getTeacher()->getTeacherData()->getPhone() . ". they use room [?]. Payment can be made Via Mpesa Paybill Business no:316550 Account No: Name of student." .
	      " Regards, 
		  Samuel Karori ";
        }
      
        return $this->render('admin/communication_link.html.twig', [
            'admin' => $this->getUser(),
            'templates' => $templates,
            'template' => $template,
            'session' => $session,
            'user' => $user,
            'msg' => $msg,          
            'days' => array_unique($days),
            'groups' => $groups,
            'communications' => $communications,
            'attachments' => $attachments,
        ]);
    }

    /**
     * @Route("/communications/template/edit/{template_id}", name="edit_template_page")
     */
    public function editTemplate(TemplateRepository $templateRepo, $template_id): Response
    {

        $template = $templateRepo->findOneById($template_id);
        
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $templates = $templateRepo->findAll();

        return $this->render('admin/edit_template.html.twig', [
            'template' => $template,
        ]);
    }

    /**
     * @Route("/dashboard", name="admin-dashboard")
     */
    public function dashboard(UserRepository $userRepo, PackageRepository $packageRepo, ContactRepository $contactRepo, InstrumentRepository $instrumentRepo, SessionRepository $sessionRepo, UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $data = [];
        $insarr = [];
        $durationarr = [];
        $contactscount = $contactRepo->countAll();
        $studentscount = $userRepo->countUsersOfType('student');
        $teacherscount = $userRepo->countUsersOfType('teacher');
        $countinactive = $userRepo->countActive(false);
        $instruments = $instrumentRepo->findAll();
        $uigs = $uigRepo->findUigInstruments();
        $packages = $packageRepo->findAll();
        $instrumentscount = 0;
        $sessions = $sessionRepo->findAll();
        $data['countinactive'] = $countinactive;
        $data['studentscount'] = $studentscount;
        $data['teacherscount'] = $teacherscount;
        $data['instruments'] = $instruments;
        $data['sessions'] = $sessions;
        $data['contactscount'] = $contactscount;
        $data['uigs'] = $uigs;
        $teachers = $userRepo->findBy(
            array('usertype' => 'teacher'),
            array('id' => 'ASC'),
            8,
        );
        $admins = $userRepo->findBy(
            array('usertype' => 'admin'),
            array('id' => 'ASC'),
            4,
        );
        $students = $userRepo->findBy(
            array('usertype' => 'student'),
            array('id' => 'DESC'),
            8,
        );
        $data['teachers'] = $teachers;
        $data['students'] = $students;
        $data['admins'] = $admins;
        foreach ($uigs as $key => $uig) {
            $inscount = $uigRepo->countForThisInstrument($uig->getInstrument());
            $insarr[$uig->getInstrument()->getName()] = $inscount."-".$uig->getInstrument()->getId();
        }
        foreach ($packages as $key => $package) {
            $durationarr[$package->getName()] = $sessionRepo->countForThisPackage($package)."-".$package->getDuration().$package->getId();
        }
        $instrumentscount += array_sum($insarr);
        $data['instrumentscount'] = $instrumentscount;
        $data['insarr'] = $insarr;
        $data['durationarr'] = $durationarr;
        return $this->render('admin/dashboard.html.twig', [
            'data' => $data,
        ]);
    }

    /**
     * @Route("/register/new/student", name="student-registration-admin")
     */
    public function studentReg(InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $data = [];
        $instruments = $instrumentRepo->findBy(
            array('category' => 'instrument'),
            array('id' => 'ASC')
        );
        $courses = $instrumentRepo->findBy(
            array('category' => 'course'),
            array('id' => 'ASC')
        );
        $traditional = $instrumentRepo->findBy(
            array('category' => 'traditional'),
            array('id' => 'ASC')
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        $instrumentGrades = $instrumentGradeRepo->findAll();
        $data['instrumentGrades'] = $instrumentGrades;

        return $this->render('admin/student_reg.html.twig', [
            'student' => $this->getUser(),
            'data' => $data,
        ]);
    }

    /**
     * @Route("/register/edit/student/{studentid}", name="student-edit-admin")
     */
    public function studentEdit(UserRepository $userRepo, InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo, $studentid): Response
    {
        
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $student = $userRepo->findOneById($studentid);
        $uigs = $student->getStudentdata()->getUserInstruments();
        $myInstruments = [];
        $myGrades = [];
        foreach ($uigs as $uig ) {
            $myInstruments[] = $uig->getInstrument()->getId();
            $myGrades[] = $uig->getInstrument()->getId().'-'.$uig->getGrade()->getId();
        }

        $data = [];
        $instruments = $instrumentRepo->findBy(
            array('category' => 'instrument'),
            array('id' => 'ASC')
        );
        $courses = $instrumentRepo->findBy(
            array('category' => 'course'),
            array('id' => 'ASC')
        );
        $traditional = $instrumentRepo->findBy(
            array('category' => 'traditional'),
            array('id' => 'ASC')
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;
        $data['myInstruments'] = $myInstruments;
        $data['myGrades'] = $myGrades;

        $instrumentGrades = $instrumentGradeRepo->findAll();
        $data['instrumentGrades'] = $instrumentGrades;

        return $this->render('admin/student_edit.html.twig', [
            'student' => $student,
            'data' => $data,
        ]);
    }

    /**
     * @Route("/view/teacher/calendar/{teacher_id}", name="admin_session_calendar", methods={"GET"})
     */
    public function calendar(UserRepository $userRepo, SchoolTimeRepository $schoolTimeRepo, $teacher_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $teacher = $userRepo->findOneById($teacher_id);
        $schedules = $schoolTimeRepo->findByTeacher($teacher);
        return $this->render('admin/calendar.html.twig', [
            'teacher_id' => $teacher_id,
            'teacher' => $teacher,
            'schedules' => $schedules,
        ]);
    }
    
    /**
     * @Route("/teachers/list", name="admin-teachers-list")
     */
    public function teachersList(UserRepository $userRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $hourly = [];
        $fourtyfive = [];
        $thirty = [];
        $listofteachers = [];
        $teachers = $userRepo->findByUsertype('teacher');
        foreach ($teachers as $teacher) {
            foreach ($teacher->getTeachersessions() as $session) {
                if($session->getDone() == false){
                    if($session->getPackage()->getDuration() == '60'){
                        $hourly[$teacher->getId().'-'.$session->getUserInstrumentGrade()->getId().'-'.$session->getPackage()->getId()] = $session->getPackage()->getId();
                    } elseif ($session->getPackage()->getDuration() == '45'){
                        $fourtyfive[$teacher->getId().'-'.$session->getUserInstrumentGrade()->getId().'-'.$session->getPackage()->getId()] = $session->getPackage()->getId();
                    } elseif ($session->getPackage()->getDuration() == '30'){
                        $thirty[$teacher->getId().'-'.$session->getUserInstrumentGrade()->getId().'-'.$session->getPackage()->getId()] = $session->getPackage()->getId();
                    }
                }
            }
            $listofteachers[$teacher->getId()] = ['hourly' => count($hourly), 'fourtyfive' => count($fourtyfive), 'thirty' => count($thirty), 'user' => $teacher];
            $hourly = [];
            $fourtyfive = [];
            $thirty = [];
        }
        
        return $this->render('admin/teacherslist.html.twig', [
            'controller_name' => 'StudentController',
            'teachers' => $teachers,
            'hourly' => $hourly,
            'fourtyfive' => $fourtyfive,
            'thirty' => $thirty,
            'listofteachers' => $listofteachers,
        ]);
    }

    /**
     * @Route("/students/list", name="admin-students-list")
     */
    public function studentsList(UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $students = $userRepo->findByUsertype('student');
        $uigs = $uigRepo->findUIGForStudents();
        return $this->render('admin/studentslist.html.twig', [
            'controller_name' => 'StudentController',
            'students' => $students,
            'uigs' => $uigs,
        ]);
    }

    /**
     * @Route("/payments/list/{uig_id}", name="admin-payments-list")
     */
    public function paymentsList(UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo, $uig_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $uig = $uigRepo->findOneById($uig_id);
        $student = $uig->getStudentUserData()->getStudent();
        $payments = $student->getPayments();
        return $this->render('admin/payments.html.twig', [
            'student' => $student,
            'uig' => $uig,
            'payments' => $payments,
        ]);
    }

    /**
     * @Route("/security/role", name="assign_access_roles")
     */
    public function assign_admin_role(UserRepository $userRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $admins = $userRepo->findBy(
            array('usertype' => 'admin'),
            array('id' => 'DESC')
        );

        $students = $userRepo->findBy(
            array('usertype' => 'student'),
            array('id' => 'DESC')
        );

        $teachers = $userRepo->findBy(
            array('usertype' => 'teacher'),
            array('id' => 'DESC')
        );

        // $admins = $userRepo->findByUsertype('admin');
        // $students = $userRepo->findByUsertype('student');
        // $teachers = $userRepo->findByUsertype('teacher');
        return $this->render('admin/roles.html.twig', [
            'admins' => $admins,
            'students' => $students,
            'teachers' => $teachers,
        ]);
    }

    /**
     * @Route("/admin/security/now/role/add/new/{usertype}/{user_id}/{roletype}", name="add_role")
     */
    public function assign_new_role(UserRepository $userRepo, $usertype, $user_id, $roletype): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $userRepo->findOneById($user_id);
        $has_role = $user->hasRole($user->getRoles(), $roletype);
        if($has_role){
            $user->setRoles([]);
        } else {
            $roles = [];
            $roles[] = $roletype;
            $user->setRoles([$roletype]);
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($user); 
        $em->flush();

        return $this->redirectToRoute('assign_access_roles');
    }

    /**
     * @Route("/admin/security/now/activate/{user_id}", name="toggle_activate")
     */
    public function toggleStudentActivate(UserRepository $userRepo, $user_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $userRepo->findOneById($user_id);
        $active = $user->getActive();
        $uigActives = $user->getStudentdata()->getUserInstruments();
        $em = $this->getDoctrine()->getManager();

        $activeText = "";
        if($active){
            $user->setActive(false);
            $user->setIsVerified(false);
            foreach ($uigActives as $uigActive ) {
                $uigActive->setActive(false);
                $em->persist($uigActive); 
            }
            $activeText = "Inactive";
        } else {
            $user->setActive(true);
            $user->setIsVerified(true);
            foreach ($uigActives as $uigActive ) {
                $uigActive->setActive(true);
                $em->persist($uigActive); 
            }
            $activeText = "Active";
        }
        
        
        $em->persist($user); 
        
        $em->flush();
        return $this->redirectToRoute('assign_access_roles');
        // return $this->render('default/test.html.twig', [
        //     'test' => $uigActive,
        // ]);
    }

    /**
     * @Route("/admin/statement/preview", name="preview_statement", methods={"GET"})
     */
    public function previewStatement(UserInstrumentGradeRepository $userInstrumentGradeRepo, PaymentRepository $paymentRepo, TermRepository $termRepo): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if (isset($_SERVER['QUERY_STRING']) && $_GET){
            $dateFrom = explode("=", explode("&", $_SERVER['QUERY_STRING'])[0])[1];
            $dateTimeFrom = new \Datetime($dateFrom." 00:00:00");
            $dateTo = explode("=", explode("&", $_SERVER['QUERY_STRING'])[1])[1];
            $dateTimeTo = new \Datetime($dateTo." 23:59:59");
            $uig_ids = explode("-", explode("=", explode("&", $_SERVER['QUERY_STRING'])[2])[1]);
            $uig_ids = array_unique($uig_ids);
            $uigs = [];
            foreach ($uig_ids as $uigid ) {
                $uigs[] = $userInstrumentGradeRepo->findOneById($uigid);
            }
            
        } else {
            $dateFrom = "";
            $dateTo = "";
            $uigs = null;
        }
        $dates=[];
        $student = $uigs[0]->getStudentUserData()->getStudent();
        $payments = $paymentRepo->findAllWithinRange($dateTimeFrom, $dateTimeTo, $student);
        $totalInvoicesBD = $paymentRepo->findSumOfTypeForUserBeforeDate("inv", $dateTimeFrom, $student)['totalAmount'];
        $totalPaymentsBD = $paymentRepo->findSumOfTypeForUserBeforeDate("pmt", $dateTimeFrom, $student)['totalAmount'];
        $totalInvoices = $paymentRepo->findSumOfTypeForUser("inv", $student)['totalAmount'];
        $totalPayments = $paymentRepo->findSumOfTypeForUser("pmt", $student)['totalAmount'];
        $dateTimeFrom->setDate(date('Y'), $dateTimeFrom->format('m'), $dateTimeFrom->format('d'));
        $currentTerm = $termRepo->findCurrentTerm($dateTimeFrom);
        if(null !== $currentTerm){
            $lastDayOfPreviousTerm = $currentTerm->getStartingOn()->modify("yesterday");
        } else {
            $today = new \Datetime();
            $currentTerm = $termRepo->findCurrentTerm($today);
            $lastDayOfPreviousTerm = $currentTerm->getStartingOn();
        }
        $balanceCF = $totalInvoicesBD - $totalPaymentsBD;
        $currentBalance = $totalInvoices - $totalPayments;
        $oneToThirdyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '30', '0', $student)['totalAmount'];
        $thirtyToSixtyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '60', '31', $student)['totalAmount'];
        $sixtyToNinetyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '90', '61', $student)['totalAmount'];
        $moreThenNinetyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '100000', '91', $student)['totalAmount'];
        $oneToThirdyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '30', '0', $student)['totalAmount'];
        $thirtyToSixtyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '60', '31', $student)['totalAmount'];
        $sixtyToNinetyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '90', '61', $student)['totalAmount'];
        $moreThenNinetyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '100000', '91', $student)['totalAmount'];

        return $this->render('admin/preview_statement.html.twig', [
            'student' => $student,
            'payments' => $payments,
            'uigs' => $uigs,
            'balanceCF' => $balanceCF,
            'currentBalance' => $currentBalance,
            'lastDayOfPreviousTerm' => $lastDayOfPreviousTerm,
            'oneToThirdyDaysDue' => $oneToThirdyDaysDue,
            'thirtyToSixtyDaysDue' => $thirtyToSixtyDaysDue,
            'sixtyToNinetyDaysDue' => $sixtyToNinetyDaysDue,
            'moreThenNinetyDaysDue' => $moreThenNinetyDaysDue,
            'oneToThirdyDaysPaid' => $oneToThirdyDaysPaid,
            'thirtyToSixtyDaysPaid' => $thirtyToSixtyDaysPaid,
            'sixtyToNinetyDaysPaid' => $sixtyToNinetyDaysPaid,
            'moreThenNinetyDaysPaid' => $moreThenNinetyDaysPaid,
        ]);
    }

    /**
     * @Route("/admin/invoice/preview/{payment_id}", name="preview_invoice", methods={"GET"})
     */
    public function previewInvoice(UserInstrumentGradeRepository $userInstrumentGradeRepo, TermRepository $termRepo, PaymentRepository $paymentRepo, $payment_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $payment = $paymentRepo->findOneById($payment_id);
        $currentTerm = $termRepo->findCurrentTerm($payment->getDoneOn());
        return $this->render('admin/preview_invoice.html.twig', [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
        ]);
    }

    /**
     * @Route("/admin/find/user/from/search", name="findthisuser")
     */
    public function findThisUser(Request $request, UserRepository $userRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user_id = $request->request->get('user_id');
        $user = $userRepo->findOneById($user_id);
        $data = [];
        $data['email'] = $user->getEmail();
        $data['fullname'] = $user->getFullname();
        if($user->getUsertype() == 'student'){
            $data['phone'] = $user->getStudentdata()->getPhone();
        } elseif($user->getUsertype() == 'teacher') {
            $data['phone'] = $user->getTeacherdata()->getPhone();
        } else {
            $data['phone'] = "Not provided";
        }
        
        return new JsonResponse($data);
    }

    /**
     * @Route("/admin/teacher/students/list/{teacher_id}", name="admin-teacher-students-list")
     */
    public function reportStudentsList(TermRepository $termRepo, SessionRepository $sessionRepo, UserRepository $userRepo, $teacher_id): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $teacher = $userRepo->findOneById($teacher_id);
        $sessionsuniq = [];
        $sentreports = [];
        $reports = [];
        $r = [];
        foreach ($teacher->getTeacherdata()->getReports() as $reportentry) {
            if($reportentry->getSent() == true ){
                $sentreports[] = $reportentry->getUig()->getId();
            } else {
                $reports[] = $reportentry->getUig()->getId();
                $r[$reportentry->getUig()->getId()] = $reportentry;
            }
            
        }
        $currentTerm = $termRepo->findCurrentTermArr(new \Datetime());

        $sessions = $sessionRepo->findBy(
            array('teacher' => $teacher),
            array('id' => 'ASC')
        );

        foreach ($sessions as $key => $sess) {
            $sessionsuniq[$sess->getUserInstrumentGrade()->getId()] = $sess;
        }

        return $this->render('teacher/studentslist.html.twig', [
            'controller_name' => 'StudentController',
            'sessions' => $sessionsuniq,
            'teacher' => $teacher,
            'sentreports' => $sentreports,
            'currentTerm' => $currentTerm,
            'reports' => $reports,
            'r' => $r,
        ]);
    }

    /**
     * @Route("/admin/cheza/search/user", name="ajax_search")
     */
    public function searchAction(Request $request, UserRepository $userRepo): Response
    {

        $requestString = $request->request->get('q');

        $entities =  $userRepo->findEntitiesByString($requestString);
    
        if(!$entities) {
            $result['entities']['error'] = "Name not found";
        } else {
            $result['entities'] = $this->getRealEntities($entities);
        }

        return new JsonResponse(json_encode($result));

    }
    
    //   /**
    //    * Creates a new ActionItem entity.
    //    *
    //    * @Route("/search", name="ajax_search")
    //    * @Method("GET")
    //    */
    //   public function searchAction(Request $request, )
    //   {
    //       $em = $this->getDoctrine()->getManager();
    
    //       $requestString = $request->get('q');
    
    //       $entities =  $userRepo->findEntitiesByString($requestString);
    
    //       if(!$entities) {
    //           $result['entities']['error'] = "keine Einträge gefunden";
    //       } else {
    //           $result['entities'] = $this->getRealEntities($entities);
    //       }
    
    //       return new Response(json_encode($result));
    //   }
    
      public function getRealEntities($entities){
    
          foreach ($entities as $entity){
              $realEntities[$entity->getId()] = $entity->getFullname();
          }
    
          return $realEntities;
      }


}
