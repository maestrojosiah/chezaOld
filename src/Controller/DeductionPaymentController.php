<?php

namespace App\Controller;

use App\Entity\DeductionPayment;
use App\Form\DeductionPaymentType;
use App\Repository\DeductionPaymentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/deduction/payment")
 */
class DeductionPaymentController extends AbstractController
{
    /**
     * @Route("/", name="deduction_payment_index", methods={"GET"})
     */
    public function index(DeductionPaymentRepository $deductionPaymentRepository): Response
    {
        return $this->render('deduction_payment/index.html.twig', [
            'deduction_payments' => $deductionPaymentRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="deduction_payment_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $deductionPayment = new DeductionPayment();
        $form = $this->createForm(DeductionPaymentType::class, $deductionPayment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($deductionPayment);
            $entityManager->flush();

            return $this->redirectToRoute('deduction_payment_index');
        }

        return $this->render('deduction_payment/new.html.twig', [
            'deduction_payment' => $deductionPayment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="deduction_payment_show", methods={"GET"})
     */
    public function show(DeductionPayment $deductionPayment): Response
    {
        return $this->render('deduction_payment/show.html.twig', [
            'deduction_payment' => $deductionPayment,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="deduction_payment_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DeductionPayment $deductionPayment): Response
    {
        $form = $this->createForm(DeductionPaymentType::class, $deductionPayment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('deduction_payment_index');
        }

        return $this->render('deduction_payment/edit.html.twig', [
            'deduction_payment' => $deductionPayment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="deduction_payment_delete", methods={"DELETE"})
     */
    public function delete(Request $request, DeductionPayment $deductionPayment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$deductionPayment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($deductionPayment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('deduction_payment_index');
    }
}
