<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\StudentUserData;
use App\Entity\Payment;
use App\Entity\UserSettings;
use App\Entity\Template;
use App\Entity\Comment;
use App\Entity\CommentReply;
use App\Entity\UserInstrumentGrade;
use App\Entity\SchoolTime;
use App\Entity\DeductionPayment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\InstrumentRepository;
use App\Repository\SessionRepository;
use App\Repository\PackageRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\PaymentRepository;
use App\Repository\ReportRepository;
use App\Repository\SettingsRepository;
use App\Repository\SchoolTimeRepository;
use App\Repository\TemplateRepository;
use App\Repository\UserRepository;
use App\Repository\UserSettingsRepository;
use App\Repository\TermRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\AttachmentRepository;
use App\Repository\CommentRepository;
use App\Repository\PostRepository;
use App\Service\Mailer;
use Twig\Environment;
use Knp\Snappy\Pdf;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AjaxController extends AbstractController
{
    private $snappy_pdf;

    public function __construct(Pdf $snappy_pdf){
        $this->snappy_pdf = $snappy_pdf;
    }    

    /**
     * @Route("/ajax", name="ajax")
     */
    public function index(Request $request): Response
    {
        return $this->render('ajax/index.html.twig', [
            'controller_name' => 'AjaxController',
        ]);
    }

    /**
     * @Route("/upload/profile/picture", name="save_profile_picture")
     */
    public function saveFile(Request $request)
    {
        $file = $_POST;
        $path_to_save = 'profile_img_directory';
        if($file){

            $current_photo_path = $this->getParameter($path_to_save).time().$this->getUser()->getId().".png";
            $short_profile_img_directory = $this->getParameter('short_profile_img_directory').time().$this->getUser()->getId().".png";
            $data = $_POST['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            file_put_contents($current_photo_path, $data);

            $user = $this->getUser();

            $prefix_to_delete = $this->getParameter('prefix_to_delete');
            $existing_img = $prefix_to_delete . $user->getPhoto();
            if(is_file($existing_img) && file_exists($existing_img)){ unlink($existing_img); }

            $user->setPhoto($short_profile_img_directory);
            $this->save($user);             
            return new JsonResponse($short_profile_img_directory);
            
        }
        // return new JsonResponse("success");
    }

    /**
     * @Route("/give/duration/from/package/id", name="check_duration")
     */
    public function checkDuration(Request $request, PackageRepository $packageRepo)
    {
        $package_id = $request->request->get('package_id');
        $package = $packageRepo->findOneById($package_id);
        $duration = $package->getDuration();

        return new JsonResponse($duration);
    }

    public function save($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    public function delete($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($entity);
        $entityManager->flush();
    }
    /**
     * @Route("/save/user/bio", name="savebio")
     */
    public function savebio(Request $request): Response
    {
        $user = $this->getUser();
        if($user->getUsertype() == 'student'){
            $userdata = $user->getStudentdata();
        } else if ($user->getUsertype() == 'teacher'){
            $userdata = $user->getTeacherdata();
        }

        $phone = $request->request->get('phone');
        $fullname = $request->request->get('fullname');
        $email = $request->request->get('email');
        $age = $request->request->get('age');
        $sex = $request->request->get('sex');

        $user->setFullname($fullname);
        if($user->getUsertype() != 'admin'){
            $userdata->setPhone($phone);
            $user->setEmail($email);
            if($age){
                $userdata->setAge($age);
            }
            $userdata->setSex($sex);
    
        }

        $this->save($user);
        if($user->getUsertype() != 'admin'){
            $this->save($userdata);
        }


        return new JsonResponse($_POST);

    }
    
    /**
     * @Route("/save/student/from/admin", name="savestudentfromadmin")
     */
    public function savestudentfromadmin(Request $request, InstrumentRepository $instrumentRepo, UserPasswordEncoderInterface $passwordEncoder, InstrumentGradeRepository $instrumentGradeRepo ): Response
    {
        $user = new User();
        $userdata = new StudentUserData;
        $entityManager = $this->getDoctrine()->getManager();

        $instruments = $request->request->get('instrument'); // array
        $grades = $request->request->get('grade'); // array
        $branch = $request->request->get('branch');
        $phone = $request->request->get('phone');
        $fullname = $request->request->get('fullname');
        $email = $request->request->get('email');
        $age = $request->request->get('age');
        $sex = $request->request->get('sex');
        $kinemail = $request->request->get('kinemail');
        $kinfullname = $request->request->get('kinemail');
        $kinphone = $request->request->get('kinphone');
        $kinrship = $request->request->get('kinrship');
        $lessonmode = $request->request->get('lessonmode');

        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $phone
            )
        );

        $user->setActive(false);
        $user->setFullname($fullname);
        $user->setEmail($email);
        $user->setUsertype("student");
        $entityManager->persist($user);

        $userdata->setPhone($phone);
        $userdata->setAge($age);
        $userdata->setSex($sex);
        $userdata->setBranch($branch);
        $userdata->setNextofkinemail($kinemail);
        $userdata->setNextofkinname($kinfullname);
        $userdata->setNextofkinphonenum($kinphone);
        $userdata->setNextofkinrship($kinrship);
        $userdata->setLessonmode($lessonmode);
        $entityManager->persist($userdata);

        foreach ($instruments as $key => $ins) {
            $instrument = $instrumentRepo->findOneById($ins);
            $userInstrument = new UserInstrumentGrade();
            $userInstrument->setStudentUserData($userdata);
            $userInstrument->setGrade($instrumentGradeRepo->findOneById($grades[$key]));
            $userInstrument->setInstrument($instrument);
            $entityManager->persist($userInstrument);    

            $userdata->addUserInstruments($userInstrument);
            $entityManager->persist($userdata);
        }

        $user->setStudentdata($userdata);
        $entityManager->persist($user);
    
        // activate student
        $active = $user->getActive();
        $uigActives = $user->getStudentdata()->getUserInstruments();

        $activeText = "";
        if($active){
            $user->setActive(false);
            foreach ($uigActives as $uigActive ) {
                $uigActive->setActive(false);
                $entityManager->persist($uigActive); 
            }
            $activeText = "Inactive";
        } else {
            $user->setActive(true);
            foreach ($uigActives as $uigActive ) {
                $uigActive->setActive(true);
                $entityManager->persist($uigActive); 
            }
            $activeText = "Active";
        }
        
        
        $entityManager->persist($user); 

        $entityManager->flush();

        $this->addFlash(
            'success',
            'An account was created for ' . $user->getFullname() . ". You can assign them a teacher from here.",
        );

        // return $this->redirectToRoute('admin-teachers-list');

        return new JsonResponse($userdata);

    }
    
    /**
     * @Route("/edit/student/from/admin", name="editstudentfromadmin")
     */
    public function editstudentfromadmin(Request $request, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo, InstrumentRepository $instrumentRepo, UserPasswordEncoderInterface $passwordEncoder, InstrumentGradeRepository $instrumentGradeRepo ): Response
    {
        $user = $userRepo->findOneById($request->request->get('userid'));
        $userdata = $user->getStudentdata();
        $entityManager = $this->getDoctrine()->getManager();

        $instruments = $request->request->get('instrument'); // array
        $grades = $request->request->get('grade'); // array
        $branch = $request->request->get('branch');
        $phone = $request->request->get('phone');
        $fullname = $request->request->get('fullname');
        $email = $request->request->get('email');
        $age = $request->request->get('age');
        $sex = $request->request->get('sex');
        $kinemail = $request->request->get('kinemail');
        $kinfullname = $request->request->get('kinemail');
        $kinphone = $request->request->get('kinphone');
        $kinrship = $request->request->get('kinrship');
        $lessonmode = $request->request->get('lessonmode');

        $user->setFullname($fullname);
        $user->setEmail($email);
        $entityManager->persist($user);

        $userdata->setPhone($phone);
        $userdata->setAge($age);
        $userdata->setSex($sex);
        $userdata->setBranch($branch);
        $userdata->setNextofkinemail($kinemail);
        $userdata->setNextofkinname($kinfullname);
        $userdata->setNextofkinphonenum($kinphone);
        $userdata->setNextofkinrship($kinrship);
        $userdata->setLessonmode($lessonmode);
        $entityManager->persist($userdata);
        $userInstruments = $user->getStudentdata()->getUserInstruments();
        $idarrayuig = [];
        $test = [];

        foreach($userInstruments as $uig){
            $idarrayuig[] = $uig->getInstrument()->getId();
        }

        foreach ($instruments as $key => $ins) {

            $instrument = $instrumentRepo->findOneById($ins);

            if (!in_array ($ins, $idarrayuig)){
                $test[] = 'not in array';
                $userInstrument = new UserInstrumentGrade();
                $userInstrument->setStudentUserData($userdata);
                $userInstrument->setGrade($instrumentGradeRepo->findOneById($grades[$key]));
                $userInstrument->setInstrument($instrument);
                $entityManager->persist($userInstrument);    
    
                $userdata->addUserInstruments($userInstrument);
                $entityManager->persist($userdata);
            } else {
                $test[] = 'in array';
                $userInstrument = $uigRepo->findOneByInstrument($instrument);
            }
            
        }

        $user->setStudentdata($userdata);
        $entityManager->persist($user);        

        // activate student
        $uigActives = $user->getStudentdata()->getUserInstruments();
        
        foreach ($uigActives as $uigActive ) {
            $uigActive->setActive(true);
            $entityManager->persist($uigActive); 
        }
        
        $entityManager->flush();

        $this->addFlash(
            'success',
            $user->getFullname() . "'s account was successfully edited.",
        );

        // return $this->redirectToRoute('admin-teachers-list');

        return new JsonResponse($test);

    }
    
    /**
     * @Route("/add/sibling/from/admin", name="addsiblingfromadmin")
     */
    public function addSiblingUig(Request $request, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo, InstrumentRepository $instrumentRepo, UserPasswordEncoderInterface $passwordEncoder, InstrumentGradeRepository $instrumentGradeRepo ): Response
    {
        $user = $userRepo->findOneById($request->request->get('userid'));
        $userdata = $user->getStudentdata();
        $entityManager = $this->getDoctrine()->getManager();

        $instrument_id = $request->request->get('instrument'); 
        $grade_id = $request->request->get('grade'); 
        $siblingname = $request->request->get('siblingname'); 

        $instrument = $instrumentRepo->findOneById($instrument_id);
        $userInstrument = new UserInstrumentGrade();
        $userInstrument->setStudentUserData($userdata);
        $userInstrument->setGrade($instrumentGradeRepo->findOneById($grade_id));
        $userInstrument->setInstrument($instrument);
        $userInstrument->setActive(true);
        $userInstrument->setSiblingname($siblingname);
        $entityManager->persist($userInstrument);    

        $userdata->addUserInstruments($userInstrument);
        $entityManager->persist($userdata);

        $entityManager->flush();

        $this->addFlash(
            'success',
            'A sibling account was created for ' . $user->getFullname() . ". You can assign them a teacher from here.",
        );

        // return $this->redirectToRoute('admin-teachers-list');

        return new JsonResponse($siblingname);

    }
    
    /**
     * @Route("/add/term/from/admin", name="addtermfromadmin")
     */
    public function addTermUig(Request $request, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo, InstrumentRepository $instrumentRepo, UserPasswordEncoderInterface $passwordEncoder, InstrumentGradeRepository $instrumentGradeRepo ): Response
    {
        $user = $userRepo->findOneById($request->request->get('userid'));
        $userdata = $user->getStudentdata();
        $entityManager = $this->getDoctrine()->getManager();

        $instrument_id = $request->request->get('instrument'); 
        $grade_id = $request->request->get('grade'); 

        $instrument = $instrumentRepo->findOneById($instrument_id);
        $userInstrument = new UserInstrumentGrade();
        $userInstrument->setStudentUserData($userdata);
        $userInstrument->setGrade($instrumentGradeRepo->findOneById($grade_id));
        $userInstrument->setInstrument($instrument);
        $userInstrument->setActive(true);
        $entityManager->persist($userInstrument);    

        $userdata->addUserInstruments($userInstrument);
        $entityManager->persist($userdata);

        $entityManager->flush();

        $this->addFlash(
            'success',
            $user->getFullname() . " Can now be assigned more sessions",
        );

        // return $this->redirectToRoute('admin-teachers-list');

        return new JsonResponse($grade_id);

    }
    
    /**
     * @Route("/save/user/nextofkin", name="savenextofkin")
     */
    public function savenextofkin(Request $request): Response
    {
        $user = $this->getUser();
        if($user->getUsertype() == 'student'){
            $userdata = $user->getStudentdata();
        } else if ($user->getUsertype() == 'teacher'){
            $userdata = $user->getTeacherdata();
        }

        $kinphone = $request->request->get('kinphone');
        $kinemail = $request->request->get('kinemail');
        $kinfullname = $request->request->get('kinfullname');
        $kinrship = $request->request->get('kinrship');

        $userdata->setNextofkinname($kinfullname);
        $userdata->setNextofkinphonenum($kinphone);
        $userdata->setNextofkinemail($kinemail);
        $userdata->setNextofkinrship($kinrship);

        $this->save($userdata);


        return new JsonResponse($userdata);

    }
    
    /**
     * @Route("/save/user/academic", name="saveacademic")
     */
    public function saveacademic(Request $request): Response
    {
        $user = $this->getUser();
        if($user->getUsertype() == 'student'){
            $userdata = $user->getStudentdata();
        } else if ($user->getUsertype() == 'teacher'){
            $userdata = $user->getTeacherdata();
            $resumelnk = $request->request->get('resumelnk');
            $userdata->setResumelnk($resumelnk);
        }
        

        $branch = $request->request->get('branch');
        $lessonmode = $request->request->get('lessonmode');

        $userdata->setLessonmode($lessonmode);
        $userdata->setBranch($branch);

        $this->save($userdata);


        return new JsonResponse($userdata);

    }

    /**
     * @Route("/calendar/change/date", name="calendar_change_date")
     */
    public function calendarChangeDate(Request $request, SessionRepository $sessionRepo): Response
    {        

        $year = $request->request->get('year');
        $month = $request->request->get('month');
        $day = $request->request->get('date');
        $url = $request->request->get('url');
        $hour = $request->request->get('hour');
        $minute = $request->request->get('minute');
        $id = explode("/", $url)[3];

        $session = $sessionRepo->findOneById($id);
        // $startinghour = $session->getBeginAt()->format('H');
        // $startingminute = $session->getBeginAt()->format('i');
        // $endinghour = $session->getEndAt()->format('H');
        // $endingminute = $session->getEndAt()->format('i');
        $duration = "";
        switch ($session->getPackage()->getDuration()) {
            case '60':
                $duration = "+1 hour";
                break;
            case '45':
                $duration = "+45 minutes";
                break;
            case '30':
                $duration = "+30 minutes";
                break;
        }

        $newdate = new \DateTime();
        $newdate->setDate($year, $month+1, $day);
        $newdate->setTime($hour, $minute);

        $varenddate = clone $newdate;
        $endinghr = $varenddate->modify($duration);

        $session->setBeginAt($newdate);
        $session->setEndAt($endinghr);

        $startingOn = clone $newdate;
        $session->setStartingOn($startingOn);

        $this->save($session);

        return new JsonResponse($duration);

    }

    /**
     * @Route("/validity/check/session/schedule", name="check_schedule")
     */
    public function checkValidityOfDateAndTime(Request $request, SessionRepository $sessionRepo, UserRepository $userRepo, SchoolTimeRepository $schoolTimeRepo)
    {        

        // get available days in schedules for this teacher as intervals
        // get all the lessons for this teacher in intervals
        // if teacher is not available on the given day, return
        // if teacher is available on given day but not available on the given time, return
        // if teacher has lesson on given day and time, return
        $dayDateStart = new \Datetime($request->request->get('startingOn'));
        $dayDateEnd = clone $dayDateStart;
        

        $dayInt = $dayDateStart->format('N');
        $teacher = $userRepo->findOneById($request->request->get('teacher_id'));
        $schedules = $teacher->getSchoolTimes();
        $responses = [];
        $message = "";
        $target = "";

        $arr = [];
        foreach ($schedules as $key => $schedule) {
            $arr[] = $schedule->getJsdaycode();            
        }
        $thisDay = (int)$dayInt - 1;

        $availableSchedules = array_unique($arr);

        //if teacher is not available on the day
        if(!in_array($thisDay, $availableSchedules)){
            $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " is not available on " . $dayDateStart->format('l') . "s</div>";
            $target = "feedback-startingOn";
            $responses[] = ['target' => $target, 'message' => $message];
        } else {
            $message = "<div class='alert alert-success'>" . $teacher->getFullname() . " is available on " . $dayDateStart->format('l') . "s</div>";
            $target = "feedback-startingOn";
            $responses[] = ['target' => $target, 'message' => $message];
        }

        // $sessions = $teacher->getTeachersessions();
        $sessions = $sessionRepo->findFutureLessons($dayDateStart, $teacher);

        if($request->request->get('beginAt')){
            $hourBegin = explode(":", $request->request->get('beginAt'))[0];
            $minuteBegin = explode(":", $request->request->get('beginAt'))[1];
            $dayDateTimeBegin = $dayDateStart->setTime($hourBegin, $minuteBegin);    
            $dayDateTimeBeginStr = $hourBegin.$minuteBegin;
        }
        if($request->request->get('endAt')){
            $hourEnd = explode(":", $request->request->get('endAt'))[0];
            $minuteEnd = explode(":", $request->request->get('endAt'))[1];
            $dayDateTimeEnd = $dayDateEnd->setTime($hourEnd, $minuteEnd);  
            $dayDateTimeEndStr = $hourEnd.$minuteEnd;  
        }
        $test = null;
        // echo 'test';
        //if teacher has not arrived
        if(isset($dayDateTimeBegin)){
            $theDaySelected = $dayDateTimeBegin->format('l');
            $matchingDay = $schoolTimeRepo->findOneBy(
                array('day' => $theDaySelected, 'teacher' => $teacher),
                array('id' => 'ASC'),
            );

            if ($matchingDay){
                $teacherArrivesAt = $matchingDay->getBeginAt();
                $teacherDepartingTime = $matchingDay->getEndAt();
    
                if($dayDateTimeBegin->format('Hi') < $teacherArrivesAt->format('Hi')){
                    $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " is not available before ".$teacherArrivesAt->format("H:i"). " on " . $dayDateStart->format('l') . "s</div>";
                    $target = "feedback-beginAt";
                    $responses[] = ['target' => $target, 'message' => $message];
                } else if ($dayDateTimeBegin->format('Hi') > $teacherDepartingTime->format('Hi')){
                    $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " is not available after " .$teacherDepartingTime->format("H:i"). " on " . $dayDateStart->format('l') . "s</div>";
                    $target = "feedback-beginAt";
                    $responses[] = ['target' => $target, 'message' => $message];    
                } else if ($dayDateTimeEnd->format('Hi') > $teacherDepartingTime->format('Hi')){
                    $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " is not available after " .$teacherDepartingTime->format("H:i"). " on " . $dayDateStart->format('l') . "s</div>";
                    $target = "feedback-endAt";
                    $responses[] = ['target' => $target, 'message' => $message];    
                } else {
                    foreach ($sessions as $key => $session) {
                        if(isset($dayDateTimeBegin) && $dayDateTimeBegin->format('l') == $session->getBeginAt()->format('l')){
                            // if comes after existing lesson and diff btw both beginnings is less than existing lesson duration
                            // or beginning is less than beginning of existing lesson and end is greater than beginning of existing lesson
                            $comesAfterExistingLesson = $dayDateTimeBegin->format('Hi') > $session->getBeginAt()->format('Hi');
                            $comesSameAsExistingLesson = $dayDateTimeBegin->format('Hi') == $session->getBeginAt()->format('Hi');
                            $intervalBetweenBeginnings = abs($dayDateTimeBegin->format('Hi') - $session->getBeginAt()->format('Hi'))-40;
                            $existingLessonDifference = ($session->getEndAt()->format('Hi') - $session->getBeginAt()->format('Hi'))-40;
            
                            $comesBeforeExistingLesson = $dayDateTimeBegin->format('Hi') < $session->getBeginAt()->format('Hi');
            
                            if($comesAfterExistingLesson && $intervalBetweenBeginnings < $existingLessonDifference){
                                $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " will be on another lesson with " . $session->getStudent()->getFullname() . "</div>";
                                $target = "feedback-beginAt";        
                                $responses[] = ['target' => $target, 'message' => $message];
                                break;
                            } else if ($comesBeforeExistingLesson && $dayDateTimeEndStr > $session->getBeginAt()->format('Hi')){
                                $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " will be on another lesson with " . $session->getStudent()->getFullname() . "</div>";
                                $target = "feedback-endAt";        
                                $responses[] = ['target' => $target, 'message' => $message];
                                break;
                            } else if ($comesSameAsExistingLesson){
                                $message = "<div class='alert alert-danger'>" . $teacher->getFullname() . " will be starting another lesson with " . $session->getStudent()->getFullname() . "</div>";
                                $target = "feedback-beginAt";        
                                $responses[] = ['target' => $target, 'message' => $message];
                                break;
                            } else {
                                $message = "<div class='alert alert-success'> Looks good </div>";
                                $target = "feedback-beginAt";        
                                $responses[] = ['target' => $target, 'message' => $message];
                                $message2 = "<div class='alert alert-success'> Looks good </div>";
                                $target2 = "feedback-endAt";        
                                $responses[] = ['target' => $target2, 'message' => $message2];
                            }
                            
                            // echo $session->getTeacher()->getFullname();
            
                        } else {
                            $message = "<div class='alert alert-success'> Looks good </div>";
                            $target = "feedback-beginAt";        
                            $responses[] = ['target' => $target, 'message' => $message];
                            $message2 = "<div class='alert alert-success'> Looks good </div>";
                            $target2 = "feedback-endAt";        
                            $responses[] = ['target' => $target2, 'message' => $message2];
                        }
                        
                    }
            
                }
    
            }
        }

        return new JsonResponse($responses);

    }

    /**
     * @Route("/calendar/add/schedule", name="add_schedule")
     */
    public function addSchedule(Request $request, SchoolTimeRepository $schoolTimeRepo, UserRepository $userRepo): Response
    {        

        $teacher = $userRepo->findOneById($request->request->get('teacher_id'));
        $data = json_decode($request->request->get('data'));
        $dbdata = $schoolTimeRepo->findByTeacher($teacher);

        $dt = [];
        $dbdt = [];
        
        foreach ($dbdata as $key => $value) {
            $dbdt[$value->getJsdaycode()."-".$key] = $value->getBeginAt()->format('H:i') . '-' . $value->getEndAt()->format('H:i'). '-' .$value->getJsdaycode();
        }


        $lastperiod = "";

        foreach ($data as $key => $singleday ) {
            $schoolTime = new SchoolTime();
            $dayIndex = $singleday->day;
            $periods = $singleday->periods;
            
            foreach ($periods as $key => $period) {
                $dt[] = $period->start .'-'. $period->end . '-' . $dayIndex;
                $lastperiod = $period->start .'-'. $period->end . '-' . $dayIndex;
            }

        }


        // add to database.
        // split the string $lastperiod

        $diff = array_diff($dt, $dbdt);

        $periodStart = explode("-", reset($diff))[0];
        $periodEnd = explode("-", reset($diff))[1];
        $whichDay = explode("-", reset($diff))[2];

        $datetime = new \Datetime();
        $starttime = clone $datetime;
        $starttime->setTime(explode(":", $periodStart)[0], explode(":", $periodStart)[1]);
        
        $endtime = clone $datetime;
        $endtime->setTime(explode(":", $periodEnd)[0], explode(":", $periodEnd)[1]);

        switch ($whichDay) {
            case 0:
                $dayName = "Monday";
                break;
            case 1:
                $dayName = "Tuesday";
                break;
            case 2:
                $dayName = "Wednesday";
                break;
            case 3:
                $dayName = "Thursday";
                break;
            case 4:
                $dayName = "Friday";
                break;
            case 5:
                $dayName = "Saturday";
                break;
            case 6:
                $dayName = "Sunday";
                break;

        }

        $schoolTime->setBeginAt($starttime);
        $schoolTime->setEndAt($endtime);
        $schoolTime->setDay($dayName);
        $schoolTime->setJsdaycode($whichDay);
        $schoolTime->setTeacher($teacher);
        $schoolTime->setTitle($whichDay."-".$periodStart."-".$periodEnd);

        $this->save($schoolTime);

        return new JsonResponse($dayName . " " . $periodStart . " - " . $periodEnd);

    }

    /**
     * @Route("/calendar/remove/schedule", name="remove_schedule")
     */
    public function removeSchedule(Request $request, SchoolTimeRepository $schoolTimeRepo): Response
    {        

        $title = $request->request->get('title');
        $schedule = $schoolTimeRepo->findOneByTitle($title);
        $this->delete($schedule);

        return new JsonResponse($title);

    }

    /**
     * @Route("/sessionuig/change/instrument", name="change_instrument")
     */
    public function changeInstrument(Request $request, UserInstrumentGradeRepository $uigRepo, InstrumentRepository $instrumentRepo ): Response
    {        

        $entryId = $request->request->get('entryId');
        $newInstrument = $request->request->get('newInstrument');

        $uig = $uigRepo->findOneById($entryId);
        $instrument = $instrumentRepo->findOneById($newInstrument);

        $uig->setInstrument($instrument);
        $this->save($uig);
        
        return new JsonResponse($uig->getStudentUserData()->getStudent()->getFullname() . ' - ' .  $uig->getInstrument()->getName() . ' ( ' . $uig->getGrade()->getName() . ' ) ');

    }

    /**
     * @Route("/sessionuig/change/grade", name="change_grade")
     */
    public function changeGrade(Request $request, UserInstrumentGradeRepository $uigRepo, InstrumentGradeRepository $instrumentGradeRepo ): Response
    {        

        $entryId = $request->request->get('entryId');
        $newGrade = $request->request->get('newGrade');

        $uig = $uigRepo->findOneById($entryId);
        $grade = $instrumentGradeRepo->findOneById($newGrade);

        $uig->setGrade($grade);
        $this->save($uig);
        
        return new JsonResponse($uig->getStudentUserData()->getStudent()->getFullname() . ' - ' .  $uig->getInstrument()->getName() . ' ( ' . $uig->getGrade()->getName() . ' ) ');

    }

    /**
     * @Route("/sessionuig/change/package", name="change_package")
     */
    public function changePackage(Request $request, UserInstrumentGradeRepository $uigRepo, PackageRepository $packageRepo ): Response
    {        

        $entityManager = $this->getDoctrine()->getManager();
        $entryId = $request->request->get('entryId');
        $newPackage = $request->request->get('newPackage');
        $package = $packageRepo->findOneById($newPackage);

        $uig = $uigRepo->findOneById($entryId);
        $sessions = $uig->getSessions();
        foreach ($sessions as $session) {
            $session->setPackage($package);
            $entityManager->persist($session);  
        }
        

        $entityManager->flush();
        
        return new JsonResponse();

    }

    /**
     * @Route("/sessionuig/change/siblingname", name="change_siblingname")
     */
    public function changeSiblingname(Request $request, UserInstrumentGradeRepository $uigRepo ): Response
    {        

        $siblingname = $request->request->get('siblingname');
        $uigid = $request->request->get('uigid');

        $uig = $uigRepo->findOneById($uigid);
        $uig->setSiblingname($siblingname);
        $this->save($uig);
        return new JsonResponse($uigid);

    }

    /**
     * @Route("/sessionuig/change/time", name="change_time")
     */
    public function changeTime(Request $request, SessionRepository $sessionRepo ): Response
    {        
        // get starttime from form vars
        $startTime = $request->request->get('startTime');
        // get name of day of clicked date
        $startTimeSessDay = $request->request->get('startTimeSessDay');

        // manipulate the entries
        $sthr = explode(":", $startTime)[0];
        $stmin = explode(":", $startTime)[1];

        // do the same for endtime
        $endTime = $request->request->get('endTime');
        $ethr = explode(":", $endTime)[0];
        $etmin = explode(":", $endTime)[1];

        // get the id of this particular session
        $session = $sessionRepo->findOneById($request->request->get('session'));

        // get the uig associated with this session
        $uig = $session->getUserInstrumentGrade();

        // get all the sessions that belong with this session
        $sessionsForThisUIG = $sessionRepo->findByUserInstrumentGrade($uig);

        $count = 0;
        $test = [];
        foreach ($sessionsForThisUIG as $session ) {
            // check to see if the session in iteration is of same day as this startTimeSessDay
            if($session->getStartingOn()->format('l') == $startTimeSessDay){
                $beginAt = $session->getBeginAt();
                $endAt = $session->getEndAt();
        
                $newStartTime = clone $beginAt;
                $newEndTime = clone $endAt;
                $newStartTime->setTime($sthr, $stmin);
                $newEndTime->setTime($ethr, $etmin);
                
                $session->setBeginAt($newStartTime);
                $session->setEndAt($newEndTime);
                
                $this->save($session);
                $count += 0;
    
            }
            $test[] = $session->getStartingOn()->format('l');
        }

        return new JsonResponse($test);

    }

    /**
     * @Route("/sessionuig/change/times", name="change_times")
     */
    public function changeTimes(Request $request, SessionRepository $sessionRepo ): Response
    {        
        // get starttime from form vars
        $startTime = $request->request->get('startTime');
        // get date from form vars
        $sessionDate = $request->request->get('sessionDate');
        $sessionDatetime = new \Datetime($sessionDate);
        // get name of day of clicked date
        $sessid = $request->request->get('sessid');

        // manipulate the entries
        $sthr = explode(":", $startTime)[0];
        $stmin = explode(":", $startTime)[1];

        // do the same for endtime
        $endTime = $request->request->get('endTime');
        $ethr = explode(":", $endTime)[0];
        $etmin = explode(":", $endTime)[1];

        // do the same for date
        $stday = explode("-", $sessionDate)[0];
        $stmon = explode("-", $sessionDate)[1];
        $styr = explode("-", $sessionDate)[2];

        // get the id of this particular session
        $session = $sessionRepo->findOneById($sessid);

        $beginAt = $session->getBeginAt();
        $endAt = $session->getEndAt();
        $date = $session->getStartingOn();

        $newStartTime = clone $beginAt;
        $newEndTime = clone $endAt;
        $newDate = clone $sessionDatetime;
        $newStartTime->setTime($sthr, $stmin);
        $newEndTime->setTime($ethr, $etmin);
        $newStartTime->setDate($stday, $stmon, $styr);
        $newEndTime->setDate($stday, $stmon, $styr);

        
        $session->setBeginAt($newStartTime);
        $session->setEndAt($newEndTime);
        $session->setStartingOn($newDate);
        
        $this->save($session);
    

        return new JsonResponse($session->getId());

    }

    /**
     * @Route("/sessionuig/change/dates", name="change_dates")
     */
    public function changeDates(Request $request, SessionRepository $sessionRepo, TermRepository $termRepo): Response
    {        

        // $test = [];
        $days = [];
        // the new date string
        $newdate = $request->request->get('newdate');
        $halftermandholidays = $request->request->get('halftermandholidays');
        // the new datetime
        $newDateTime = new \DateTime($newdate);
        // find the session
        $session = $sessionRepo->findOneById($request->request->get('session'));
        $numberofsessionsperweek = $request->request->get('daysLength');
        // find the uig for this session
        $uig = $session->getUserInstrumentGrade();
        // find the sessions that go along with this session
        $sessionsForThisUIG = $sessionRepo->findByUserInstrumentGrade($uig);
        // $test = $newDateTime->format('l Y-m-d');
        $entityManager = $this->getDoctrine()->getManager();

        $currentTerm = $termRepo->findCurrentTermArr($newDateTime);
        $termnumber = $currentTerm[0]->getTermnumber();
        $halftermstart = $currentTerm[0]->getHalftermstart();
        $halftermend = $currentTerm[0]->getHalftermend();
        $holidaystart = $currentTerm[0]->getHolidaystart();
        $holidayend = $currentTerm[0]->getHolidayend();
        $days = $_POST['days'];
        $daysperweek = count($days);
        $iterations = count($sessionsForThisUIG) / $daysperweek;
        $multipliedby = count($sessionsForThisUIG) / 12;
        $startingDate = $newDateTime;
        $otherdate = clone $startingDate;
        $counter = 1;
        $test[] = [$halftermstart, $halftermend];
        for ($i=0; $i < $iterations; $i++) {
            foreach ($days as $key => $day) {
                if($counter <= (12*$multipliedby)){
                    $halftermbegins = date('Y-m-d', strtotime($halftermstart->format('m/d/Y')));
                    $halftermends = date('Y-m-d', strtotime($halftermend->format('m/d/Y')));
                    $holidaybegins = date('Y-m-d', strtotime($holidaystart->format('m/d/Y')));
                    $holidayends = date('Y-m-d', strtotime($holidayend->format('m/d/Y')));

                    if($counter > 1){
                        $otherdate->modify("next $day");
                        $test[] = 'said next '.$day;
    
                    } elseif($counter == 1) {
                        $otherdate->modify("$day");
                        $test[] = 'said this '.$day;

                    }
                    $otherdatestr=date('Y-m-d', strtotime($otherdate->format('Y-m-d')));
                    // $test[] = $otherdatestr .'.'.$halftermbegins . '.'.$halftermends;
    
                    if (($otherdatestr >= $halftermbegins) && ($otherdatestr <= $halftermends && $halftermandholidays == "skip")){
                        $test[] = 'halfterm: '.$otherdate->format('l Y-m-d');
                        $iterations ++;
                    }elseif (($otherdatestr > $holidaybegins) && ($otherdatestr < $holidayends && $halftermandholidays == "skip")){
                        $test[] = 'holiday: '.$otherdate->format('l, Y-m-d');
                        $iterations ++;
                    } else {
                        $sess = $sessionsForThisUIG[$counter-1];

                        $begin = clone $sess->getBeginAt();
                        $begin->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));
                        $end = clone $sess->getEndAt();
                        $end->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));
                        $startdate = clone $sess->getStartingOn();
                        $startdate->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));

                        $sess->setBeginAt($begin);
                        $sess->setEndAt($end);
                        $sess->setStartingOn($startdate);
                        $entityManager->persist($sess); 
                        
                        $test[] = $otherdate->format('l, Y-m-d');    
                        $counter ++;
                    }
            
            
                }

            }
        }
        $entityManager->flush();

        return new JsonResponse($currentTerm);

    }
    
    /**
     * @Route("/sessionuig/organize/all/dates", name="organize_all_dates")
     */
    public function organizeAllDates(Request $request, UserInstrumentGradeRepository $uigRepo, SessionRepository $sessionRepo, TermRepository $termRepo): Response
    {        
        ini_set('max_execution_time', '300'); //300 seconds = 5 minutes
        set_time_limit(300);
        $test = [];

        $sessions = [];
        foreach ($uigRepo->findAll() as $uigkey => $uig) {
            foreach($uig->getSessions() as $session){
                $sessions[$uig->getId()] = $session;
            }
        }
        foreach ($sessions as $uigid => $session) {
            // $test[] = $session;
            $days = [];
            // the new date string
            // $newdate = $request->request->get('newdate');
            // the new datetime
            // find the uig for this session
            $uig = $session->getUserInstrumentGrade();
            // find the sessions that go along with this session
            $sessionsForThisUIG = $sessionRepo->findByUserInstrumentGrade($uig);
            $newDateTime = $sessionsForThisUIG[0]->getStartingOn();
            // $test[] = $newDateTime->format('l, Y-m-d');

            foreach ($sessionsForThisUIG as $s) {
                $days[$s->getStartingOn()->format('l')] = $s->getStartingOn()->format('l');
            }
            // $test = $newDateTime->format('l Y-m-d');
            $entityManager = $this->getDoctrine()->getManager();

            $currentTerm = $termRepo->findCurrentTermArr($newDateTime);
            $halftermstart = $currentTerm[0]->getHalftermstart();
            $halftermend = $currentTerm[0]->getHalftermend();
            $holidaystart = $currentTerm[0]->getHolidaystart();
            $holidayend = $currentTerm[0]->getHolidayend();
            $daysperweek = count($days);
            $iterations = 12 / $daysperweek;
            $startingDate = $newDateTime;
            $otherdate = clone $startingDate;
            $counter = 0;
            // $test[] = [$halftermstart, $halftermend];
            for ($i=0; $i < $iterations; $i++) {
                foreach ($days as $key => $day) {
                    if($counter <= 11){
                        $otherdatestr=date('Y-m-d', strtotime($otherdate->format('Y-m-d')));
                        $halftermbegins = date('Y-m-d', strtotime($halftermstart->format('m/d/Y')));
                        $halftermends = date('Y-m-d', strtotime($halftermend->format('m/d/Y')));
                        $holidaybegins = date('Y-m-d', strtotime($holidaystart->format('m/d/Y')));
                        $holidayends = date('Y-m-d', strtotime($holidayend->format('m/d/Y')));

                        if (($otherdatestr >= $halftermbegins) && ($otherdatestr <= $halftermends)){
                            $test[] = 'halfterm: '.$otherdate->format('l Y-m-d');
                            $iterations ++;
                        }elseif (($otherdatestr > $holidaybegins) && ($otherdatestr < $holidayends)){
                            $test[] = 'holiday: '.$otherdate->format('l, Y-m-d');
                            $iterations ++;
                        } else {
                            if(isset($sessionsForThisUIG[$counter])){
                                $sess = $sessionsForThisUIG[$counter];

                                $begin = clone $sess->getBeginAt();
                                $begin->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));
                                $end = clone $sess->getEndAt();
                                $end->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));
                                $startdate = clone $sess->getStartingOn();
                                $startdate->setDate($otherdate->format('Y'), $otherdate->format('m'), $otherdate->format('d'));
    
                                $sess->setBeginAt($begin);
                                $sess->setEndAt($end);
                                $sess->setStartingOn($startdate);
                                $entityManager->persist($sess); 
    
                                $test[] = $session->getStudent().'-'.$otherdate->format('l, Y-m-d');    
                                $counter ++;
    
                            }
                        }
                
                
                        
                        $otherdate->modify("next $day");
        
                    }

                }
            }

            $entityManager->flush();
        }

        // return new JsonResponse($test);
        return $this->render('default/test2.html.twig', [
            'statements' => $test,
        ]);


    }

    /**
     * @Route("/teacher/attendance/manage", name="manage_attendance")
     */
    public function manageAttendance(Request $request, UserInstrumentGradeRepository $userInstrumentGradeRepo, SessionRepository $sessionRepo): Response
    {        

        $handle = $request->request->get('handle');
        $uigId = explode("-", $handle)[1];
        $sessionId = explode("-", $handle)[2];

        // $userInstrumentGrade = $userInstrumentGradeRepo->findOneById($uigId);
        $session = $sessionRepo->findOneById($sessionId);
        $saved = $session->getDone() == true ? true : false;
        $return = '';
        $modify = '';

        if($saved){
            $session->setDone(false);
            $dt = $session->getStartingOn();
            $return = "<small>" .$dt->format('M jS') . " <br /> <i class='fa fa-close text-warning'></i></small>";
            $modify = '-';
        } else {
            $session->setDone(true);
            $dt = $session->getStartingOn();
            $return = "<small>" . $dt->format('M jS') . " <br /> <i class='fa fa-check text-success'></i></small>";
            $modify = '+';
        }

        $this->save($session);
        $amt = ($session->getPackage()->getPrice()/12) * 0.700;

        return new JsonResponse([$return, $amt, $modify]);

    }

    /**
     * @Route("/teacher/submit/invoice", name="submit_invoice")
     */
    public function submitInvoice(UserInstrumentGradeRepository $userInstrumentGradeRepo, PackageRepository $packageRepo, Request $request, Mailer $mailer, Environment $twig): Response
    {        

        $userInstrumentGradesIds = json_decode($request->request->get('nonEmptyUigs'));
        $em = $this->getDoctrine()->getManager();
        $teacher = $this->getUser();

        foreach ($userInstrumentGradesIds as $uigId ) {
            $key = explode(":", $uigId)[0];
            $val = explode(":", $uigId)[1];
            $pack = explode(":", $uigId)[2]; // 2030-3
            $packageId = explode("-", $pack)[1]; // 3
            $package = $packageRepo->findOneById($packageId);
            $packageDuration = $package->getDuration();
            // $userInstrumentGrades[$key] = $val;
            $userInstrumentGrades[$packageDuration.'-'.$uigId] = $userInstrumentGradeRepo->findOneById($key);
        }
       
        $inv = 0;
        foreach ($userInstrumentGrades as $key => $uig ) { // key has the package duration
            foreach ($uig->getSessions() as $session ) {
                if($session->getDone() == true && $session->getInvoiced() == false){
                    $inv += ($session->getPackage()->getPrice()/12) * 0.700;
                    $session->setInvoiced(true);
                    $this->save($session);
                }
            }
        }
        $ded = 0;
        foreach ($teacher->getTeacherdata()->getDeductions() as $d) {
            $ded += $d->getToDeduct();
        }


        $html = $twig->render('pdf/invoice.html.twig', [
            'teacher' => $teacher,
            'userInstrumentGrades' => $userInstrumentGrades,
            'inv' => $inv,
            'ded' => $ded,
            'nonEmptyUigs' => $userInstrumentGrades,
        ]);
        // change email to go to finance / accounts email
        $mailer->sendEmailWithAttachment(['teacher' => $teacher], 'info@chezamusicschool.co.ke', $teacher->getFullname().' Invoice Submission', $html, 'invoice.html.twig', $teacher->getFullname().'-invoice');
        $mailer->sendEmailWithAttachment(['teacher' => $teacher], 'vessijohn@gmail.com', $teacher->getFullname().' Invoice Submission', $html, 'invoice.html.twig', $teacher->getFullname().'-invoice');
        $mailer->sendEmailWithAttachment(['teacher' => $teacher], $teacher->getEmail(), $teacher->getFullname().' Copy of Invoice Submission', $html, 'invoice.html.twig', $teacher->getFullname().'-invoice');
        $this->addFlash(
            'success',
            'Your invoice of ' . $inv . " was sent successfully.",
        );
        foreach ($teacher->getTeacherdata()->getDeductions() as $deduction ) {                                                             
            // $this->delete($deduction);
            $dedPayment = new DeductionPayment();
            if(null !== $deduction->getBalance()){
                $amt = $deduction->getBalance();
            } else {
                $amt = $deduction->getAmount(); // 10000
            }
            $bal = $amt - $ded;

            $ded = $deduction->getToDeduct(); // 4000
            $deduction->setBalance($bal); // 10000 - 4000
            $deduction->setToDeduct($bal);
            $this->save($deduction);

            if($bal <= 0){
                $dedPayment->setCleared(true); // false
            } else {
                $dedPayment->setCleared(false);
            }
            $dedPayment->setPaidAmt($ded); 
            $dedPayment->setTeacher($teacher);
            $dedPayment->setBalance($bal);
            $dedPayment->setPaidOn(new \Datetime());
            $dedPayment->setDeduction($deduction);
            $this->save($dedPayment);
        }

        return new JsonResponse( $inv);

    }

    /**
     * @Route("/admin/send/statement", name="send_statement")
     */
    public function sendStatement(UserInstrumentGradeRepository $userInstrumentGradeRepo, TermRepository $termRepo, PaymentRepository $paymentRepo, Request $request, Mailer $mailer, Environment $twig): Response
    {        

        
        $dateFrom = $request->request->get('dateFrom');
        $dateTimeFrom = new \Datetime($dateFrom." 00:00:00");
        $dateTo = $request->request->get('dateTo');
        $dateTimeTo = new \Datetime($dateTo." 23:59:59");
        $uig_ids = explode("-", $request->request->get('uig'));
        $uig_ids = array_unique($uig_ids);
        $uigs = [];
        foreach ($uig_ids as $uigid ) {
            $uigs[] = $userInstrumentGradeRepo->findOneById($uigid);
        }
        
       
        $dates=[];
        $student = $uigs[0]->getStudentUserData()->getStudent();
        $payments = $paymentRepo->findAllWithinRange($dateTimeFrom, $dateTimeTo, $student);
        $totalInvoicesBD = $paymentRepo->findSumOfTypeForUserBeforeDate("inv", $dateTimeFrom, $student)['totalAmount'];
        $totalPaymentsBD = $paymentRepo->findSumOfTypeForUserBeforeDate("pmt", $dateTimeFrom, $student)['totalAmount'];
        $totalInvoices = $paymentRepo->findSumOfTypeForUser("inv", $student)['totalAmount'];
        $totalPayments = $paymentRepo->findSumOfTypeForUser("pmt", $student)['totalAmount'];
        $dateTimeFrom->setDate(date('Y'), $dateTimeFrom->format('m'), $dateTimeFrom->format('d'));
        $currentTerm = $termRepo->findCurrentTerm($dateTimeFrom);
        $lastDayOfPreviousTerm = $currentTerm->getStartingOn()->modify("yesterday");
        $balanceCF = $totalInvoicesBD - $totalPaymentsBD;
        $currentBalance = $totalInvoices - $totalPayments;
        $oneToThirdyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '30', '0', $student)['totalAmount'];
        $thirtyToSixtyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '60', '31', $student)['totalAmount'];
        $sixtyToNinetyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '90', '61', $student)['totalAmount'];
        $moreThenNinetyDaysDue = $paymentRepo->findSumOfAllWithinRange("inv", '100000', '91', $student)['totalAmount'];
        $oneToThirdyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '30', '0', $student)['totalAmount'];
        $thirtyToSixtyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '60', '31', $student)['totalAmount'];
        $sixtyToNinetyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '90', '61', $student)['totalAmount'];
        $moreThenNinetyDaysPaid = $paymentRepo->findSumOfAllWithinRange("pmt", '100000', '91', $student)['totalAmount'];

        $html = $twig->render('pdf/statement.html.twig', [
            'student' => $student,
            'payments' => $payments,
            'uigs' => $uigs,
            'balanceCF' => $balanceCF,
            'currentBalance' => $currentBalance,
            'lastDayOfPreviousTerm' => $lastDayOfPreviousTerm,
            'oneToThirdyDaysDue' => $oneToThirdyDaysDue,
            'thirtyToSixtyDaysDue' => $thirtyToSixtyDaysDue,
            'sixtyToNinetyDaysDue' => $sixtyToNinetyDaysDue,
            'moreThenNinetyDaysDue' => $moreThenNinetyDaysDue,
            'oneToThirdyDaysPaid' => $oneToThirdyDaysPaid,
            'thirtyToSixtyDaysPaid' => $thirtyToSixtyDaysPaid,
            'sixtyToNinetyDaysPaid' => $sixtyToNinetyDaysPaid,
            'moreThenNinetyDaysPaid' => $moreThenNinetyDaysPaid,
        ]);

        $mailer->sendEmailWithAttachment(['student' => $student], $student->getEmail(), $student->getFullname().' Your Fee Statement', $html, 'statement.html.twig', $student->getFullname().'-statement');
        $this->addFlash(
            'success',
            'The statement was sent successfully.',
        );
        return new JsonResponse( $uig_ids );

    }

    /**
     * @Route("/admin/send/invoice", name="send_invoice")
     */
    public function sendInvoice(TermRepository $termRepo, PaymentRepository $paymentRepo, Request $request, Mailer $mailer, Environment $twig): Response
    {        
        
        $payment_id = $request->request->get('payment_id');
        $payment = $paymentRepo->findOneById($payment_id);
        $currentTerm = $termRepo->findCurrentTerm($payment->getDoneOn());
        $student = $payment->getUser();
        $html = $twig->render('pdf/student_invoice.html.twig', [
            'payment' => $payment,
            'currentTerm' => $currentTerm,
        ]);

        $mailer->sendEmailWithAttachment(['student' => $student, 'payment' => $payment], $student->getEmail(), $student->getFullname().' Your Invoice dated '.$payment->getDoneOn()->format('d/m/Y'), $html, 'invoice_student.html.twig', $student->getFullname().'-invoice-'.$payment->getDoneOn()->format('d/m/Y'));
        $this->addFlash(
            'success',
            'The invoice was sent successfully.',
        );
        return new JsonResponse( 'test' );

    }

    /**
     * @Route("/homepage/contact/form", name="contactformsend")
     */
    public function contactForm(Request $request, Mailer $mailer, Environment $twig, UserRepository $userRepo): Response
    {        
        
        $name = $request->request->get('name');
        $email = $request->request->get('email');
        $subject = $request->request->get('subject');
        $message = $request->request->get('message');
        $maildata = ['name' => $name, 'emailAd' => $email, 'subject' => $subject, 'message' => $message];

        $admins = $userRepo->findByUsertype('admin');
        foreach ($admins as $admin ) {
            $mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "contactform.html.twig", "necessary", "contact_form");    
        }
        $mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig", "necessary", "contact_form");    

        
        $this->addFlash(
            'success',
            'The message was sent successfully.',
        );
        return new JsonResponse( 'test' );

    }

    /**
     * @Route("/admin/send/report", name="send_report")
     */
    public function sendReport(TermRepository $termRepo, ReportRepository $reportRepo, Request $request, Mailer $mailer, Environment $twig): Response
    {
        
        $report_id = $request->request->get('report_id');
        // $report_id = 5;
        $report = $reportRepo->findOneById($report_id);

        $currentTerm = $termRepo->findCurrentTerm($report->getAddedOn());
        if($report->getUig()->getSiblingname() != null and $report->getUig()->getSiblingname() != ""){
            $fullname = $report->getUig()->getSiblingname();
        } else {
            $fullname = $report->getStudent()->getStudent()->getFullname();
        }

        $html = $twig->render('pdf/student_report.html.twig', [
            'report' => $report,
            'currentTerm' => $currentTerm,
            'fullname' => $fullname,
        ]);
        
        $mailer->sendEmailWithAttachment(['report' => $report, 'fullname' => $fullname, 'currentTerm' => $currentTerm],  $report->getStudent()->getStudent()->getEmail(), $fullname."'s Report for term ".$report->getTerm()->getTermnumber(), $html, 'report_student.html.twig', $fullname.'-report-'.$report->getTerm()->getTermnumber());
        // $this->addFlash(
        //     'success',
        //     'The report was sent successfully.',
        // );
        $report->setSent(true);
        $this->save($report);

        return new JsonResponse( $report->getStudent()->getStudent()->getFullname() );
        // return $this->render('default/test2.html.twig', [
        //     'statements' => $report,
        // ]);

    }

    /**
     * @Route("/student/add/payment", name="add_payment")
     */
    public function addPayment(UserInstrumentGradeRepository $userInstrumentGradeRepo, Request $request, Mailer $mailer): Response
    {        
        $uigid = $request->request->get('uigid');
        $amount = $request->request->get('amount'); // 1000
        $receipt_number = $request->request->get('receipt_number'); 
        $uig = $userInstrumentGradeRepo->findOneById($uigid);
        $student = $uig->getStudentUserData()->getStudent();

        $em = $this->getDoctrine()->getManager();
        $RAW_QUERY = 'SELECT SUM(amount) AS totalamount FROM payment where payment.uig_id = :thisuig and payment.type = :pmt ;';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters 
        $statement->bindValue('thisuig', $uig->getId());
        $statement->bindValue('pmt', "pmt");
        $statement->execute();

        $sumOfPayments = $statement->fetchAll();
        $allpaymentsmade = $sumOfPayments[0]['totalamount'];// 18000

        $totalpaid = $allpaymentsmade + $amount; // 18000 + 1000 = 19000
        $uig->setPayment($totalpaid);
        $this->save($uig);

        $fullfee = $uig->getSessions()[0]->getPackage()->getPrice(); // 24000
        $onelessonfee = $fullfee / 12; // 2000
        $sessions = count($uig->getSessions()); //12
        $realfee = $onelessonfee * $sessions; // 24000
        $balance = $realfee - $totalpaid; // 24000 - 19000 = 5000

        $newPayment = new Payment();
        $newPayment->setAmount($amount);
        $newPayment->setDoneOn(new \Datetime("now"));
        $newPayment->setUser($student);
        $newPayment->setDocNumber($receipt_number);
        $newPayment->setType("pmt");
        $newPayment->setUig($uig);
        $newPayment->setBalance($balance);
        $this->save($newPayment);
        
        $maildata = ['amount' => $amount, 'newbal' => $balance, 'student' => $student];
        //$mailer->sendEmailMessage($maildata, $student->getEmail(), "Payment Received", "payment_received.html.twig", "necessary", "payment_received");    
        $this->addFlash(
            'success',
            'Payment of ' . $amount . " for " . $student->getFullname() . " has been made. Balance is now " . $balance ,
        );

        return new JsonResponse($balance);

    }

    /**
     * @Route("/student/edit/payment", name="edit_payment")
     */
    public function editPayment(PaymentRepository $paymentRepo, Request $request, Mailer $mailer): Response
    {        
        $pmtid = $request->request->get('pmtid');
        $amount = $request->request->get('amount');
        $payment = $paymentRepo->findOneById($pmtid); // the entry to be edited

        $uig = $payment->getUig(); // get the instrument paid for
        $fullfee = $uig->getSessions()[0]->getPackage()->getPrice();
        $onelessonfee = $fullfee / 12;
        $sessions = count($uig->getSessions());
        $realfee = $onelessonfee * $sessions; // actual fee to pay


        $payment->setAmount($amount); // set the new amount
        $this->save($payment); // save the edited entry

        $em = $this->getDoctrine()->getManager();
        $RAW_QUERY = 'SELECT SUM(amount) AS totalamount FROM payment where payment.uig_id = :thisuig;';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters 
        $statement->bindValue('thisuig', $uig->getId());
        $statement->execute();

        $sumOfPayments = $statement->fetchAll();


        $totalpaid = $sumOfPayments[0]['totalamount']; // all the money paid updated and correct

        $balance = $realfee - $totalpaid;

        $payment->setBalance($balance);
        $this->save($payment);

        $this->addFlash(
            'success',
            'Payment for '. $uig->getInstrument() .' was successfully edited',
        );

        return new JsonResponse($balance);

    }

    /**
     * @Route("/teacher/print/form/for/{uig_id}", name="print_form")
     */
    public function printFormTest(Request $request, UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo, $uig_id): Response
    {        

        $uig = $uigRepo->findOneById($uig_id);
        $student = $userRepo->findOneById($uig->getSessions()[0]->getStudent());
        return $this->toPdf('pdf/reg_form.html.twig', ['student' => $student, 'uig' => $uig], $student->getFullname()."-Registration-form");
        
        // return $this->render('pdf/student_form.html.twig', [
        //     'student' => $student,
        // ]);

    }

    public function toPdf($path = '', $array = [], $filename): Response
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        //Generate pdf with the retrieved HTML
        return new Response( $this->snappy_pdf->getOutputFromHtml($html), 200, array(
            'Content-Type'          => 'application/pdf',
            'Content-Disposition'   => 'inline; filename='.$filename.'.pdf'
        )
        );        
    }
    

    /**
     * @Route("/add/user/instrument", name="addinstrument")
     */
    public function addinstrument(Request $request, InstrumentRepository $instrumentRepo, InstrumentGradeRepository $gradeRepo): Response
    {
        $user = $this->getUser();
        if($user->getUsertype() == 'student'){
            $userdata = $user->getStudentdata();
        } else if ($user->getUsertype() == 'teacher'){
            $userdata = $user->getTeacherdata();
        }

        $ins = $request->request->get('instrument');
        $grade = $request->request->get('grade');

        $instrument = $instrumentRepo->findOneById($ins);
        $grade = $gradeRepo->findOneById($grade);
        $userInstrument = new UserInstrumentGrade();
        $userInstrument->setInstrument($instrument);
        
        if($user->getUsertype() == 'student'){
            $userInstrument->setStudentUserData($userdata);

            // activate uig if student is activated
            $active = $user->getActive();
            $entityManager = $this->getDoctrine()->getManager();

            if($active){
                $userInstrument->setActive(true);
                $entityManager->persist($userInstrument); 
            } 

        } else if ($user->getUsertype() == 'teacher'){
            $userInstrument->setTeacherUserData($userdata);
        }        

        $userInstrument->setGrade($grade);

        $this->save($userInstrument);
        $html = '
        <button type="button" id="uigid-' . $userInstrument->getId() . '" class="btn btn-secondary">
            '. $userInstrument->getInstrument()->getName() . ' ' . $userInstrument->getGrade()->getName() . '&nbsp;&nbsp;&nbsp;<span id="delete-'. $userInstrument->getId() .'" class="badge badge-danger">x</span>
        </button>                                                    
        ';


        return new JsonResponse($html);

    }
    
    /**
     * @Route("/remove/user/instrument", name="removeinstrument")
     */
    public function removeinstrument(Request $request, UserInstrumentGradeRepository $instrumentGradeRepo): Response
    {

        $uig = $request->request->get('uigid');

        $uiGrade = $instrumentGradeRepo->findOneById($uig);
        $this->delete($uiGrade);

        return new JsonResponse($uig);

    }
    
    /**
     * @Route("/admin/communications/delete/template/", name="delete_template")
     */
    public function deleteTemplate(Request $request, TemplateRepository $templateRepo): Response
    {

        $template_id = $request->request->get('template_id');

        $template = $templateRepo->findOneById($template_id);
        $this->delete($template);

        return new JsonResponse($template->getId());

    }
    
    /**
     * @Route("/communications/get/template/message", name="get_template_message")
     */
    public function getTemplateMessage(Request $request, TemplateRepository $templateRepo): Response
    {

        $template_id = $request->request->get('template_id');

        $template = $templateRepo->findOneById($template_id);

        return new JsonResponse($template->getMessage());

    }
    
    /**
     * @Route("/user/settings/change/setting", name="change_setting")
     */
    public function changeSetting(Request $request, SettingsRepository $settingsRepo, UserSettingsRepository $userSettingsRepo): Response
    {
        $setting_id = $request->request->get('setting_id');
        $state = $request->request->get('state');
        $setting = $settingsRepo->findOneById($setting_id);
        $user = $this->getUser();
        $userSetting = $userSettingsRepo->findOneBy(
            array('user' => $user, 'setting' => $setting),
            array('id' => 'ASC'),
        );
        if(empty($userSetting)){
            $userSetting = new UserSettings();
            $userSetting->setUser($user);
        }

        $userSetting->setSetting($setting);
        $userSetting->setValue($state);
        $this->save($userSetting);

        return new JsonResponse("Setting Saved");

    }
    
    /**
     * @Route("/user/settings/save/preference", name="save_preference")
     */
    public function savePreference(Request $request, SettingsRepository $settingsRepo, UserSettingsRepository $userSettingsRepo): Response
    {
        $setting_id = $request->request->get('setting_id');
        $preference = $request->request->get('preference');
        $setting = $settingsRepo->findOneById($setting_id);
        $user = $this->getUser();
        $userSetting = $userSettingsRepo->findOneBy(
            array('user' => $user, 'setting' => $setting),
            array('id' => 'ASC'),
        );
        if(empty($userSetting)){
            $userSetting = new UserSettings();
            $userSetting->setUser($user);
        }

        $userSetting->setSetting($setting);
        $userSetting->setValue($preference);
        $this->save($userSetting);

        return new JsonResponse("Setting Saved");

    }
    
    /**
     * @Route("/admin/save/message/template", name="save_template")
     */
    public function saveTemplate(Request $request, AttachmentRepository $attachRepo): Response
    {

        $msg = $request->request->get('msg');
        $type = $request->request->get('type');
        $title = $request->request->get('title');
        if($request->request->get('attachment') != ""){
            $attachment = $attachRepo->findOneById($request->request->get('attachment'));
            $attachmentFile = $attachment->getFilename();
        } else {
            $attachmentFile = null;
            $attachment = null;
        }
        
        $data = [];

        $template = new Template();

        $template->setMessage($msg);
        $template->setType($type);
        $template->setTitle($title);
        $template->setAttachment($attachment);
        
        $this->save($template);
        $data['msg'] = $template->getMessage();
        $data['title'] = $template->getTitle();
        $data['type'] = $template->getType();
        $data['id'] = $template->getId();
        $data['attachment'] = $attachmentFile;
        return new JsonResponse($data);

    }
    
    /**
     * @Route("/test/test/test", name="multitesting")
     */
    public function testingmultiplethings(PaymentRepository $paymentRepo, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo): Response
    {
        // $statements = [];
        // $userInstrumentGrades = $uigRepo->findUIGForStudents();
        // foreach ($userInstrumentGrades as $uig) {
            // if($uig->getStudentUserData()->getStudent() == null){
            //     $statements[$uig->getId()] = "DELETE FROM user_instrument_grade WHERE id = " . $uig->getId();
            // } else {
            //     $statements[$uig->getId()] = "UPDATE user_instrument_grade SET user_id = " . $uig->getStudentUserData()->getStudent()->getId() . " WHERE id = " . $uig->getId();
            // }
            // if(count($uig->getSessions()) > 1){
            //     $statements[$uig->getId()] = "UPDATE user_instrument_grade SET package_id = ".$uig->getSessions()[0]->getPackage()->getId(). " WHERE id = " . $uig->getId();
            // }
        //     $countOfInvoices = $paymentRepo->countEntriesWithType("inv");
        //     $nextInvoiceNumber = $countOfInvoices + 1;

        //     $fullfee = $uig->getSessions()[0]->getPackage()->getPrice();
        //     $onelessonfee = $fullfee / 12;
        //     $sessions = count($uig->getSessions());
        //     $realfee = $onelessonfee * $sessions;

        //     $payment = new Payment();
        //     $payment->setDoneOn(new \Datetime("now"));
        //     $payment->setAmount($uig->getPayment());
        //     $payment->setType("inv");
        //     $payment->setDocNumber($nextInvoiceNumber);
        //     $payment->setBalance($realfee - $uig->getPayment());
        //     $payment->setUser($uig->getStudentUserData()->getStudent());
        //     $payment->setUig($uig);
        //     $statements[] = $payment;
        //     $this->save($payment);
            
        // }
        // $test = [];
        // $user_ids = [];
        // $roletype = 'ROLE_STUDENT';
        // $uigs = $uigRepo->findBy(
        //     array('active' => true),
        //     array('id' => 'ASC')
        // );
        // foreach ($uigs as $uig) {
        //     if(null !== $uig->getStudentUserData()){
        //         if(null !== $uig->getStudentUserData()->getStudent()){
        //             $user_ids[] = $uig->getStudentUserData()->getStudent()->getId();

        //         }
                
        //     }
            
        // }
        // foreach($user_ids as $user_id){
        //     $user = $userRepo->findOneById($user_id);
        //     $has_role = $user->hasRole($user->getRoles(), $roletype);
            
        //     $roles = [];
        //     $roles[] = $roletype;
        //     $user->setRoles([$roletype]);
        //     $user->setActive(true);
            
        //     $test[] = $user->getFullname();
            
        //     $em = $this->getDoctrine()->getManager();
        //     $em->persist($user); 
        //     $em->flush();
    
        // }

        // $arr = [
        //         ['date_creation' => 'Apr 10, 2021 10:17 pm', 'idformation' => 84, 'idsociete' => 7, 'training' => 'ELECTRICAL SAFETY TRAINING', 'company' => 'ALUCAM'],
        //         ['date_creation' => 'Apr 10, 2021 10:17 pm', 'idformation' => 84, 'idsociete' => 7, 'training' => 'ELECTRICAL SAFETY TRAINING', 'company' => 'ALUCAM'],
        //         ['date_creation' => 'Apr 12, 2021 03:27 pm', 'idformation' => 104, 'idsociete' => 201, 'training' => 'FORKLIFT, JLG SCISSOR LIFT, AERAL PLATFORM', 'company' => 'US EMBASSY'],
        // ];

        // $training = 'US EMBASSY';
        
        // $companies = array_count_values(array_column($arr, 'company'))[$training]; // outputs: 2
        $arr = ["templates/default/classes.html.twig", "templates/default/index.html.twig", "templates/default/welcome.html.twig", "templates/post/show.html.twig", "templates/public_base.html.twig", "templates/registration/register.html.twig", "templates/templates/dt_nav_links.html.twig"];
        $proj_dir = $this->getParameter('proj_dir');
        $test = [];
        foreach($arr as $a){
            $src = $proj_dir . "/" . $a;
            $sections = explode("/", $a);
            $file = array_pop($sections);
            $ext = explode(".", $file, 2)[1];
            $destfolder = $proj_dir . "/test";
            foreach ($sections as $section ) {
                $destfolder .= "/".$section;
            }
            if (!file_exists($destfolder)) {
                mkdir($destfolder, 0775, true);
            }            
            $dest = $destfolder . "/" . $file;
            copy($src, $destfolder . "/" . $file);

            $test[] = $dest;
        }
            
        return $this->render('default/test2.html.twig', [
            'arr' => $arr,
            'statements' => $test,
        ]);
    }

    /**
     * @Route("/payment/delete/one/{payment_id}", name="payment_delete_one", methods={"GET"})
     */
    public function deleteOne(Request $request, PaymentRepository $paymentRepo, $payment_id): Response
    {
        $payment = $paymentRepo->findOneById($payment_id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($payment);
        $entityManager->flush();
        // return $this->redirectToRoute('user_instrument_grade_show');
        return $this->redirectToRoute('admin-payments-list', ['uig_id' => $payment->getUig()->getId()]);
    }

    /**
     * @Route("/communications/template/save/edit", name="edit_template")
     */
    public function editTemplate(Request $request, TemplateRepository $templateRepo): Response
    {
        $template_id = $request->request->get('template_id');
        $message = $request->request->get('msg');
        $title = $request->request->get('title');
        $type = $request->request->get('type');

        $template = $templateRepo->findOneById($template_id);

        $template->setType($type);
        $template->setMessage($message);
        $template->setTitle($title);
        $this->save($template);

        return new JsonResponse($template->getType());
    }

    /**
     * @Route("/save/user/comment", name="savecomment")
     */
    public function saveComment(Request $request, PostRepository $postRepo): Response
    {
        $dt = [];
        $email = $request->request->get('email');
        $name = $request->request->get('name');
        $content = $request->request->get('comment');
        $post = $postRepo->findOneById($request->request->get('post_id'));
        $comment = new Comment();
        $comment->setContent($content);
        $comment->setCreatedAt(new \DateTime());
        $comment->setName($name);
        $comment->setEmail($email);
        $comment->setPost($post);

        $this->save($comment);
        // send email to concerned parties
        $dt['name'] = $name;
        $dt['content'] = $content;
        $dt['time'] = $comment->getCreatedAt()->format("F j, Y \a\t h:i a");
        $dt['id'] = $comment->getId();
        return new JsonResponse($dt);

    }

    /**
     * @Route("/save/comment/reply", name="save_reply")
     */
    public function saveReply(Request $request, CommentRepository $commentRepo): Response
    {
        $dt = [];
        $r_email = $request->request->get('r_email');
        $r_name = $request->request->get('r_name');
        $content = $request->request->get('reply');
        $comment = $commentRepo->findOneById($request->request->get('comment_id'));
        $reply = new CommentReply();
        $reply->setContent($content);
        $reply->setCreatedAt(new \DateTime());
        $reply->setRName($r_name);
        $reply->setREmail($r_email);
        $reply->setComment($comment);

        $this->save($reply);
        // send email to concerned parties
        $dt['name'] = $r_name;
        $dt['content'] = $content;
        $dt['time'] = $reply->getCreatedAt()->format("F j, Y \a\t h:i a");
        $dt['id'] = $comment->getId();
        return new JsonResponse($dt);

    }


    
}
