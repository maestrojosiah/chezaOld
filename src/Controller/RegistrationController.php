<?php

namespace App\Controller;

use App\Entity\StudentUserData;
use App\Entity\TeacherUserData;
use App\Entity\User;
use App\Entity\UserInstrumentGrade;
use App\Form\RegistrationFormType;
use App\Security\EmailVerifier;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use App\Repository\InstrumentRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Sonata\SeoBundle\Seo\SeoPageInterface;

/**
 * @Route("/accounts")
 */
class RegistrationController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    /**
     * @Route("/register/{usertype}", name="app_register")
     */
    public function register(
        Request $request, 
        UserPasswordEncoderInterface $passwordEncoder, 
        $usertype, InstrumentRepository $instrumentRepo,
        Mailer $mailer,
        UserRepository $userRepo,
        SeoPageInterface $seoPage,
        InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        $user = new User();
        $users = $userRepo->findAll();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        $instrument = "";
        $data = [];
        // get the instrument from url if available
        if (isset($_SERVER['QUERY_STRING']) && $_GET){
            $query = explode("=", $_SERVER['QUERY_STRING'])[1];
        } else {
            $query = "";
        }

        if($usertype == 'student'){
            $instruments = $instrumentRepo->findBy(
                array('category' => 'instrument'),
                array('id' => 'ASC')
            );
            $courses = $instrumentRepo->findBy(
                array('category' => 'course'),
                array('id' => 'ASC')
            );
            $traditional = $instrumentRepo->findBy(
                array('category' => 'traditional'),
                array('id' => 'ASC')
            );
            $data['instruments'] = $instruments;
            $data['courses'] = $courses;
            $data['traditional'] = $traditional;
    
            $userdata = new StudentUserData;

            $instrumentGrades = $instrumentGradeRepo->findAll();
            $data['instrumentGrades'] = $instrumentGrades;

        }


        if($usertype == 'teacher'){
            $userdata = new TeacherUserData;
        }

        $data['usertype'] = $usertype;

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setActive(false);
            $user->setUsertype($usertype);
            if(count($users) == 0 && $usertype == 'admin'){
                $user->setRoles(['ROLE_ADMIN']);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);

            if(isset($_POST['instrument']) && $usertype == 'student'){
                $instrument = $instrumentRepo->findOneById($_POST['instrument']);
                $phone = $_POST['phone'];
                $userInstrument = new UserInstrumentGrade();
                $userInstrument->setStudentUserData($userdata);
                $userInstrument->setGrade($instrumentGradeRepo->findOneById($_POST['grade']));
                $userInstrument->setInstrument($instrument);
                $entityManager->persist($userInstrument);

                $userdata->addUserInstruments($userInstrument);
                $userdata->setPhone($phone);
                $entityManager->persist($userdata);
                
                $user->setStudentdata($userdata);
                $entityManager->persist($user);

            } else if ($usertype == 'teacher'){
                $user->setTeacherdata($userdata);
                $entityManager->persist($userdata);
                $entityManager->persist($user);
            }

            $entityManager->flush();

            // generate a signed url and email it to the user
            $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                (new TemplatedEmail())
                    ->from(new Address('info@chezamusicschool.co.ke', 'Cheza Music School'))
                    ->to($user->getEmail())
                    ->subject('Please Confirm your Email')
                    ->htmlTemplate('registration/confirmation_email.html.twig')
            );
            $data = ['student' => $user, 'instrument' => $instrument];
            if ($usertype == 'student'){
                $mailer->sendEmailMessage($data, $user->getEmail(), "Welcome to Cheza Music School", "welcome.html.twig", "necessary", "welcome_message");    
            } elseif ($usertype == 'teacher') {
                $mailer->sendEmailMessage($data, $user->getEmail(), "Welcome to Cheza Music School", "welcome_teacher.html.twig", "necessary", "welcome_message");    
            }
            // do anything else you need here, like send an email
            // send email to alert admins of a new student
            $admins = $userRepo->findByUsertype('admin');
            if ($user->getUsertype() == 'student') {
                foreach ($admins as $admin ) {
                    $maildata = ['student' => $user];
                    $mailer->sendEmailMessage($maildata, $admin->getEmail(), "New Student Registration Alert", "new_student.html.twig", "necessary", "registration_successful");    
                }
    
            }

            return $this->redirectToRoute('welcome_page', ['user_id' => $user->getId()]);
        }

        $seoPage
            ->setTitle(ucfirst($usertype) . ' Registration')
            ->addMeta('name', 'keywords', 'Learn your favorite instrument. Join Cheza Music Today')
            ->addMeta('name', 'title', 'Join Cheza Music School Today')
            ->addMeta('name', 'description', 'Join Cheza Music School today and learn from professional musicians at reasonable rates.')
            ->addMeta('property', 'og:title', 'Join Cheza Music School Today')
            ->addMeta('property', 'og:type', 'website')
            ->addMeta('property', 'og:image', "https://chezamusicschool.co.ke/adm/assets/images/cheza_logo_sm.jpg")
            ->addMeta('property', 'og:url', 'https://chezamusicschool.co.ke/accounts/register/'.$usertype )
            ->addMeta('property', 'og:description', 'Join Cheza Music School today and learn from professional musicians at reasonable rates.')
        ;

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'data' => $data,
            'query' => $query,            
        ]);
    }

    /**
     * @Route("/verify/email", name="app_verify_email")
     */
    public function verifyUserEmail(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());

            return $this->redirectToRoute('app_login');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Your email address has been verified.');

        return $this->redirectToRoute('app_login');
    }
}
