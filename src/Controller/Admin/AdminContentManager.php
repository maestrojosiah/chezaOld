<?php

namespace App\Controller\Admin;

use App\Entity\Instrument;
use App\Entity\InstrumentGrade;
use App\Entity\Package;
use App\Entity\Term;
use App\Entity\Deductions;
use App\Entity\DeductionPayment;
use App\Form\InstrumentGradeType;
use App\Form\InstrumentType;
use App\Form\PackageType;
use App\Form\TermType;
use App\Form\DeductionsType;
use App\Form\DeductionPaymentType;
use App\Repository\InstrumentGradeRepository;
use App\Repository\InstrumentRepository;
use App\Repository\PackageRepository;
use App\Repository\TermRepository;
use App\Repository\DeductionsRepository;
use App\Repository\DeductionPaymentRepository;
use App\Repository\SchoolTimeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/content/management/area")
 */
class AdminContentManager extends AbstractController
{


    /**
     * @Route("/index/{entity}", name="user_admin_index", methods={"GET"})
     */
    public function index(
        InstrumentGradeRepository $grades, 
        InstrumentRepository $instruments, 
        PackageRepository $packages,
        TermRepository $terms,
        DeductionsRepository $deductions,
        DeductionPaymentRepository $deductionPayments,
        SchoolTimeRepository $schedules,
        $entity = ''): Response
    {
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $this->getUser();
        $title = ucfirst($entity);

        switch ($entity) {
            case 'grades':
                $entity = $grades->findAll();
                break;
            case 'instruments':
                $entity = $instruments->findAll();
                break;
            case 'packages':
                $entity = $packages->findAll();
                break;
            case 'deductions':
                $entity = $deductions->findAll();
                break;
            case 'deductionPayments':
                $entity = $deductionPayments->findAll();
                break;
            case 'terms':
                $entity = $terms->findAll();
                break;
        }

        $sidebar_menu = [ 'grades', 'instruments', 'packages', 'deductions', 'deductionPayments', 'terms' ];

        return $this->render('admin/index.html.twig', [
            'user' => $user,
            'sidebar_menu' => $sidebar_menu,
            'entity' => $entity,
            'title' => $title,
        ]);
    }

    /**
     * @Route("/edit/{entity_name}/{id}", name="admin_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, $entity_name, $id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $usable_entity_name = rtrim(ucfirst($entity_name), "s");
        $user = $this->getUser();
        switch ($entity_name) {
            case 'grades':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:InstrumentGrade")->find($id);
                $form = $this->createForm(InstrumentGradeType::class, $entity);
                break;
            case 'instruments':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:Instrument")->find($id);
                $form = $this->createForm(InstrumentType::class, $entity);
                break;
            case 'packages':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:Package")->find($id);
                $form = $this->createForm(PackageType::class, $entity);
                break;
            case 'deductions':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:Deductions")->find($id);
                $form = $this->createForm(DeductionsType::class, $entity);
                break;
            case 'deductionPayments':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:DeductionPayment")->find($id);
                $form = $this->createForm(DeductionPaymentType::class, $entity);
                break;
            case 'terms':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:Term")->find($id);
                $form = $this->createForm(TermType::class, $entity);
                break;
            default:
                $entity = $this->getDoctrine()->getManager()->getRepository("App:Instrument")->find($id);
                $form = $this->createForm(InstrumentType::class, $entity);
                break;
        }
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($usable_entity_name == 'Instrument'){
                $image = $form->get('image')->getData();
                $category = $form->get('category')->getData();            
                $instrument_title = $form->get('name')->getData();
                $trimmed_title = str_replace(" ", "_", $instrument_title);
                $originalName = $image->getClientOriginalName();;
                $filepath = $this->getParameter('instrument_img_directory')."/$category/$trimmed_title/";
                $image->move($filepath, $originalName);
                $simple_filepath = "/img/instruments/$category/$trimmed_title/";
                $entity->setImage($simple_filepath . $originalName);    
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('user_admin_index', ['entity' => $entity_name]);
        }

        $sidebar_menu = [ 'grades', 'instruments', 'packages', 'deductions', 'deductionPayments', 'terms' ];

        return $this->render('admin/edit.html.twig', [
            'entity' => $entity,
            'user' => $user,
            'form' => $form->createView(),
            'usable_entity_name' => $usable_entity_name,
            'sidebar_menu' => $sidebar_menu,
        ]);
    }

    /**
     * @Route("/add/{entity_name}", name="admin_add", methods={"GET","POST"})
     */
    public function add(Request $request, $entity_name): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $usable_entity_name = rtrim(ucfirst($entity_name), "s");
        $user = $this->getUser();
        switch ($entity_name) {
            case 'grades':
                $entity = new InstrumentGrade();
                $form = $this->createForm(InstrumentGradeType::class, $entity);
                break;
            case 'instruments':
                $entity = new Instrument();
                $form = $this->createForm(InstrumentType::class, $entity);
                break;
            case 'packages':
                $entity = new Package();
                $form = $this->createForm(PackageType::class, $entity);
                break;
            case 'deductions':
                $entity = new Deductions();
                $form = $this->createForm(DeductionsType::class, $entity);
                break;
            case 'deductionPayments':
                $entity = new DeductionPayment();
                $form = $this->createForm(DeductionPaymentType::class, $entity);
                break;
            case 'terms':
                $entity = new Term();
                $form = $this->createForm(TermType::class, $entity);
                break;
            default:
                $entity = new Instrument();
                $form = $this->createForm(InstrumentType::class, $entity);
                break;
        }
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($usable_entity_name == 'Instrument'){
                $image = $form->get('image')->getData();
                $category = $form->get('category')->getData();            
                $instrument_title = $form->get('name')->getData();
                $trimmed_title = str_replace(" ", "_", $instrument_title);
                $originalName = $image->getClientOriginalName();;
                $filepath = $this->getParameter('instrument_img_directory')."/$category/$trimmed_title/";
                $image->move($filepath, $originalName);
                $simple_filepath = "/img/instruments/$category/$trimmed_title/";
                $entity->setImage($simple_filepath . $originalName);    
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('user_admin_index', ['entity' => $entity_name]);
        }

        $sidebar_menu = [ 'grades', 'instruments', 'packages', 'deductions', 'deductionPayments', 'terms' ];

        return $this->render('admin/add.html.twig', [
            'entity' => $entity,
            'user' => $user,
            'form' => $form->createView(),
            'usable_entity_name' => $usable_entity_name,
            'sidebar_menu' => $sidebar_menu,
        ]);
    }

    /**
     * @Route("/delete/{entity_name}/{id}", name="admin_delete", methods={"DELETE"})
     */
    public function delete(Request $request, $entity_name, $id): Response
    {
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        switch ($entity_name) {
            case 'grades':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:InstrumentGrade")->find($id);
                break;
            case 'instruments':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:Instrument")->find($id);
                break;
            case 'packages':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:Package")->find($id);
                break;
            case 'deductions':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:Deductions")->find($id);
                break;
            case 'deductionPayments':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:DeductionPayment")->find($id);
                break;
            case 'terms':
                $entity = $this->getDoctrine()->getManager()->getRepository("App:Term")->find($id);
                break;
            default:
                $entity = $this->getDoctrine()->getManager()->getRepository("App:Instrument")->find($id);
                break;
        }
        
        if ($this->isCsrfTokenValid('delete'.$entity->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entity);
            $entityManager->flush();

        }

        return $this->redirectToRoute('user_admin_index', ['entity' => $entity_name]);
    }


}