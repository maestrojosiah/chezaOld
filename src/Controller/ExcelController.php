<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Repository\InstrumentRepository;
use App\Entity\UserInstrumentGrade;
use App\Entity\TeacherUserData;
use App\Repository\InstrumentGradeRepository;
use App\Repository\SessionRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\UserRepository;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use App\Service\ExcelExport;

class ExcelController extends AbstractController
{

    private $userRepo;
    private $uigRepo;
    private $sessionRepo;

    public function __construct( UserRepository $userRepo, UserInstrumentGradeRepository $uigRepo, SessionRepository $sessionRepo)
    {
        $this->userRepo = $userRepo;
        $this->uigRepo = $uigRepo;
        $this->sessionRepo = $sessionRepo;

    }

    /**
     * @Route("/excel", name="excel")
     */
    public function index(): Response
    {
        return $this->render('excel/index.html.twig', [
            'controller_name' => 'ExcelController',
        ]);
    }
    
    /**
     * @Route("/excel/import/data/from/file", name="excel_import")
     */
    public function insertAction(UserPasswordEncoderInterface $passwordEncoder, InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo)
    {
        set_time_limit(0);
        $excelfile = $this->getParameter('excel_files')."teachers.xlsx";
        $spreadsheet = IOFactory::load($excelfile);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $test = [];
        foreach($sheetData as $i=>$row) {
            if($i > 4 && $i <= 34) {
                $fname = explode(" ", $row['B'])[0];
                $rowA = $row['A'] == null ? 'Not Set'.$i : $row['A'];
                $rowB = $row['B'] == null ? 'Not Set'.$i : $row['B'];
                $rowC = $row['C'] == null ? 'Not Set'.$i : $row['C'];
                $rowD = $row['D'] == null ? 'Not Set'.$i : $row['D'];
                $rowE = $row['E'] == null ? 'Not Set'.$i : $row['E'];
                $rowF = $row['F'] == null ? 'Not Set'.$i : $row['F'];
                $rowG = $row['G'] == null ? 'Not Set'.$i : $row['G'];
                $user = new User();
                $usertype = "teacher";
                // encode the plain password
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $rowC.$fname
                    )
                );
    
                $user->setActive(true);
                $user->setUsertype($usertype);
                $user->setFullname($rowB);
                $user->setEmail($rowG);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
    
                if($instrument = $instrumentRepo->findOneByName($rowC)){
                    $test[]  = $instrument->getName();
                } else {
                    $instrument = $instrumentRepo->findOneById(1);
                    $test[]  = $instrument->getName();

                }

                $userdata = new TeacherUserData;
                

                $userdata->setNextofkinname(null);
                $userdata->setNextofkinemail(null);
                $userdata->setNextofkinphonenum(null);
                $entityManager->persist($userdata);

                $userInstrument = new UserInstrumentGrade();
                $userInstrument->setTeacherUserData($userdata);
                $userInstrument->setGrade($instrumentGradeRepo->findOneById(1));
                $userInstrument->setInstrument($instrument);
                $entityManager->persist($userInstrument);

                $user->setTeacherdata($userdata);
    
                $userdata->addUserInstruments($userInstrument);
                
                $entityManager->persist($user);
    
                $entityManager->flush();
    
            }

        }
        return new JsonResponse($test);          
    }

    /**
     * @Route("/excel/export/data/to/excel/{filetype}/{entity}", name="file_export")
     */
    public function exportFile(ExcelExport $excelexport, $filetype, $entity){
        $data = [];
        if (strpos($entity, 'user-') === 0) {
            $usertype = explode("-", $entity)[1];
            $cpusertype = ucfirst($usertype).'s';
            $ths = ['A1' => 'Name', 'B1' => 'Email'];
            $title = "$cpusertype List";
            $description = "A list of all $usertype";
            $keywords = "$cpusertype List";
            $category = "$cpusertype Excel Export";
            $filename = "Cheza-$cpusertype-List";
            $admins = $this->userRepo->findByUsertype($usertype);
            foreach ($admins as $admin ) {
                $data[] = ['name' => $admin->getFullname(), 'email' => $admin->getEmail()];
            }
        }
        if (strpos($entity, 'phoneactiveuser-') === 0) {
            $usertype = explode("-", $entity)[1];
            $cpusertype = ucfirst($usertype).'s';
            $ths = ['A1' => 'Name', 'B1' => 'Phone'];
            $title = "$cpusertype List";
            $description = "A list of all $usertype";
            $keywords = "$cpusertype List";
            $category = "$cpusertype Excel Export";
            $filename = "Cheza-$cpusertype-List";
            $admins = $this->userRepo->findBy(
                array('usertype' => $usertype, 'active' => 1),
                array('id' => 'DESC')
            );
            foreach ($admins as $admin ) {
                $data[] = ['name' => $admin->getFullname(), 'phone' => $admin->getStudentdata()->getPhone()];
            }
        }
        if($entity == 'uig'){
            $ths = ['A1' => 'Student', 'B1' => 'Instrument', 'C1' => 'Teacher'];
            $title = "Sessions List";
            $description = "A list of all sessions";
            $keywords = "Sessions List";
            $category = "Sessions Excel Export";
            $filename = "Cheza-All-Sessions-List";
            $uigs = $this->uigRepo->findUIGForStudents();
            foreach ($uigs as $uig ) {
                $firstsession = $uig->getSessions()[0];
                $data[] = ['Student' => explode("-", $uig)[0], 'Instrument' => explode("-", $uig)[1], $firstsession->getTeacher()];
            }
 
        }
        if (strpos($entity, 'session-for-') === 0) {
            $id = explode("-", $entity)[2];
            $ths = ['A1' => 'Date', 'B1' => 'Begin At', 'C1' => 'End At', 'D1' => 'Cleared'];
            $title = "Sessions List";
            $description = "A list of all sessions";
            $keywords = "Sessions List";
            $category = "Sessions Excel Export";
            $uig = $this->uigRepo->findOneById($id);
            $filename = "Cheza-Sessions-For-".$uig;

            $sessionscount = count($uig->getSessions());
            $total_fee = $uig->getSessions()[0]->getPackage()->getPrice() / 12 * $sessionscount;
            $feeperlesson = $total_fee / $sessionscount;
            $paid_amt = 0;
            foreach ($uig->getPayments() as $payment) {
                if($payment->getType() == 'pmt'){
                    $paid_amt += $payment->getAmount();
                }
            }
            $balance = $total_fee - $paid_amt;
            $bal = $paid_amt;
            $sessions = $uig->getSessions();
            foreach ($sessions as $session ) {
                $bal -= $feeperlesson;
                $data[] = ['Date' => $session->getStartingOn()->format('d/m/Y'), 'Begin At' => $session->getBeginAt()->format('H:i'), 'End At' => $session->getEndAt()->format('H:i'), $bal >= $feeperlesson ? 'Cleared' : 'Not Cleared'];
            }
 
        }
        
        // $ths = ['A1' => 'name', 'B1' => 'email'];
        // $data = [['name' => 'Someone Omori', 'email' => 'someone@mail.com'], ['name' => 'Another Onyango', 'email' => 'another@mail.com']];
        if($filetype == 'excel'){
            $excelexport->exportExcelFile($data, $this->getUser(), $ths, $title, $description, $keywords, $category, $filename);
        } elseif ($filetype == 'pdf'){
            $excelexport->exportPdfFile($data, $this->getUser(), $ths, $title, $description, $keywords, $category, $filename);
        }
    }

}
