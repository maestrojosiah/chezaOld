<?php

namespace App\Controller;

use App\Entity\Attachment;
use App\Form\AttachmentType;
use App\Repository\AttachmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/attachment")
 */
class AttachmentController extends AbstractController
{
    /**
     * @Route("/", name="attachment_index", methods={"GET"})
     */
    public function index(AttachmentRepository $attachmentRepository): Response
    {
        return $this->render('attachment/index.html.twig', [
            'attachments' => $attachmentRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="attachment_new", methods={"GET","POST"})
     */
    public function new(Request $request, SluggerInterface $slugger): Response
    {
        $attachment = new Attachment();
        $form = $this->createForm(AttachmentType::class, $attachment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('file')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($uploadedFile) {
                $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile->move(
                        $this->getParameter('attachment_files'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'uploadedFilename' property to store the PDF file name
                // instead of its contents
                $attachment->setFile($newFilename);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($attachment);
            $entityManager->flush();

            return $this->redirectToRoute('attachment_index');
        }

        return $this->render('attachment/new.html.twig', [
            'attachment' => $attachment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="attachment_show", methods={"GET"})
     */
    public function show(Attachment $attachment): Response
    {
        return $this->render('attachment/show.html.twig', [
            'attachment' => $attachment,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="attachment_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Attachment $attachment, SluggerInterface $slugger): Response
    {
        $form = $this->createForm(AttachmentType::class, $attachment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('file')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($uploadedFile) {
                $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile->move(
                        $this->getParameter('attachment_files'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'uploadedFilename' property to store the PDF file name
                // instead of its contents
                $attachment->setFile($newFilename);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($attachment);
            $entityManager->flush();

            return $this->redirectToRoute('attachment_index');
        }

        return $this->render('attachment/edit.html.twig', [
            'attachment' => $attachment,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="attachment_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Attachment $attachment): Response
    {
        if ($this->isCsrfTokenValid('delete'.$attachment->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($attachment);
            $entityManager->flush();
        }

        return $this->redirectToRoute('attachment_index');
    }
}
