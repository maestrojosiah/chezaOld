<?php

namespace App\Controller;

use App\Entity\Report;
use App\Form\ReportType;
use App\Repository\ReportRepository;
use App\Repository\SessionRepository;
use App\Repository\TermRepository;
use App\Repository\UserRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\StudentUserDataRepository;
use App\Repository\TeacherUserDataRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Knp\Snappy\Pdf;
use Twig\Environment;
use Knp\Snappy\Image;

/**
 * @Route("/report")
 */
class ReportController extends AbstractController
{
	private $pdf;
	private $img;
	private $twig;
	
	public function __construct(Pdf $pdf, Image $img, Environment $twig)
	{
        $this->pdf = $pdf;		
        $this->img = $img;		
        $this->twig = $twig;		
	}
    /**
     * @Route("/", name="report_index", methods={"GET"})
     */
    public function index(ReportRepository $reportRepository): Response
    {
        return $this->render('report/index.html.twig', [
            'reports' => $reportRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new/{session_id}", name="report_new", methods={"GET","POST"})
     */
    public function new(Request $request, SessionRepository $sessionRepo, TermRepository $termRepo, ReportRepository $reportRepo, $session_id): Response
    {
        $session = $sessionRepo->findOneById($session_id);
        $all_reports = [];
        $sentreports = [];
        $rpts = [];
        $currentTerm = $termRepo->findCurrentTerm(new \Datetime(date('Y-m-d')));
        $reports = $reportRepo->findBy(
            array('teacher' => $session->getTeacher()->getTeacherdata(), /*'term' => $currentTerm,*/ 'student' => $session->getStudent()->getStudentdata()/*, 'uig' => $session->getUserInstrumentGrade()*/),
            array('id' => 'ASC'),
        );
        $reportE = $reportRepo->findOneBy(
            array('teacher' => $session->getTeacher()->getTeacherdata(), 'term' => $currentTerm, 'student' => $session->getStudent()->getStudentdata(), 'uig' => $session->getUserInstrumentGrade()),
            array('id' => 'ASC'),
        );

        foreach ($reports as $report) {
            if(null !== $report->getTerm()){
                $all_reports[$report->getTerm()->getTermnumber().'-'.$report->getId()] = $report;
            } else {
                $all_reports[$currentTerm->getTermNumber().'-'.$report->getId()] = $report;
            }
            
        }
        foreach ($session->getTeacher()->getTeacherdata()->getReports() as $reportentry) {
            if($reportentry->getSent() == true ){
                $sentreports[] = $reportentry->getUig()->getId();
            } else {
                $rpts[] = $reportentry->getUig()->getId();
            }
        }

        return $this->render('report/new.html.twig', [
            'reports' => $all_reports,
            'reportE' => $reportE,
            'rpts' => $rpts,
            'sentreports' => $sentreports,
            'session' => $session,
            'currentTerm' => $currentTerm,
        ]);
    }

    /**
     * @Route("/{id}", name="report_show", methods={"GET"})
     */
    public function show(Report $report, TermRepository $termRepo): Response
    {

        $currentTerm = $termRepo->findCurrentTerm($report->getAddedOn());
        if($report->getUig()->getSiblingname() != null and $report->getUig()->getSiblingname() != ""){
            $fullname = $report->getUig()->getSiblingname();
        } else {
            $fullname = $report->getStudent()->getStudent()->getFullname();
        }

        $arr = [
            'report' => $report,
            'currentTerm' => $currentTerm,
            'fullname' => $fullname,
        ];
        
        return $this->toPdf("pdf/student_report.html.twig", $arr, $fullname."_report");
    }

    /**
     * @Route("/{id}/edit", name="report_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Report $report): Response
    {
        $form = $this->createForm(ReportType::class, $report);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('report_index');
        }

        return $this->render('report/edit.html.twig', [
            'report' => $report,
            'form' => $form->createView(),
        ]);
    }

    // /**
    //  * @Route("/{id}", name="report_delete", methods={"DELETE"})
    //  */
    // public function delete(Request $request, Report $report): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$report->getId(), $request->request->get('_token'))) {
    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->remove($report);
    //         $entityManager->flush();
    //     }

    //     return $this->redirectToRoute('report_index');
    // }

    /**
     * @Route("/teacher/save/student/report", name="save_student_report")
     */
    public function saveStudentReport(Request $request, TermRepository $termRepo, UserInstrumentGradeRepository $uigRepo, UserRepository $userRepo, ReportRepository $reportRepo): Response
    {
        $teacher = $userRepo->findOneById($request->request->get('teacher_id'));
        $student = $userRepo->findOneById($request->request->get('student_id'));
        $uig = $uigRepo->findOneById($request->request->get('uig_id'));
        $term = $termRepo->findOneById($request->request->get('term_id'));
        $rep = $request->request->get('report_id');

        // $report = $reportRepo->findOneBy(
        //     array('teacher' => $teacher->getTeacherdata(), 'student' => $student->getStudentdata(), 'uig' => $uig, 'term' => $term),
        //     array('id' => 'ASC'),
        // );

        // $em = $this->getDoctrine()->getManager();
        // $RAW_QUERY = 'SELECT * FROM report where report.teacher_id = :teacher_id and report.student_id = :student_id and report.uig_id = :uig_id and report.term_id = :term_id and YEAR(report.added_on) = :this_year;';
        
        // $statement = $em->getConnection()->prepare($RAW_QUERY);
        // // Set parameters 
        // $statement->bindValue('teacher_id', $teacher->getTeacherdata()->getId());
        // $statement->bindValue('student_id', $student->getStudentdata()->getId());
        // $statement->bindValue('uig_id', $request->request->get('uig_id'));
        // $statement->bindValue('term_id', $request->request->get('term_id'));
        // $statement->bindValue('this_year', date('Y'));
        // $statement->execute();

        // $report = $statement->fetchAll();
        if($rep != 0){
            $report = $reportRepo->findOneById($rep);
        } else {
            $report = new Report();
        }
        
        
        if(!$report){
            
        }
        $test = [];
        $data = $request->request->all();
        foreach ($data as $field => $d ) {
            $test[] = $field;
            if (strpos($field, '_id') == false) {
                $cmd = "set".$field;
                $report->{$cmd}($d);    
            }
        }
        $report->setTeacher($teacher->getTeacherdata());
        $report->setStudent($student->getStudentdata());
        $report->setAddedOn(new \Datetime(date("Y-m-d")));
        $report->setUig($uig);
        $report->setTerm($term);

        $this->save($report);

        return new JsonResponse($test);
            
        // return new JsonResponse("success");
    }

    public function save($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    public function delete($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($entity);
        $entityManager->flush();
    }

    public function toPdf($path = '', $array = [], $filename): Response
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        //Generate pdf with the retrieved HTML
        return new Response( $this->pdf->getOutputFromHtml($html), 200, array(
            'Content-Type'          => 'application/pdf',
            'Content-Disposition'   => 'inline; filename='.$filename.'.pdf'
        )
        );        
    }

    public function toImage($path = '', $array = [], $filename): Response
    {

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView($path, $array);

        //Generate pdf with the retrieved HTML
        return new Response( $this->img->getOutputFromHtml($html), 200, array(
            'Content-Type'          => 'image/jpg',
            'Content-Disposition'   => 'inline; filename='.$filename.'.jpg'
        )
        );        
    }

}
