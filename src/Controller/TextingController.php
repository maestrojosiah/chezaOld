<?php

namespace App\Controller;

use App\Repository\UserRepository;
use App\Repository\AttachmentRepository;
use App\Repository\GroupRepository;
use App\Entity\Communication;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\SendSms;
use App\Service\Mailer;

class TextingController extends AbstractController
{
    /**
     * @Route("/communication/email/texting/send", name="communicate")
     */
    public function index(SendSms $sendSms, Request $request, Mailer $mailer, UserRepository $userRepo, AttachmentRepository $attachRepo)
    {
        $tosendsms = $request->request->get('tosendsms');
        $tosendemail = $request->request->get('tosendemail');
        $sent_to = $request->request->get('sent_to');
        $subject = $request->request->get('subject');
        $msg = $request->request->get('msg');
        $sent_to_contact = "";
        $sentStatus = "";
        $deliveredStatus = "N/A";
        $error = "";
        $type = "";
        $res = [];
        $reportres = [];
        $messageId = "";
        // instantiate communication
        $communication = new Communication();
        // time now
        $time_now = new \Datetime();
        // set sent at
        $communication->setSentAt($time_now);

        $resp = "";
        $data = "";

        if(null != $request->request->get('phone_to') && $tosendsms == "true"){
            $phone_to = $this->fullNumber($request->request->get('phone_to'));
            $msg = $request->request->get('msg');
            $sent_to_contact .= " " . $phone_to . " ";
            $type .= "SMS";

            if($sent_to == ""){
                $sent_to = $phone_to;
            }
            if($subject == ""){
                $subject = substr($msg, 0, 20);
            }
    
            if ($phone_to !== "" && $msg !== "") {
                
                $msg = trim(strip_tags($msg));
                $msg = str_replace(PHP_EOL, "\\r\\n", $msg);

                $data = "{ \r\n \"from\":\"CONSERV_KCM\",\r\n \"to\":\"$phone_to\",\r\n \"text\":\"$msg\"\r\n}";
    
                try {
                    $response = $sendSms->CallAPI('POST', 'http://54.247.191.102/restapi/sms/1/text/single', $data );
                    // $response = "{\"messages\":[{\"to\":\"254740516282\",\"status\":{\"groupId\":1,\"groupName\":\"PENDING\",\"id\":7,\"name\":\"PENDING_ENROUTE\",\"description\":\"Message sent to next instance\"},\"messageId\":\"31967500447603536271\",\"smsCount\":1}]}";
                    $res[] = $response;
                    $res = $this->jsonToAssocArray($response);
                    $status = $this->getMessagesVar($res, 'groupName', 3);
                    $messageId = $this->getMessagesVar($res, 'messageId', 2);
                    $sentStatus .= " SMS_$status";
                    $resp .= "SMS sent";
                } catch (HttpException $ex) {
                    $sentStatus .= ' SMS_NOT_SENT';
                    echo $ex;
                }


                try {
                    $reportresponse = $sendSms->CallAPI('GET', 'http://54.247.191.102/restapi/sms/1/reports', $data );
                    // $reportresponse = "{\"results\":[{\"messageId\":\"31958905461501630974\",\"to\":\"254740516282\",\"from\":\"CONSERV_KCM\",\"sentAt\":\"2021-04-28T05:50:54.835+0000\",\"doneAt\":\"2021-04-28T05:51:05.097+0000\",\"smsCount\":1,\"mccMnc\":\"null\",\"price\":{\"pricePerMessage\":0.5,\"currency\":\"KES\"},\"status\":{\"groupId\":3,\"groupName\":\"DELIVERED\",\"id\":5,\"name\":\"DELIVERED_TO_HANDSET\",\"description\":\"Message delivered to handset\"},\"error\":{\"groupId\":0,\"groupName\":\"OK\",\"id\":0,\"name\":\"NO_ERROR\",\"description\":\"No Error\",\"permanent\":false}}]}";
                    $reportres[] = $reportresponse;
                    $reportres = $this->jsonToAssocArray($reportresponse);
                    $deliveredStatus = " SMS_" . $this->getResponsesVar($reportres, 'groupName', 3, 'status');
                    $error = " SMS_ERR_" . $this->getResponsesVar($reportres, 'groupName', 3, 'error');               
                } catch (HttpException $ex) {
                    echo $ex;
                }
    
            } 
    
        } 
        
        if(null != $request->request->get('mail_to') && $tosendemail == "true"){

            $mail_to = $request->request->get('mail_to');
            $attachment = $request->request->get('attachment');
            $msg = $request->request->get('msg');
            $subject = $request->request->get('subject');
            $user = $userRepo->findOneByEmail($mail_to);
            $maildata = ['msg' => $msg, 'user' => $user];
            if($attachment == ""){
                if($mailer->sendEmailMessage($maildata, $mail_to, $subject, "communication.html.twig", "communication", "personal_email_from_office")){
                    $sentStatus .= ' EMAIL_SENT';
                } else {
                    $sentStatus .= ' EMAIL_NOT_SENT';
                }    
            } else {
                $attch = $attachRepo->findOneById($attachment);
                if($mailer->sendEmailWithLinkedAttachment($maildata, $mail_to, $subject, "communication.html.twig", "communication", "personal_email_from_office", $this->getParameter('attachment_files').$attch->getFile(), $attch->getFilename())){
                    $sentStatus .= ' EMAIL_SeNT';
                } else {
                    $sentStatus .= ' EMAIL_NOT_SeNT';
                }    
            }
            $resp .= ", $sentStatus";

            if($sent_to == ""){
                $sent_to = $mail_to;
            }
            $type .= "MAIL";
            $sent_to_contact .= " " . $mail_to . " ";
        }

        $communication->setSentTo($sent_to);
        $communication->setSendToContact($sent_to_contact);
        $communication->setMessage($msg);
        $communication->setType($type);
        $communication->setSubject($subject);
        $communication->setSentStatus($sentStatus);
        $communication->setDeliveredStatus($deliveredStatus);
        $communication->setMessageId($messageId);
        $communication->setOpenedStatus('N/A');
        $communication->setDeliveredAt(new \Datetime('1970/01/01'));
        $communication->setAdmin($this->getUser());

        $this->save($communication);

        return new JsonResponse($resp);
    }

    /**
     * @Route("/communication/quick/message/send", name="quickmessage")
     */
    public function quickSend(SendSms $sendSms, Request $request)
    {
        $tosendsms = true;
        $sent_to = $request->request->get('phone_to');
        $name = $request->request->get('name');
        $phone_to = $request->request->get('phone_to');
        $subject = $request->request->get('subject');
        $msg = $request->request->get('msg');
        $sent_to_contact = "";
        $sentStatus = "";
        $deliveredStatus = "N/A";
        $error = "";
        $type = "";
        $res = [];
        $reportres = [];
        $messageId = "";

        // instantiate communication
        $communication = new Communication();
        // time now
        $time_now = new \Datetime();
        // set sent at
        $communication->setSentAt($time_now);

        $resp = "";
        $data = "";
        $contacts = $this->splitPhoneNumbers($phone_to);

        if(null != $request->request->get('phone_to') && $tosendsms == true){
            $msg = $request->request->get('msg');
            $sent_to_contact .= " " . $phone_to . " ";
            $type .= "SMS";

            if($sent_to == ""){
                $sent_to = $phone_to;
            }
            if($subject == ""){
                $subject = substr($msg, 0, 20);
            }
    
            if ($phone_to !== "" && $msg !== "") {
                
                $msg = trim(strip_tags($msg));
                $msg = str_replace(PHP_EOL, "\\r\\n", $msg);

                $data = "{ \r\n \"from\":\"CONSERV_KCM\",\r\n \"to\":$contacts,\r\n \"text\":\"$msg\"\r\n}";
                
                try {
                    $response = $sendSms->CallAPI('POST', 'http://54.247.191.102/restapi/sms/1/text/single', $data );
                    // $this->writeToFile($response, 'ajax');               
                    // $response = "{\"bulkId\":\"32098046480501631751\",\"messages\":[{\"to\":\"254702825682\",\"status\":{\"groupId\":1,\"groupName\":\"PENDING\",\"id\":7,\"name\":\"PENDING_ENROUTE\",\"description\":\"Message sent to next instance\"},\"messageId\":\"32098046480601631753\",\"smsCount\":1},{\"to\":\"254705285959\",\"status\":{\"groupId\":1,\"groupName\":\"PENDING\",\"id\":7,\"name\":\"PENDING_ENROUTE\",\"description\":\"Message sent to next instance\"},\"messageId\":\"32098046480501631752\",\"smsCount\":1}]}";
                    $res[] = $response;
                    $res = $this->jsonToAssocArray($response);
                    $status = $this->getMessagesVar($res, 'groupName', 3);
                    $messageId = $this->getMessagesVar($res, 'messageId', 2);
                    $bulkId = $this->getBulkId($res);
                    $sentStatus .= " SMS_$status";
                    $resp .= "SMS sent";
                } catch (HttpException $ex) {
                    $sentStatus .= ' SMS_NOT_SENT';
                    echo $ex;
                }
    
            } 
    
        } 
        
        $trimmed = preg_replace("/\s+/", "", $phone_to);
        $arrOfNum = explode(',', $trimmed);
        $numOfNumbers = count($arrOfNum);
        $deliveredStatus = "";
        if($numOfNumbers > 1){
            $deliveredStatus = 'MULTIPLE';
        } else {
            try {
                $reportresponse = $sendSms->CallAPI('GET', 'http://54.247.191.102/restapi/sms/1/reports', $data );
                // $reportresponse = "{\"results\":[{\"messageId\":\"31958905461501630974\",\"to\":\"254740516282\",\"from\":\"CONSERV_KCM\",\"sentAt\":\"2021-04-28T05:50:54.835+0000\",\"doneAt\":\"2021-04-28T05:51:05.097+0000\",\"smsCount\":1,\"mccMnc\":\"null\",\"price\":{\"pricePerMessage\":0.5,\"currency\":\"KES\"},\"status\":{\"groupId\":3,\"groupName\":\"DELIVERED\",\"id\":5,\"name\":\"DELIVERED_TO_HANDSET\",\"description\":\"Message delivered to handset\"},\"error\":{\"groupId\":0,\"groupName\":\"OK\",\"id\":0,\"name\":\"NO_ERROR\",\"description\":\"No Error\",\"permanent\":false}}]}";
                $reportres[] = $reportresponse;
                $reportres = $this->jsonToAssocArray($reportresponse);
                $deliveredStatus = " SMS_" . $this->getResponsesVar($reportres, 'groupName', 3, 'status');
                $error = " SMS_ERR_" . $this->getResponsesVar($reportres, 'groupName', 3, 'error');               
            } catch (HttpException $ex) {
                echo $ex;
            } 
        }

        $communication->setSentTo($name);
        $communication->setSendToContact($sent_to_contact);
        $communication->setMessage($msg);
        $communication->setType($type);
        $communication->setSubject($subject);
        $communication->setSentStatus($sentStatus);
        $communication->setDeliveredStatus($deliveredStatus);
        $communication->setMessageId($bulkId);
        $communication->setOpenedStatus('N/A');
        $communication->setDeliveredAt(new \Datetime('1970/01/01'));
        $communication->setAdmin($this->getUser());

        $this->save($communication);

        return new JsonResponse($resp);
    }

    /**
     * @Route("/communication/bulk/message/send/{group_id}", name="bulkmessage")
     */
    public function bulkSend(GroupRepository $groupRepo, SendSms $sendSms, Request $request, $group_id)
    {
        $group = $groupRepo->findOneById($group_id);
        $contacts = $group->getContacts();
        $contactsStr = "";
        foreach ($contacts as $c) {
            $contactsStr .= $c->getNumber().",";
        }
        $sent_to = substr($contactsStr, 0, -1);
        $tosendsms = true;
        $name = $group->getTitle();
        $phone_to = substr($contactsStr, 0, -1);
        $subject = "Bulk message to " . $name;
        $msg = $request->request->get('msg');
        $sent_to_contact = "";
        $sentStatus = "";
        $deliveredStatus = "N/A";
        $error = "";
        $type = "";
        $res = [];
        $reportres = [];
        $messageId = "";

        // instantiate communication
        $communication = new Communication();
        // time now
        $time_now = new \Datetime();
        // set sent at
        $communication->setSentAt($time_now);

        $resp = "";
        $data = "";
        $contacts = $this->splitPhoneNumbers($phone_to);

        if(null != $sent_to && $tosendsms == true){
            $msg = $request->request->get('msg');
            $sent_to_contact .= " " . $phone_to . " ";
            $type .= "SMS";

            if($sent_to == ""){
                $sent_to = $phone_to;
            }
            if($subject == ""){
                $subject = substr($msg, 0, 20);
            }
    
            if ($phone_to !== "" && $msg !== "") {
                
                $msg = trim(strip_tags($msg));
                $msg = str_replace(PHP_EOL, "\\r\\n", $msg);

                $data = "{ \r\n \"from\":\"CONSERV_KCM\",\r\n \"to\":$contacts,\r\n \"text\":\"$msg\"\r\n}";
                
                try {
                    $response = $sendSms->CallAPI('POST', 'http://54.247.191.102/restapi/sms/1/text/single', $data );
                    // $this->writeToFile($response, 'ajax');               
                    // $response = "{\"bulkId\":\"32098046480501631751\",\"messages\":[{\"to\":\"254702825682\",\"status\":{\"groupId\":1,\"groupName\":\"PENDING\",\"id\":7,\"name\":\"PENDING_ENROUTE\",\"description\":\"Message sent to next instance\"},\"messageId\":\"32098046480601631753\",\"smsCount\":1},{\"to\":\"254705285959\",\"status\":{\"groupId\":1,\"groupName\":\"PENDING\",\"id\":7,\"name\":\"PENDING_ENROUTE\",\"description\":\"Message sent to next instance\"},\"messageId\":\"32098046480501631752\",\"smsCount\":1}]}";
                    $res[] = $response;
                    $res = $this->jsonToAssocArray($response);
                    $status = $this->getMessagesVar($res, 'groupName', 3);
                    $messageId = $this->getMessagesVar($res, 'messageId', 2);
                    $bulkId = $this->getBulkId($res);
                    $sentStatus .= " SMS_$status";
                    $resp .= "SMS sent";
                } catch (HttpException $ex) {
                    $sentStatus .= ' SMS_NOT_SENT';
                    echo $ex;
                }
    
            } 
    
        } 
        
        $trimmed = preg_replace("/\s+/", "", $phone_to);
        $arrOfNum = explode(',', $trimmed);
        $numOfNumbers = count($arrOfNum);
        $deliveredStatus = "";
        if($numOfNumbers > 1){
            $deliveredStatus = 'MULTIPLE';
        } else {
            try {
                $reportresponse = $sendSms->CallAPI('GET', 'http://54.247.191.102/restapi/sms/1/reports', $data );
                // $reportresponse = "{\"results\":[{\"messageId\":\"31958905461501630974\",\"to\":\"254740516282\",\"from\":\"CONSERV_KCM\",\"sentAt\":\"2021-04-28T05:50:54.835+0000\",\"doneAt\":\"2021-04-28T05:51:05.097+0000\",\"smsCount\":1,\"mccMnc\":\"null\",\"price\":{\"pricePerMessage\":0.5,\"currency\":\"KES\"},\"status\":{\"groupId\":3,\"groupName\":\"DELIVERED\",\"id\":5,\"name\":\"DELIVERED_TO_HANDSET\",\"description\":\"Message delivered to handset\"},\"error\":{\"groupId\":0,\"groupName\":\"OK\",\"id\":0,\"name\":\"NO_ERROR\",\"description\":\"No Error\",\"permanent\":false}}]}";
                $reportres[] = $reportresponse;
                $reportres = $this->jsonToAssocArray($reportresponse);
                $deliveredStatus = " SMS_" . $this->getResponsesVar($reportres, 'groupName', 3, 'status');
                $error = " SMS_ERR_" . $this->getResponsesVar($reportres, 'groupName', 3, 'error');               
            } catch (HttpException $ex) {
                echo $ex;
            } 
        }

        $communication->setSentTo($name);
        $communication->setSendToContact($sent_to_contact);
        $communication->setMessage($msg);
        $communication->setType($type);
        $communication->setSubject($subject);
        $communication->setSentStatus($sentStatus);
        $communication->setDeliveredStatus($deliveredStatus);
        $communication->setMessageId($bulkId);
        $communication->setOpenedStatus('N/A');
        $communication->setDeliveredAt(new \Datetime('1970/01/01'));
        $communication->setAdmin($this->getUser());

        $this->save($communication);

        return new JsonResponse($resp);
    }

    /**
     * @Route("/communication/get/message/log/{para}/{id}", name="getlogs")
     */
    public function getLogs(SendSms $sendSms, Request $request, $para, $id){

        try {
            $reportresponse = $sendSms->callAPI('GET', "http://54.247.191.102/restapi/sms/1/logs?from=CONSERV_KCM&$para=$id");
            // $reportresponse = $sendSms->CallAPI('GET', 'http://54.247.191.102/restapi/sms/1/reports', $data );
            // $reportresponse = "{\"results\":[{\"messageId\":\"31958905461501630974\",\"to\":\"254740516282\",\"from\":\"CONSERV_KCM\",\"sentAt\":\"2021-04-28T05:50:54.835+0000\",\"doneAt\":\"2021-04-28T05:51:05.097+0000\",\"smsCount\":1,\"mccMnc\":\"null\",\"price\":{\"pricePerMessage\":0.5,\"currency\":\"KES\"},\"status\":{\"groupId\":3,\"groupName\":\"DELIVERED\",\"id\":5,\"name\":\"DELIVERED_TO_HANDSET\",\"description\":\"Message delivered to handset\"},\"error\":{\"groupId\":0,\"groupName\":\"OK\",\"id\":0,\"name\":\"NO_ERROR\",\"description\":\"No Error\",\"permanent\":false}}]}";
            $reportres[] = $reportresponse;
            $reportres = $this->jsonToAssocArray($reportresponse);
            $deliveredStatus = " SMS_" . $this->getResponsesVar($reportres, 'groupName', 3, 'status');
            $error = " SMS_ERR_" . $this->getResponsesVar($reportres, 'groupName', 3, 'error');               
        } catch (HttpException $ex) {
            echo $ex;
        }

        $table = $this->arrayToTable($reportres);
        return new JsonResponse($table);

    }

    public function arrayToTable($data)
    {
        $rows = array();
        foreach ($data as $row) {
            $cells = array();
            foreach ($row as $key => $cell) {

                $cells[] = "<tr><td>" . $cell['to'] . "</td>";
                $cells[] = "<td>" . $cell['status']['name'] . "</td>";
                $cells[] = "<td>" . $cell['error']['name'] . "</td></tr>";
                // $rows[$key] = $cell;
            }
            $rows[] = implode('', $cells);
        }
        return "<table class='table'>" . implode('', $rows) . "</table>";
        // return $rows;
    }

    public function writeToFile($data, $filename){
        $proj_dir = $this->getParameter('proj_dir');
        $file = $proj_dir.'/'. $filename . '.txt';

        if (!file_exists($file)) {
            touch($file);
        }
        // Open the file to get existing content
        $current = file_get_contents($file);
        // Append a new person to the file
        $current .= "\r\n".$data;
        // Write the contents back to the file
        return file_put_contents($file, $current);

    }
    public function jsonToAssocArray($jsonstring){
        $array = [];
        $assocArray = json_decode($jsonstring, true);
        foreach ($assocArray as $key => $value) {
            $array[$key] = $value;
        }
        return $array;
    }

    public function save($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    /**
     * @Route("/texting/send/sms/{to}/{msg}", name="texting")
     */
    public function sendSMSLink(SendSms $sendSms, Request $request, $to, $msg)
    {
    
        if ($to !== "" && $msg !== "") {

            $phone_to = $this->fullNumber($request->request->get('phone_to'));
            $res = [];
            $data = "{\n \"from\":\"ModuleZilla\",\n \"to\":\"$phone_to\",\n \"text\":\"$msg\"\n}";

            try {
                $response = $sendSms->CallAPI('POST', 'http://54.247.191.102/restapi/sms/1/text/single', $data );
                $res = $response;
            } catch (HttpException $ex) {
                echo $ex;
            }

            return new JsonResponse($msg);

        }
    
    }

    public function fullNumber($num){
    	$number = "";
        if(strlen($num) == 10){
            $number = preg_replace('/^0/', '+254', $num);
        } elseif(strlen($num) == 13) {
            $number = $num; 
        } else {
            $number = $num;
        }
        return $number;
    }

    /**
     * @Route("/send/otp/to/number", name="sendotp")
     */
    public function sendOtp(SendSms $sendSms, Request $request)
    {

        $phonenumber = $request->request->get('phonenumber');
        $phone_to = "";
        $msg = "test";
        if ($phonenumber !== "") {
            $phone_to = $this->fullNumber($phonenumber);
            $res = [];
            $data = "{\n \"from\":\"CONSERV_KCM\",\n \"to\":\"$phone_to\",\n \"text\":\"$msg\"\n}";

            try {
                $response = $sendSms->CallAPI('POST', 'http://54.247.191.102/restapi/sms/1/text/single', $data );
                $res = $response;
            } catch (HttpException $ex) {
                echo $ex;
            }

            return new JsonResponse($res);

        }
    

        return new JsonResponse($res);
    }

    public function callBack() {
               
        if($json = json_decode(file_get_contents("php://input"), true)) {
            $CheckoutRequestID = $this->getVar($json, 'CheckoutRequestID', 2);
            $entityManager = $this->getDoctrine()->getManager();
            $callback = new Callback();
            $callback->setCallbackmetadata($json);
            $callback->setCheckoutRequestID($CheckoutRequestID);
            $entityManager->persist($callback);
            $entityManager->flush();        
            // $this->followUp($CheckoutRequestID);
        } else {
            $json = $_POST;
            $CheckoutRequestID = $this->getVar($json, 'CheckoutRequestID', 2);
            $entityManager = $this->getDoctrine()->getManager();
            $callback = new Callback();
            $callback->setCallbackmetadata($json);
            $callback->setCheckoutRequestID($CheckoutRequestID);
            $entityManager->persist($callback);
            $entityManager->flush();        
            // $this->followUp($CheckoutRequestID);
        }
    }

    function getBulkId($hay){
        if(isset($hay['bulkId'])){
            $bulkId = $hay['bulkId'];
        } else {
            $bulkId = $this->getMessagesVar($hay, 'messageId', 2);
        }
        
        return $bulkId;
    }
    
    function getMessagesVar($hay, $needle, $level){

        // body and callback
        $body = $hay['messages'];
        $callback = $body[0];

        // depending on array level provided to 3rd parameter,
        // give back the carrier which contains the value
        // given as the second parameter
        if($level == 0){
            $carrier = $body;
        }
        if($level == 1){
            $carrier = $callback;
        }
        if($level == 2){
            $carrier = $callback[$needle];
        }
        if($level == 3){
            $carrier = $callback['status'][$needle];
        }

        return $carrier;
    }

    function getResponsesVar($hay, $needle, $level, $category=""){

        // body and callback
        $body = $hay['results'];
        $callback = $body[0];

        // depending on array level provided to 3rd parameter,
        // give back the carrier which contains the value
        // given as the second parameter
        if($level == 0){
            $carrier = $body;
        }
        if($level == 1){
            $carrier = $callback;
        }
        if($level == 2){
            $carrier = $callback[$needle];
        }
        if($level == 3){
            $carrier = $callback[$category][$needle];
        }

        return $carrier;
    }

    function splitPhoneNumbers($str){
        $trimmed = preg_replace("/\s+/", "", $str);
        $arrOfNum = explode(',', $trimmed);
        $len = count($arrOfNum);
        $i = 0;
        $numStr = "[";
        foreach ($arrOfNum as $num ) {
            $fullNum = $this->fullNumber($num);
            $numStr .= '"'.$fullNum.'"';
            if ($i != $len - 1) {
                $numStr .= ',';
            }       
            $i++; 
        }
        $numStr .= "]";
        return $numStr;
    }
    
}
