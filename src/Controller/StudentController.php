<?php

namespace App\Controller;

use App\Entity\InstrumentGrade;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\StudentUserDataRepository;
use App\Repository\InstrumentRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\SessionRepository;
use App\Repository\TermRepository;
use App\Repository\ReportRepository;
use App\Repository\UserRepository;
use App\Repository\UserInstrumentGradeRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class StudentController extends AbstractController
{
    /**
     * @Route("/student/calendar", name="student_session_calendar", methods={"GET"})
     */
    public function calendar(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');
        return $this->render('student/calendar.html.twig',[
            'student' => $this->getUser(),
        ]);
    }
    
    /**
     * @Route("/student/attendance", name="student_attendance", methods={"GET"})
     */
    public function attendance(UserInstrumentGradeRepository $userInstrumentGradeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');
        
        $em = $this->getDoctrine()->getManager();

        $RAW_QUERY = 'SELECT DISTINCT user_instrument_grade_id FROM session where session.student_id = :thisstudent;';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters 
        $statement->bindValue('thisstudent', $this->getUser()->getId());
        $statement->execute();
        $userInstrumentGrades = [];

        $userInstrumentGradesIds = $statement->fetchAll();

        foreach ($userInstrumentGradesIds as $uigId ) {
            $userInstrumentGrades[] = $userInstrumentGradeRepo->findOneById($uigId);
        }
        $nonEmptyUigs = [];

        $inv = 0;
        foreach ($userInstrumentGrades as $uig ) {
            $uigForInvoice = [];
            foreach ($uig->getSessions() as $session ) {
                if($session->getDone() == true && $session->getInvoiced() == false){
                    $inv += ($session->getPackage()->getPrice()/12) * 0.700;
                    $uigForInvoice[] = $uig->getId() . '-' . $session->getPackage()->getId();
                }
            }
            if(!empty($uigForInvoice)){
                $nonEmptyUigs[$uig->getId()] = $uigForInvoice;
            }
        }

        return $this->render('student/attendance.html.twig', [
            'teacher' => $this->getUser(),
            'userInstrumentGrades' => $userInstrumentGrades,
            'nonEmptyUigs' => $nonEmptyUigs,
            'inv' => $inv,
        ]);
    }

    /**
     * @Route("/student", name="student")
     */
    public function index(InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');
        
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $data = [];
        $instruments = $instrumentRepo->findBy(
            array('category' => 'instrument'),
            array('id' => 'ASC')
        );
        $courses = $instrumentRepo->findBy(
            array('category' => 'course'),
            array('id' => 'ASC')
        );
        $traditional = $instrumentRepo->findBy(
            array('category' => 'traditional'),
            array('id' => 'ASC')
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        $instrumentGrades = $instrumentGradeRepo->findAll();
        $data['instrumentGrades'] = $instrumentGrades;

        return $this->render('student/profile.html.twig', [
            'student' => $this->getUser(),
            'data' => $data,
        ]);
    }
    /**
     * @Route("/student/dashboard", name="student-dashboard")
     */
    public function dashboard(UserInstrumentGradeRepository $uigRepo, SessionRepository $sessionRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');
        $studentData = $this->getUser()->getStudentdata();
        $student = $this->getUser();
        $payments = $student->getPayments();

        $mysessions = $sessionRepo->findBy(
            array('student' => $this->getUser()),
            array('id' => 'ASC')
        );
        $uigsForThisStudent = $uigRepo->findBy(
            array('studentUserData' => $studentData),
            array('id' => 'DESC')
        );
        return $this->render('student/dashboard.html.twig', [
            'uigs' => $uigsForThisStudent,
            'payments' => $payments,
            'mysessions' => $mysessions,
        ]);
    }

    /**
     * @Route("/report/view/{session_id}", name="report_view", methods={"GET","POST"})
     */
    public function report(UserInstrumentGradeRepository $uigRepo, SessionRepository $sessionRepo, TermRepository $termRepo, ReportRepository $reportRepo, $session_id): Response
    {
        $session = $sessionRepo->findOneById($session_id);
        $all_reports = [];
        $sentreports = [];
        $rpts = [];
        $currentTerm = $termRepo->findCurrentTerm(new \Datetime(date('Y-m-d')));
        $reports = $reportRepo->findBy(
            array('teacher' => $session->getTeacher()->getTeacherdata(), /*'term' => $currentTerm,*/ 'student' => $session->getStudent()->getStudentdata()/*, 'uig' => $session->getUserInstrumentGrade()*/),
            array('id' => 'ASC'),
        );
        $reportE = $reportRepo->findOneBy(
            array('teacher' => $session->getTeacher()->getTeacherdata(), 'term' => $currentTerm, 'student' => $session->getStudent()->getStudentdata(), 'uig' => $session->getUserInstrumentGrade()),
            array('id' => 'ASC'),
        );

        foreach ($reports as $report) {
            $all_reports[$report->getTerm()->getTermnumber().'-'.$report->getId()] = $report;
        }
        foreach ($session->getTeacher()->getTeacherdata()->getReports() as $reportentry) {
            if($reportentry->getSent() == true ){
                $sentreports[] = $reportentry->getUig()->getId();
            } else {
                $rpts[] = $reportentry->getUig()->getId();
            }
        }

        return $this->render('report/report_view.html.twig', [
            'reports' => $all_reports,
            'reportE' => $reportE,
            'rpts' => $rpts,
            'sentreports' => $sentreports,
            'session' => $session,
            'currentTerm' => $currentTerm,
        ]);

    }

    /**
     * @Route("/student/report/list", name="student-report-list")
     */
    public function studentsReportList(SessionRepository $sessionRepo, UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $student = $this->getUser();
        $sessionsuniq = [];

        $sessions = $sessionRepo->findBy(
            array('student' => $student),
            array('id' => 'ASC')
        );

        foreach ($sessions as $key => $sess) {
            $sessionsuniq[$sess->getUserInstrumentGrade()->getId()] = $sess;
        }

        return $this->render('student/reportlist.html.twig', [
            'controller_name' => 'StudentController',
            'sessions' => $sessionsuniq,
        ]);
    }

    /**
     * @Route("/student/test", name="student-test")
     */
    public function test(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');
        return $this->render('student/test.html.twig', [
            'controller_name' => 'StudentController',
        ]);
    }

    /**
     * @Route("/student/uigs", name="student-uigs")
     */
    public function showUigs(UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');
        $studentData = $this->getUser()->getStudentdata();
        $uigsForThisStudent = $uigRepo->findBy(
            array('studentUserData' => $studentData),
            array('id' => 'DESC')
        );
        return $this->render('student/student_uigs.html.twig', [
            'uigs' => $uigsForThisStudent,
        ]);
    }

    /**
     * @Route("/student/payments/list/{uig_id}", name="student-payments-list")
     */
    public function paymentsList(UserInstrumentGradeRepository $uigRepo, $uig_id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_STUDENT');

        $uig = $uigRepo->findOneById($uig_id);
        $student = $this->getUser();
        $payments = $student->getPayments();
        return $this->render('student/payments.html.twig', [
            'student' => $student,
            'uig' => $uig,
            'payments' => $payments,
        ]);
    }

    /**
     * @Route("/student/delete/one/{student_id}", name="student_delete", methods={"GET"})
     */
    public function deleteStudent(Request $request, UserRepository $userRepo, $student_id): Response
    {
        $student = $userRepo->findOneById($student_id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($student);
        $entityManager->flush();

        $this->addFlash(
            'success',
            $student->getFullname() . " has been deleted from the database.",
        );

        return $this->redirectToRoute('assign_access_roles', ['_fragment' => 'students-list']);
    }


}
