<?php

namespace App\Controller;

use App\Entity\Upload;
use App\Entity\Contact;
use App\Form\UploadType;
use App\Repository\UploadRepository;
use App\Repository\GroupRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * @Route("/upload")
 */
class UploadController extends AbstractController
{
    /**
     * @Route("/", name="upload_index", methods={"GET"})
     */
    public function index(UploadRepository $uploadRepository): Response
    {
        return $this->render('upload/index.html.twig', [
            'uploads' => $uploadRepository->findAll(),
        ]);
    }
    
    /**
     * @Route("/new/excel/file/{group_id}", name="app_upload_new")
     */
    public function new(Request $request, GroupRepository $groupRepo, SluggerInterface $slugger, $group_id)
    {
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);
        $form->handleRequest($request);
        $entityManager = $this->getDoctrine()->getManager();

        $group = $groupRepo->findOneById($group_id);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $excelFile */
            $excelFile = $form->get('file')->getData();

            // this condition is needed because the 'excel' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($excelFile) {
                $originalFilename = pathinfo($excelFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$excelFile->guessExtension();

                // Move the file to the directory where excels are stored
                try {
                    $excelFile->move(
                        $this->getParameter('excel_files'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'excelFilename' property to store the PDF file name
                // instead of its contents
                $upload->setFile($newFilename);
                $upload->setUploadgroup($group);
            }

            $rows = $this->readThrough($newFilename);
            $data['rows'] = $rows;
            // echo "<pre>";
            // var_dump($rows);
            // die();
            if($this->addToDatabase($rows, $group)){
                $this->save($upload);
                return $this->redirectToRoute('contact_index', ['group_id' => $group->getId()]);
            } else {
                $this->addFlash('error', "There is nothing in that excel file. Please add contacts with column A: name and column B: phone number");
                return $this->redirectToRoute('admin-communications');
            }

            return $this->redirectToRoute('admin-communications');
        }

        return $this->render('upload/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="upload_show", methods={"GET"})
     */
    public function show(Upload $upload): Response
    {
        return $this->render('upload/show.html.twig', [
            'upload' => $upload,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="upload_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Upload $upload): Response
    {
        $form = $this->createForm(UploadType::class, $upload);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('upload_index');
        }

        return $this->render('upload/edit.html.twig', [
            'upload' => $upload,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="upload_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Upload $upload): Response
    {
        if ($this->isCsrfTokenValid('delete'.$upload->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($upload);
            $entityManager->flush();
        }

        return $this->redirectToRoute('upload_index');
    }

    private function readThrough($filename){

        set_time_limit(0);
        $excelfile = $this->getParameter('excel_files').$filename;
        $spreadsheet = IOFactory::load($excelfile);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $test = [];
        $rows = [];

        foreach($sheetData as $i=>$row) { 
            $test[] = $row;

		    $col = [];
		    foreach ($row as $cell) {
		    	if($cell != NULL && $cell != ""){
		    		$col[] = $cell;
		    	}
		    	
		    }

		    $rows[] = $col;
    
        }
		return $rows;

    }

    private function addToDatabase($rows, $group_id){
    	$group = $this->em()->getRepository('App:Group')->find($group_id);
    	if(isset($rows[0][0]) ){
            foreach($rows as $row){
                if(isset($row[1]) && is_numeric($row[1])){
                    $contact = new Contact;	
                    $contact->setName($row[0]);
                        if(strlen($row[1]) < 10 ){
                            $contact->setNumber("0".$row[1]);
                        } else {
                            $contact->setNumber($row[1]);
                        }    
                    $contact->setContactgroup($group);
                    $this->save($contact);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private function em(){
        $em = $this->getDoctrine()->getManager();
        return $em;
    }

    private function save($entity){
        $this->em()->persist($entity);
        $this->em()->flush();        
    } 

}
