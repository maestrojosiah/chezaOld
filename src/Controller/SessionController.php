<?php

namespace App\Controller;

use App\Entity\Session;
use App\Entity\Payment;
use App\Form\SessionType;
use App\Repository\SessionRepository;
use App\Repository\UserRepository;
use App\Repository\PackageRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\UserInstrumentGradeRepository;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Mailer;
use App\Repository\InstrumentRepository;
use App\Repository\PaymentRepository;
use App\Repository\TermRepository;
use Twig\Environment;

/**
 * @Route("/admin/session")
 */
class SessionController extends AbstractController
{

    /**
     * @Route("/calendar", name="adm_session_calendar", methods={"GET"})
     */
    public function calendar(): Response
    {
        return $this->render('session/calendar.html.twig');
    }

    /**
     * @Route("/", name="session_index", methods={"GET"})
     */
    public function index(InstrumentRepository $instrumentRepo, SessionRepository $sessionRepository, UserInstrumentGradeRepository $userInstrumentGradeRepo): Response
    {
        $groupedusers = $userInstrumentGradeRepo->findUIGForStudents();
        $instruments = $instrumentRepo->findBy(
            array('category' => 'instrument'),
            array('id' => 'ASC')
        );
        $courses = $instrumentRepo->findBy(
            array('category' => 'course'),
            array('id' => 'ASC')
        );
        $traditional = $instrumentRepo->findBy(
            array('category' => 'traditional'),
            array('id' => 'ASC')
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        return $this->render('session/index.html.twig', [
            'groupedusers' => $groupedusers,
            'data' => $data,
        ]);
    }

    /**
     * @Route("/new", name="session_new", methods={"GET","POST"})
     */
    public function new(Request $request, TermRepository $termRepo, Mailer $mailer, Environment $twig, PackageRepository $packageRepo, PaymentRepository $paymentRepo, UserInstrumentGradeRepository $userInstrumentGradeRepo, UserRepository $userRepo): Response
    {
        
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $session = new Session();
        $form = $this->createForm(SessionType::class, $session);
        $form->handleRequest($request);


        // get the teacher id from url if available
        // teacher_id=11&date=2021-01-19&time=08:20
        if (isset($_SERVER['QUERY_STRING']) && $_GET){
            $query = $_GET['teacher_id'];
            $teacher = $userRepo->findOneById($query);
        } else {
            $query = "";
            $teacher = null;
        }
        if (isset($_SERVER['QUERY_STRING']) && isset($_GET['date'])) {
            $date = $_GET['date'];
            $starttime = $_GET['time'];
            $firstPart = explode(":", $_GET['time'])[0]+1;
            $padded = sprintf("%02d", $firstPart);
            $endtime = $padded .":". explode(":", $_GET['time'])[1];
        } else {
            $date = "";
            $starttime = "";
            $endtime = "";
        }
        if ($form->isSubmitted() && $form->isValid()) {

            // some declarations to use in the function
            $package = $form->get('package')->getData();
            $userinstrumentgrade = $form->get('userinstrumentgrade')->getData();
            $teacher = $form->get('teacher')->getData();
            $numberofsessions = $form->get('numberofsessions')->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $startingDate = $form->get('startingOn')->getData();
            $paidamt = $form->get('paid')->getData();

            if(isset($_POST['day'])){
                $numberofsessionsperweek = count($_POST['day']);
            }
            
            // get price per lesson
            $package_price_per_term = $package->getPrice();
            $price_per_lesson = $package_price_per_term / $numberofsessions;

            // calculate the number of sessions per week
            // and weeks needed depending on class frequency
            $numberofweeks = $numberofsessions / $numberofsessionsperweek;

            // declare some defaults and more data
            $cleared = false;

            $userinstrumentgrade->setPayment($paidamt);

            $entityManager->persist($userinstrumentgrade);
            $entityManager->flush();

            $student = $userinstrumentgrade->getStudentUserData()->getStudent();

            $countOfInvoices = $paymentRepo->countEntriesWithType("inv");
            $countOfPayments = $paymentRepo->countEntriesWithType("pmt");
            $nextInvoiceNumber = $countOfInvoices + 1;
            $nextReceiptNumber = $countOfPayments + 1;

            $days = $_POST['day'];
            $fees = $package->getPrice();
            $balance = $fees - $paidamt;
    
            $fullfee = $package->getPrice();
            $onelessonfee = $fullfee / 12;
            $realfee = $onelessonfee * $numberofsessions;

            $invoice = new Payment();
            $invoice->setAmount($realfee);
            $invoice->setDoneOn(new \Datetime("now"));
            $invoice->setType("inv");
            $invoice->setDocNumber($nextInvoiceNumber);
            $invoice->setUser($student);
            $invoice->setUig($userinstrumentgrade);
            $invoice->setBalance($realfee);
            $this->save($invoice);

            $newPayment = new Payment();
            $newPayment->setAmount($paidamt);
            $newPayment->setDoneOn(new \Datetime("now"));
            $newPayment->setType("pmt");
            $newPayment->setDocNumber($nextReceiptNumber);
            $newPayment->setUser($student);
            $newPayment->setUig($userinstrumentgrade);
            $newPayment->setBalance($balance);
            $this->save($newPayment);

            $dates = []; 
            $test = []; 
            $daysofweek = [];
            $tosave = [];
            $today = date('Y-m-d H:i:s');
            $td = date('w');
            // $dd = new \Datetime("now");
            $dd = clone $startingDate;
            
            for ($i=0; $i < $numberofweeks; $i++) { 

                foreach ($days as $day ) {
                    if($i == 0){
                        $dd->modify($day. " this week");
                        $beginAt = $form->get('beginAt')->getData()->setDate($dd->format('Y'), $dd->format('m'), $dd->format('d'));
                        $endAt = $form->get('endAt')->getData()->setDate($dd->format('Y'), $dd->format('m'), $dd->format('d'));
                        $daysofweek[] = $dd->format('l');
                        $test[] = $dd->format('d');
                        if($dd->format('w') > $td){
                            $dates[] = $dd->format('l, d-m');
                            $paidamt -= $price_per_lesson; 
                            if($paidamt >= 0) {
                                $cleared = true;
                            } else {
                                $cleared = false;
                            }                            
                            $session = $this->saveSession($teacher, $userinstrumentgrade, $package, $paidamt, $student, $numberofsessions, $dd, $beginAt, $endAt, $cleared);
                            $numberofsessions -= 1;
                            if(count($days) == 1){
                                $dd->modify("$day next week");
                            }
                        } else {
                            $tosave[$dd->format('l, d-m')] = $dd;
                        }
                    } else {
                        $modDd = $dd->modify("this ".$day);
                        $dd->setTime("08", "55", "00");
                        $beginAt = $form->get('beginAt')->getData()->setDate($dd->format('Y'), $dd->format('m'), $dd->format('d'));
                        $endAt = $form->get('endAt')->getData()->setDate($dd->format('Y'), $dd->format('m'), $dd->format('d'));
                        $daysofweek[] = $dd->format('l');
                        // echo "compare today : " . strtotime($today) . " with dd : " . strtotime($dd->format("Y-m-d"));
                        // echo "<br />";
                        if(strtotime($modDd->format('Y-m-d')) > strtotime($today)){
                            // $numberofsessions -= 1;
                            $dates[] = $dd->format('l, d-m');
                            $paidamt -= $price_per_lesson; 
                            if($paidamt >= 0) {
                                $cleared = true;
                            } else {
                                $cleared = false;
                            }                            
                            $session = $this->saveSession($teacher, $userinstrumentgrade, $package, $paidamt, $student, $numberofsessions, $dd, $beginAt, $endAt, $cleared);
                            $numberofsessions -= 1;
                            if(count($days) == 1){
                                $dd->modify("$day next week");
                            }
                        } else {
                            $dates[] = $dd->format('l, d-m');
                            $paidamt -= $price_per_lesson; 
                            if($paidamt >= 0) {
                                $cleared = true;
                            } else {
                                $cleared = false;
                            }                            
                            $session = $this->saveSession($teacher, $userinstrumentgrade, $package, $paidamt, $student, $numberofsessions, $dd, $beginAt, $endAt, $cleared);
                            $numberofsessions -= 1;
                            if(count($days) == 1){
                                $dd->modify("$day next week");
                            }
                        }
    
                    }
                    
                }   
                $dd->modify(end($daysofweek));

            }
            foreach ($tosave as $key => $item ) {
                $item->modify(current($days));
                next($days);
                $dates[] = $item->format('l, d-m');
                $paidamt -= $price_per_lesson; 
                if($paidamt >= 0) {
                    $cleared = true;
                } else {
                    $cleared = false;
                }                            
                $session = $this->saveSession($teacher, $userinstrumentgrade, $package, $paidamt, $student, $numberofsessions, $item, $beginAt, $endAt, $cleared);
                $numberofsessions -= 1;
                if(count($days) == 1){
                    $dd->modify("$day next week");
                }
            }

            $maildata = ['student' => $student, 'teacher' => $teacher, 'userinstrumentgrade' => $userinstrumentgrade, 'startingdate' => $startingDate->format('Y-m-d'), 'beginAt' => $beginAt->format('H:i'), 'endAt' => $endAt->format('H:i')];
            $mailer->sendEmailMessage($maildata, $student->getEmail(), "Meet your teacher", "new_teacher.html.twig", "notifications", "new_teacher_email");    
            // $mailer->sendEmailMessage($maildata, $teacher->getEmail(), "New Student Alert", "new_session.html.twig", "notifications", "new_student_email");    
            // send invoice
            $currentTerm = $termRepo->findCurrentTerm($invoice->getDoneOn());
            $html = $twig->render('pdf/student_invoice.html.twig', [
                'payment' => $invoice,
                'currentTerm' => $currentTerm,
            ]);
    
            $mailer->sendEmailWithAttachment(['student' => $student, 'payment' => $invoice], $student->getEmail(), $student->getFullname().' Your Invoice dated '.$invoice->getDoneOn()->format('d/m/Y'), $html, 'invoice_student.html.twig', $student->getFullname().'-invoice-'.$invoice->getDoneOn()->format('d/m/Y'));
    
            // if ($numberofsessionsperweek > 1) {
            return $this->redirectToRoute('session_show', ['id' => $session->getId()]);
            // } else {
                // return $this->redirectToRoute('user_instrument_grade_show', ['id' => $userinstrumentgrade->getId()]);
            // }
            
        }

        return $this->render('session/new.html.twig', [
            'userInstrumentGradeRepo' => count($userInstrumentGradeRepo->findWithNoSessions()),
            'session' => $session,
            'query' => $query,
            'seldate' => $date,
            'seltime' => $starttime,
            'seletime' => $endtime,
            'teacher' => $teacher,
            'form' => $form->createView(),
        ]);
    }

    public function saveSession($teacher, $userinstrumentgrade, $package, $paidamt, $student, $numberofsessions, $movingdate, $beginAt, $endAt, $cleared){
        $session = new Session();
        $session->setTeacher($teacher);
        $session->setUserInstrumentGrade($userinstrumentgrade);
        $session->setPackage($package);
        $session->setPaid($paidamt);
        $session->setStudent($student);
        $session->setNumberofsessions($numberofsessions);
        // if(isset($_POST['day'])){
        //     $comingday = $_POST['day'][$a];
        // } else {
        //     $comingday = $startingDate->format('l');
        // }
        $beginAt = $beginAt->setDate($movingdate->format('Y'), $movingdate->format('m'), $movingdate->format('d'));
        $endAt = $endAt->setDate($movingdate->format('Y'), $movingdate->format('m'), $movingdate->format('d'));
        $session->setBeginAt($beginAt);
        $session->setEndAt($endAt);
        $session->setStartingOn($movingdate);
        $session->setCleared(false);
        $this->save($session);
        return $session;
    }

    public function save($myEntityClass): void
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($myEntityClass);
        $entityManager->flush();
    }
    
    /**
     * @Route("/{id}", name="session_show", methods={"GET"})
     */
    public function show(Session $session, PackageRepository $packageRepo, InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        // $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $packages = $packageRepo->findAll();
        $s = [];
        $instruments = $instrumentRepo->findBy(
            array('category' => 'instrument'),
            array('id' => 'ASC')
        );
        $courses = $instrumentRepo->findBy(
            array('category' => 'course'),
            array('id' => 'ASC')
        );
        $traditional = $instrumentRepo->findBy(
            array('category' => 'traditional'),
            array('id' => 'ASC')
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;
        $uig = $session->getUserInstrumentGrade();
        $uigsessions = $uig->getSessions();
        $days = [];
        $grades = $instrumentGradeRepo->findAll();

        foreach ($uigsessions as $key => $sess) {
            $beginAt = $sess->getBeginAt();
            $endAt = $sess->getEndAt();
            $days[$beginAt->format('H:i').'-'.$endAt->format('H:i').'-'.$beginAt->format('l')] = $beginAt->format('l');
        }

        return $this->render('session/show.html.twig', [
            'packages' => $packages,
            'session' => $session,
            'data' => $data,
            'grades' => $grades,
            'days' => array_unique($days),
        ]);
    }

    /**
     * @Route("/edit/all/{sessid}", name="session_times", methods={"GET"})
     */
    public function times(SessionRepository $sessionRepo, $sessid): Response
    {
        $session = $sessionRepo->findOneById($sessid);
        $uig = $session->getUserInstrumentGrade();
        $uigsessions = $uig->getSessions();
        $days = [];
        $dates = [];

        foreach ($uigsessions as $key => $sess) {
            $beginAt = $sess->getBeginAt();
            $endAt = $sess->getEndAt();
            $invoiced = $sess->getInvoiced();
            $days[] = $beginAt->format('l');
            $dates[$sess->getId()] = [$beginAt, $endAt, $invoiced];
        }

        return $this->render('session/times.html.twig', [
            'session' => $session,
            'days' => array_unique($days),
            'alldays' => $dates,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="session_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Session $session, PackageRepository $packageRepo, UserInstrumentGradeRepository $userInstrumentGradeRepo, UserRepository $userRepo): Response
    {
        
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $form = $this->createForm(SessionType::class, $session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('session_index');
        }

        return $this->render('session/edit.html.twig', [
            'session' => $session,
            'form' => $form->createView(),
        ]);   

    }

    /**
     * @Route("/{id}", name="session_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Session $session): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete'.$session->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($session);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_instrument_grade_show', ['id' => $session->getUserInstrumentGrade()->getId()]);
    }

    /**
     * @Route("/session/delete/one/{session_id}", name="session_delete_one", methods={"GET"})
     */
    public function deleteOne(Request $request, SessionRepository $sessionRepo, $session_id): Response
    {
        $session = $sessionRepo->findOneById($session_id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($session);
        $entityManager->flush();
        // return $this->redirectToRoute('user_instrument_grade_show');
        return $this->redirectToRoute('user_instrument_grade_show', ['id' => $session->getUserInstrumentGrade()->getId()]);
    }

}
