<?php

namespace App\Controller;

use App\Entity\Settings;
use App\Form\SettingsType;
use App\Repository\SettingsRepository;
use App\Repository\UserSettingsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Mailer;

/**
 * @Route("/settings")
 */
class SettingsController extends AbstractController
{
    /**
     * @Route("/", name="settings_index", methods={"GET"})
     */
    public function index(SettingsRepository $settingsRepository, Mailer $mailer): Response
    {
        $student = $this->getUser();
        // $maildata = ['amount' => 100, 'newbal' => 100, 'student' => $student];
        // $mailer->sendEmailMessage($maildata, 'jshbr7@gmail.com', "Payment Received", "payment_received.html.twig", "general", "new_student_email");    
        return $this->render('settings/index.html.twig', [
            'settings' => $settingsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="settings_new", methods={"GET","POST"})
     */
    public function new(Request $request, SettingsRepository $settingsRepo, UserSettingsRepository $userSettingsRepo): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $setting = new Settings();
        $form = $this->createForm(SettingsType::class, $setting);
        $form->handleRequest($request);
        $existingSettings = [];

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($setting);
            $entityManager->flush();

            return $this->redirectToRoute('settings_index');
        }

        $settings = $settingsRepo->findAll();
        $userSettings = $userSettingsRepo->findBy(
            array('user' => $this->getUser()),
            array('id' => 'ASC'),
        );

        if(!empty($userSettings)){
            foreach ($userSettings as $key => $value) {
                $existingSettings[$value->getSetting()->getId()] = $value;
            }
        } 
        return $this->render('settings/settings.html.twig', [
            'setting' => $setting,
            'settings' => $settings,
            'userSettings' => $existingSettings,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/add", name="settings_add", methods={"GET","POST"})
     */
    public function add(Request $request, SettingsRepository $settingsRepo, UserSettingsRepository $userSettingsRepo): Response
    {
        $setting = new Settings();
        $form = $this->createForm(SettingsType::class, $setting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($setting);
            $entityManager->flush();

            return $this->redirectToRoute('settings_index');
        }

        return $this->render('settings/new.html.twig', [
            'setting' => $setting,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="settings_show", methods={"GET"})
     */
    public function show(Settings $setting): Response
    {
        return $this->render('settings/show.html.twig', [
            'setting' => $setting,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="settings_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Settings $setting): Response
    {
        $form = $this->createForm(SettingsType::class, $setting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('settings_index');
        }

        return $this->render('settings/edit.html.twig', [
            'setting' => $setting,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="settings_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Settings $setting): Response
    {
        if ($this->isCsrfTokenValid('delete'.$setting->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($setting);
            $entityManager->flush();
        }

        return $this->redirectToRoute('settings_index');
    }
}
