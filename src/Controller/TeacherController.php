<?php

namespace App\Controller;

use App\Entity\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\InstrumentRepository;
use App\Repository\InstrumentGradeRepository;
use App\Repository\PackageRepository;
use App\Repository\PaymentRepository;
use App\Repository\SchoolTimeRepository;
use App\Repository\SessionRepository;
use App\Repository\TermRepository;
use App\Repository\UserInstrumentGradeRepository;
use App\Repository\UserRepository;

class TeacherController extends AbstractController
{
    /**
     * @Route("/teacher/calendar", name="teacher_session_calendar", methods={"GET"})
     */
    public function calendar(SchoolTimeRepository $schoolTimeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        $schedules = $schoolTimeRepo->findByTeacher($this->getUser());
        return $this->render('teacher/calendar.html.twig', [
            'schedules' => $schedules,
            'teacher' => $this->getUser(),
        ]);
    }
    
    /**
     * @Route("/teacher/attendance", name="teacher_student_attendance", methods={"GET"})
     */
    public function attendance(UserInstrumentGradeRepository $userInstrumentGradeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        
        $em = $this->getDoctrine()->getManager();

        $RAW_QUERY = 'SELECT DISTINCT user_instrument_grade_id FROM session where session.teacher_id = :thisteacher;';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters 
        $statement->bindValue('thisteacher', $this->getUser()->getId());
        $statement->execute();
        $userInstrumentGrades = [];

        $userInstrumentGradesIds = $statement->fetchAll();

        foreach ($userInstrumentGradesIds as $uigId ) {
            $userInstrumentGrades[] = $userInstrumentGradeRepo->findOneById($uigId);
        }
        $nonEmptyUigs = [];

        $inv = 0;
        foreach ($userInstrumentGrades as $uig ) {
            $uigForInvoice = [];
            foreach ($uig->getSessions() as $session ) {
                if($session->getDone() == true && $session->getInvoiced() == false){
                    $inv += ($session->getPackage()->getPrice()/12) * 0.700;
                    $uigForInvoice[] = $uig->getId() . '-' . $session->getPackage()->getId();
                }
            }
            if(!empty($uigForInvoice)){
                $nonEmptyUigs[$uig->getId()] = $uigForInvoice;
            }
        }

        return $this->render('teacher/attendance.html.twig', [
            'teacher' => $this->getUser(),
            'userInstrumentGrades' => $userInstrumentGrades,
            'nonEmptyUigs' => $nonEmptyUigs,
            'inv' => $inv,
        ]);
    }

    /**
     * @Route("/teacher/attendance/completed", name="teacher_student_attendance_completed", methods={"GET"})
     */
    public function attendanceDone(UserInstrumentGradeRepository $userInstrumentGradeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        
        $em = $this->getDoctrine()->getManager();

        $RAW_QUERY = 'SELECT DISTINCT user_instrument_grade_id FROM session where session.teacher_id = :thisteacher;';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters 
        $statement->bindValue('thisteacher', $this->getUser()->getId());
        $statement->execute();
        $userInstrumentGrades = [];

        $userInstrumentGradesIds = $statement->fetchAll();

        foreach ($userInstrumentGradesIds as $uigId ) {
            $userInstrumentGrades[] = $userInstrumentGradeRepo->findOneById($uigId);
        }
        $nonEmptyUigs = [];

        $inv = 0;
        foreach ($userInstrumentGrades as $uig ) {
            $uigForInvoice = [];
            foreach ($uig->getSessions() as $session ) {
                if($session->getDone() == true && $session->getInvoiced() == false){
                    $inv += ($session->getPackage()->getPrice()/12) * 0.700;
                    $uigForInvoice[] = $uig->getId() . '-' . $session->getPackage()->getId();
                }
            }
            if(!empty($uigForInvoice)){
                $nonEmptyUigs[$uig->getId()] = $uigForInvoice;
            }
        }

        return $this->render('teacher/attendance_done.html.twig', [
            'teacher' => $this->getUser(),
            'userInstrumentGrades' => $userInstrumentGrades,
            'nonEmptyUigs' => $nonEmptyUigs,
            'inv' => $inv,
        ]);
    }

    /**
     * @Route("/teacher/invoice/confirm", name="teacher_send_invoice_confirm", methods={"GET"})
     */
    public function sendInvoiceConfirm(UserInstrumentGradeRepository $userInstrumentGradeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        
        $em = $this->getDoctrine()->getManager();

        $RAW_QUERY = 'SELECT DISTINCT user_instrument_grade_id FROM session where session.teacher_id = :thisteacher;';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        // Set parameters 
        $statement->bindValue('thisteacher', $this->getUser()->getId());
        $statement->execute();
        $userInstrumentGrades = [];
        $uigs = [];

        $userInstrumentGradesIds = $statement->fetchAll();


        foreach ($userInstrumentGradesIds as $uigId ) {
            $userInstrumentGrades[] = $userInstrumentGradeRepo->findOneById($uigId);
        }
        $nonEmptyUigs = [];
        $nuigs = [];

        $inv = 0;
        foreach ($userInstrumentGrades as $uig ) {
            $uigForInvoice = [];
            foreach ($uig->getSessions() as $session ) {
                if($session->getDone() == true && $session->getInvoiced() == false){
                    $inv += ($session->getPackage()->getPrice()/12) * 0.700;
                    $uigForInvoice[] = $uig->getId() . '-' . $session->getPackage()->getId();
                }
            }
            if(!empty($uigForInvoice)){
                $nonEmptyUigs[$uig->getId()] = $uigForInvoice;
                $nuigs[$uig->getId()] = $uig;
            }
        }
        $teacher = $this->getUser();

        $ded = 0;
        foreach ($teacher->getTeacherdata()->getDeductions() as $d) {
            $ded += $d->getToDeduct();
        }

        return $this->render('teacher/send_invoice.html.twig', [
            'teacher' => $teacher,
            'userInstrumentGrades' => $userInstrumentGrades,
            'nonEmptyUigs' => $nonEmptyUigs,
            'inv' => $inv,
            'nuigs' => $nuigs,
            'ded' => $ded,
        ]);
    }
    
    /**
     * @Route("/teacher", name="teacher")
     */
    public function index(InstrumentRepository $instrumentRepo, InstrumentGradeRepository $instrumentGradeRepo): Response
    {
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $data = [];
        $instruments = $instrumentRepo->findBy(
            array('category' => 'instrument'),
            array('id' => 'ASC')
        );
        $courses = $instrumentRepo->findBy(
            array('category' => 'course'),
            array('id' => 'ASC')
        );
        $traditional = $instrumentRepo->findBy(
            array('category' => 'traditional'),
            array('id' => 'ASC')
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        $instrumentGrades = $instrumentGradeRepo->findAll();
        $data['instrumentGrades'] = $instrumentGrades;

        return $this->render('teacher/profile.html.twig', [
            'teacher' => $this->getUser(),
            'data' => $data,
        ]);
    }
    /**
     * @Route("/teacher/dashboard", name="teacher-dashboard")
     */
    public function dashboard(SessionRepository $sessionRepo, UserRepository $userRepo, TermRepository $termRepo, UserInstrumentGradeRepository $userInstrumentGradeRepo): Response
    {
        $students = [];
        $this->denyAccessUnlessGranted('ROLE_TEACHER');
        $em = $this->getDoctrine()->getManager();
        $RAW_QUERY = 'select COUNT(*) from session where teacher_id = ' . $this->getUser()->getId(). ' and invoiced is NULL group by user_instrument_grade_id;';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $today = new \Datetime(date('Y-m-d'));
        $currentTerm = $termRepo->findCurrentTerm($today);
        
        $studentscount = $statement->fetchAll();
        $todayslessons = $sessionRepo->findTodaysLessons($this->getUser());

        $RAW_QUERY2 = 'select student_id from report where teacher_id = ' . $this->getUser()->getTeacherdata()->getId() . ' and term_id = ' . $currentTerm->getId() . ';';
        $statement2 = $em->getConnection()->prepare($RAW_QUERY2);
        $statement2->execute();

        $reports = $statement2->fetchAll();

        $RAW_QUERY3 = 'select student_id from session where teacher_id = '. $this->getUser()->getId() .' and invoiced is NULL group by student_id;';
        $statement3 = $em->getConnection()->prepare($RAW_QUERY3);
        $statement3->execute();

        $student_ids = $statement3->fetchAll();

        $RAW_QUERY4= 'select user_instrument_grade_id from session where invoiced is null and teacher_id = '. $this->getUser()->getId() .' group by user_instrument_grade_id;';
        $statement4 = $em->getConnection()->prepare($RAW_QUERY4);
        $statement4->execute();

        $all_students = $statement4->fetchAll();
        $uigs = [];

        foreach ($all_students as $entry) {
            $uigs[] = $userInstrumentGradeRepo->findOneById($entry);
        }

        foreach ($student_ids as $key => $student_id) {
            $students[] = $userRepo->findOneById($student_id);
        }

        $totalfees = 0;
        $paidfees = 0;
        $schooltimes = [];
        $studenttimes = [];

        foreach ($students as $student ) {
            $totalfees += $student->getStudentsessions()[0]->getPackage()->getPrice();

            $RAW_QUERY4 = 'SELECT SUM(amount) AS totalamount FROM payment where payment.user_id = '. $student->getId() .' and payment.type = "pmt" ;';            
            $statement4 = $em->getConnection()->prepare($RAW_QUERY4);
            $statement4->execute();
            $totalpaid = $statement4->fetchAll();
            $paidfees += $totalpaid[0]['totalamount'];


        }

        foreach ($uigs as $uig) {
            $sess = $uig->getSessions()[0]->getPackage()->getDuration();

            $init = $sess*60;
            $hours = floor($init / 3600);
            $minutes = floor(($init / 60) % 60);
            $seconds = $init % 60;
            if ($hours < 10){$hours = sprintf("%02d", $hours);}
            if ($minutes < 10){$minutes = sprintf("%02d", $minutes);}
            if ($seconds < 10){$seconds = sprintf("%02d", $seconds);}
            
            $studenttimes[] = "$hours:$minutes:$seconds";            
        }
        // calculate total schooltime for this teacher
        foreach ($this->getUser()->getSchoolTimes() as $schedule ) {
            $a = $schedule->getBeginAt();
            $b = $schedule->getEndAt();
            $interval = $b->diff($a);
            
            // echo $interval->format("%H");            
            $schooltimes[] = $interval->format('%H:%I:%S');
        }

        $hour =0;$min=0;$sec=0;
        $schtime = strtotime("00:00:00"); // school times
        foreach($schooltimes as $a){
            $schtime += strtotime($a) - strtotime('00:00:00');
            $time = explode(':',$a);
            $sec +=$time[2];
            $min +=$time[1];
            $hour += $time[0];
        }
        $totalschooltime = (int)$hour + ($min/60);

        $hr =0;$mn=0;$sc=0;
        $stutime = strtotime("00:00:00"); // studenttimes
        foreach($studenttimes as $a){
            $stutime += strtotime($a) - strtotime('00:00:00');
            $time = explode(':',$a);
            $sc +=$time[2];
            $mn +=$time[1];
            $hr += $time[0];
        }
        $totalstudenttime = (int)$hr + ($mn/60);

        $sessions = $sessionRepo->findBy(
            array('invoiced' => null, 'teacher' => $this->getUser()),
            array('id' => 'ASC')
        );
        $donesessions = $sessionRepo->findBy(
            array('invoiced' => null, 'teacher' => $this->getUser(), 'done' => true ),
            array('id' => 'ASC')
        );

        $date1 = new \DateTime(date('Y-m-d H:i:s', $schtime));
        $date2 = new \DateTime(date('Y-m-d H:i:s', $stutime));
        $interval = $date1->diff($date2);

        // echo "difference " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days "; 
        $data = [];
        $teachersessions = $this->getUser()->getTeachersessions();
        $schedules = $this->getUser()->getSchoolTimes();
        $data['all_students'] = $all_students;
        $data['teachersessions'] = $sessions;
        $data['studentscount'] = $studentscount;
        $data['schedules'] = $schedules;
        $data['todayslessonscount'] = count($todayslessons);
        $data['todayslessons'] = $todayslessons;
        $data['reports'] = $reports;
        $data['sessions'] = $sessions;
        $data['donesessions'] = $donesessions;
        $data['donesessions'] = $donesessions;
        $data['students'] = $students;
        $data['totalfees'] = $totalfees;
        $data['paidfees'] = $paidfees;
        $data['schooltimes'] = $totalschooltime;
        $data['studenttime'] = $totalstudenttime;
        $data['currentTerm'] = $currentTerm->getId();
        $data['schooltimess'] = $schooltimes;
        $data['studenttimes'] = $studenttimes;
        $data['interval'] = "difference " . $interval->h . " hours ";
        $data['schtime'] = date('H:i:s', $schtime);
        $data['stutime'] = date('H:i:s', $stutime);
        $data['strtimediff'] = date('H:i:s', $schtime-$stutime-strtotime("00:00:00"));

        return $this->render('teacher/dashboard.html.twig', [
            'data' => $data,
        ]);
    }

    /**
     * @Route("/teacher/students/list", name="teacher-students-list")
     */
    public function studentsList(SessionRepository $sessionRepo, TermRepository $termRepo, UserInstrumentGradeRepository $uigRepo): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $teacher = $this->getUser();
        $sessionsuniq = [];

        // $sessions = $sessionRepo->findBy(
        //     array('teacher' => $teacher),
        //     array('id' => 'ASC')
        // );
        $r = [];
        foreach ($teacher->getTeacherdata()->getReports() as $reportentry) {
            if($reportentry->getSent() == true ){
                $sentreports[] = $reportentry->getUig()->getId();
            } else {
                $reports[] = $reportentry->getUig()->getId();
                $r[$reportentry->getUig()->getId()] = $reportentry;
            }
            
        }

        $currentTerm = $termRepo->findCurrentTermArr(new \Datetime());
        $termStart = $currentTerm[0]->getStartingOn();
        $termEnd = $currentTerm[0]->getEndingOn();
        $sessions = $sessionRepo->findForThisTerm($termStart, $termEnd, $teacher);


        foreach ($sessions as $key => $sess) {
            $term = $termRepo->findCurrentTermArr($sess->getStartingOn());
            $termName = $term[0]->getTermnumber();
            $sessionsuniq[$sess->getUserInstrumentGrade()->getId().'-'.$termName] = $sess;
        }

        return $this->render('teacher/studentslist.html.twig', [
            'sessions' => $sessionsuniq,
            'currentTerm' => $currentTerm,
            'r' => $r,
        ]);
    }

    

}
