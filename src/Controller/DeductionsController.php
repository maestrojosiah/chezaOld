<?php

namespace App\Controller;

use App\Entity\Deductions;
use App\Form\DeductionsType;
use App\Repository\DeductionsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/deductions")
 */
class DeductionsController extends AbstractController
{
    /**
     * @Route("/", name="deductions_index", methods={"GET"})
     */
    public function index(DeductionsRepository $deductionsRepository): Response
    {
        return $this->render('deductions/index.html.twig', [
            'deductions' => $deductionsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="deductions_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $deduction = new Deductions();
        $form = $this->createForm(DeductionsType::class, $deduction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($deduction);
            $entityManager->flush();

            return $this->redirectToRoute('deductions_index');
        }

        return $this->render('deductions/new.html.twig', [
            'deduction' => $deduction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="deductions_show", methods={"GET"})
     */
    public function show(Deductions $deduction): Response
    {
        return $this->render('deductions/show.html.twig', [
            'deduction' => $deduction,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="deductions_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Deductions $deduction): Response
    {
        $form = $this->createForm(DeductionsType::class, $deduction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('deductions_index');
        }

        return $this->render('deductions/edit.html.twig', [
            'deduction' => $deduction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="deductions_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Deductions $deduction): Response
    {
        if ($this->isCsrfTokenValid('delete'.$deduction->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($deduction);
            $entityManager->flush();
        }

        return $this->redirectToRoute('deductions_index');
    }
}
