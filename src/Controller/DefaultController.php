<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\InstrumentRepository;
use App\Repository\PostRepository;
use App\Repository\UserRepository;
use Sonata\SeoBundle\Seo\SeoPageInterface;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(InstrumentRepository $instrumentRepo, PostRepository $postRepo): Response
    {
        $data = [];
        $instruments = $instrumentRepo->findBy(
            array('category' => 'instrument'),
            array('id' => 'ASC')
        );
        $courses = $instrumentRepo->findBy(
            array('category' => 'course'),
            array('id' => 'ASC')
        );
        $traditional = $instrumentRepo->findBy(
            array('category' => 'traditional'),
            array('id' => 'ASC')
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;
        $data['posts'] = $postRepo->findThreeLatestPosts();

  
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'data' => $data,
        ]);
    }

    /**
     * @Route("/security/select", name="select_reg")
     */
    public function select_reg(): Response
    {
        return $this->render('default/select_reg.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/security/welcome/{user_id}", name="welcome_page")
     */
    public function welcome(UserRepository $userRepo, $user_id): Response
    {
        $user = $userRepo->findOneById($user_id);
        return $this->render('default/welcome.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/about/cheza/school", name="about")
     */
    public function about(SeoPageInterface $seoPage): Response
    {
        $seoPage
            ->setTitle('About Cheza Music School')
            ->addMeta('name', 'keywords', 'About Cheza Music School')
            ->addMeta('name', 'title', 'About Cheza Music School')
            ->addMeta('name', 'description', 'Our professional approach and continual development has led us to becoming the leading provider of professional home and online music education. ')
            ->addMeta('property', 'og:title', 'About Cheza Music School')
            ->addMeta('property', 'og:type', 'website')
            ->addMeta('property', 'og:image', "https://chezamusicschool.co.ke/adm/assets/images/cheza_logo_sm.jpg")
            ->addMeta('property', 'og:url', 'https://chezamusicschool.co.ke/about/cheza/school' )
            ->addMeta('property', 'og:description', 'Our professional approach and continual development has led us to becoming the leading provider of professional home and online music education. ')
        ;

        return $this->render('default/about.html.twig');
            
    }

    /**
     * @Route("/about/cheza/contact", name="contact")
     */
    public function contact(SeoPageInterface $seoPage): Response
    {

        $seoPage
            ->setTitle('Cheza Music School Contacts')
            ->addMeta('name', 'keywords', 'Cheza Music School Contact Phone Number')
            ->addMeta('name', 'title', 'Cheza Music School Contacts, Phone Number')
            ->addMeta('name', 'description', 'Call/SMS us on 0711832933 or WhatsApp 0716339886')
            ->addMeta('property', 'og:title', 'Cheza Music School Contacts, Phone Number')
            ->addMeta('property', 'og:type', 'website')
            ->addMeta('property', 'og:image', "https://chezamusicschool.co.ke/adm/assets/images/cheza_logo_sm.jpg")
            ->addMeta('property', 'og:url', 'https://chezamusicschool.co.ke/about/cheza/contact' )
            ->addMeta('property', 'og:description', 'Call/SMS us on 0711832933 or WhatsApp 0716339886')
        ;

        return $this->render('default/contact.html.twig');
            
    }

    /**
     * @Route("/about/cheza/classes", name="classes")
     */
    public function classes(InstrumentRepository $instrumentRepo): Response
    {
        $data = [];
        $instruments = $instrumentRepo->findBy(
            array('category' => 'instrument'),
            array('id' => 'ASC')
        );
        $courses = $instrumentRepo->findBy(
            array('category' => 'course'),
            array('id' => 'ASC')
        );
        $traditional = $instrumentRepo->findBy(
            array('category' => 'traditional'),
            array('id' => 'ASC')
        );
        $data['instruments'] = $instruments;
        $data['courses'] = $courses;
        $data['traditional'] = $traditional;

        return $this->render('default/classes.html.twig',[
            'data' => $data,
        ]);
            
    }

    // Define function to test
    function _is_curl_installed() {
        if  (in_array  ('curl', get_loaded_extensions())) {
            return true;
        }
        else {
            return false;
        }
    }


}
