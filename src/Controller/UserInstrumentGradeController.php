<?php

namespace App\Controller;

use App\Entity\UserInstrumentGrade;
use App\Form\UserInstrumentGradeType;
use App\Repository\UserInstrumentGradeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user/instrument/grade")
 */
class UserInstrumentGradeController extends AbstractController
{
    /**
     * @Route("/", name="user_instrument_grade_index", methods={"GET"})
     */
    public function index(UserInstrumentGradeRepository $userInstrumentGradeRepository): Response
    {
        return $this->render('user_instrument_grade/index.html.twig', [
            'user_instrument_grades' => $userInstrumentGradeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="user_instrument_grade_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $userInstrumentGrade = new UserInstrumentGrade();
        $form = $this->createForm(UserInstrumentGradeType::class, $userInstrumentGrade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userInstrumentGrade);
            $entityManager->flush();

            return $this->redirectToRoute('user_instrument_grade_index');
        }

        return $this->render('user_instrument_grade/new.html.twig', [
            'user_instrument_grade' => $userInstrumentGrade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_instrument_grade_show", methods={"GET"})
     */
    public function show(UserInstrumentGrade $userInstrumentGrade): Response
    {
        return $this->render('user_instrument_grade/show.html.twig', [
            'user_instrument_grade' => $userInstrumentGrade,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_instrument_grade_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UserInstrumentGrade $userInstrumentGrade): Response
    {
        $form = $this->createForm(UserInstrumentGradeType::class, $userInstrumentGrade);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_instrument_grade_index');
        }

        return $this->render('user_instrument_grade/edit.html.twig', [
            'user_instrument_grade' => $userInstrumentGrade,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_instrument_grade_delete", methods={"DELETE"})
     */
    public function delete(Request $request, UserInstrumentGrade $userInstrumentGrade): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userInstrumentGrade->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $teacher = null;
            foreach ($userInstrumentGrade->getSessions() as $session ) {
                $entityManager->remove($session);
                $teacher = $session->getTeacher();
            }
            foreach ($userInstrumentGrade->getPayments() as $payment) {
                $entityManager->remove($payment);
            }
            // $entityManager->remove($userInstrumentGrade);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_session_calendar', ['teacher_id' => $teacher->getId()]);
    }
}
