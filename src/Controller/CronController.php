<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Repository\SessionRepository;
use App\Service\Mailer;
use Twig\Environment;
use Knp\Snappy\Pdf;
use App\Repository\UserSettingsRepository;

class CronController extends AbstractController
{
    private $mailer;
    private $pdf;
    private $twig;
    private $usrSt;
    private $userRepo;
    private $sessionRepo;

    public function __construct(Mailer $mailer, Pdf $pdf, Environment $twig, UserSettingsRepository $usrSt, UserRepository $userRepo, SessionRepository $sessionRepo)
    {
        $this->mailer = $mailer;
        $this->pdf = $pdf;
        $this->twig = $twig;
        $this->usrSt = $usrSt;
        $this->userRepo = $userRepo;
        $this->sessionRepo = $sessionRepo;

    }
    
    /**
     * @Route("/cron/sendreminder/to/teachers/{category}/{emailtype}", name="cron")
     */
    public function index($category, $emailtype): Response
    {

        $test = [];
        $tl = [];

        $teachers = $this->userRepo->findBy(
            array('usertype' => 'teacher'),
            array('id' => 'ASC')
        );

        foreach ($teachers as $teacher) {

            $settings = [];
            $emailtypes = [];
            $send = false;
            $todayslessons = $this->sessionRepo->findTodaysLessons($teacher);
            $tl[] = $todayslessons;

            $usersettings = $teacher->getUserSettings();

            foreach ($usersettings as $key => $setting) {
                $settings[$setting->getSetting()->getDescription()] = $setting->getSetting()->getDescription()."-".$setting->getValue();
                $emailtypes[$setting->getSetting()->getId()] = $setting->getSetting()->getDescription();
            }
    
            // if no settings have been done OR the category for this email is 'necessary', send email
            if(empty($settings) || $category == "necessary" ){
                $send = false; //change this to true when the assigning of lessons is over
            } 
            // if the setting for this emailtype is done and it is on, send email
            if(in_array($emailtype."-on", $settings)){
                $send = true;
            }
            // if the setting for this emailtype is not done, assume that it is on
            if(!in_array($emailtype, $emailtypes)){
                $send = false;
            }
            // if all emails is off, don't send email
            if(in_array("all_emails-off", $settings)){
                $send = false;
            }
    
            if($send == true && count($todayslessons) > 0){
                $maildata = ['todayslessons' => $todayslessons, 'user' => $teacher];
                $this->mailer->sendEmailMessage($maildata, $teacher->getEmail(), "Today's Lessons", "cron.html.twig", $category, $emailtype);    
                $test[] = $teacher->getFullname(). ': '.count($usersettings).'-'.$send;
            } else {
                $test[] = 'nope';
            }
    
            $send = false;

        }
        return $this->render('cron/index.html.twig', [
            'test' => $test,
            'todayslessons' => $tl,
        ]);
    }
}
